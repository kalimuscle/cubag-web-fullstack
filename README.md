# cubag-project

Cubag is a  web app for create tours

## CLI Commands

``` Front end
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev
```

``` Back end
# go to /server
cd server

# install dependencies
npm install

# run server
node index.js
```

# Consideraations

The app was developed in node version v14.16.1, you need node-sass 4.+.

For detailed explanation on how things work, checkout the [CLI Readme](https://github.com/developit/preact-cli/blob/master/README.md).

