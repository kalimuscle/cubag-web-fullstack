import { h } from 'preact';
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'preact-router/match';
import {useEffect, useState} from "preact/hooks";
import ErrorModal from '../../components/errorModal';
import SidePanel from '../../components/sidepanel';
import imgProduct2 from '../../assets/imgs/img4.jpeg'
import TopPanel from '../../components/topPanel';
import PinnedPanel from '../../components/pinnedPanel';

import Pagination from '../../components/pagination';


const MyTours = () => {
	const [menu, setMenu] = useState(false);

	return (
		<div class="min-h-full"> 	
			<SidePanel showMenu={menu} onClose={() => setMenu(false)}/>
		
			<div class="lg:pl-64 flex flex-col">
			
				<div class="sticky top-0 z-10 flex-shrink-0 flex h-16 bg-white border-b border-gray-200 lg:hidden">
			
					<button onClick={()=> setMenu(true)} type="button" class="px-4 border-r border-gray-200 text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-blue-500 lg:hidden">
						<span class="sr-only">Open sidebar</span>
						
						<svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
							<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h8m-8 6h16" />
						</svg>
					</button>
					<div class="flex-1 flex justify-between px-4 sm:px-6 lg:px-8">
						<div class="flex-1 flex">
							<form class="w-full flex md:ml-0" action="#" method="GET">
								<label for="search-field" class="sr-only">Search</label>
								<div class="relative w-full text-gray-400 focus-within:text-gray-600">
									<div class="absolute inset-y-0 left-0 flex items-center pointer-events-none">
										
										<svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
											<path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd" />
										</svg>
									</div>
									<input id="search-field" name="search-field" class="block w-full h-full pl-8 pr-3 py-2 border-transparent text-gray-900 placeholder-gray-500 focus:outline-none focus:ring-0 focus:border-transparent focus:placeholder-gray-400 sm:text-sm" placeholder="Search" type="search"/>
								</div>
							</form>
						</div>
						<div class="flex items-center">
						
							<div class="ml-3 relative">
								<div>
									<button type="button" class="max-w-xs bg-white flex items-center text-sm rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500" id="user-menu-button" aria-expanded="false" aria-haspopup="true">
										<span class="sr-only">Open user menu</span>
										<img class="h-8 w-8 rounded-full" src="https://images.unsplash.com/photo-1502685104226-ee32379fefbe?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt=""/>
									</button>
								</div>

								
								<div class="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 divide-y divide-gray-200 focus:outline-none" role="menu" aria-orientation="vertical" aria-labelledby="user-menu-button" tabindex="-1">
									<div class="py-1" role="none">
										
										<a href="#" class="text-gray-700 block px-4 py-2 text-sm" role="menuitem" tabindex="-1" id="user-menu-item-0">View profile</a>
										<a href="#" class="text-gray-700 block px-4 py-2 text-sm" role="menuitem" tabindex="-1" id="user-menu-item-1">Settings</a>
										<a href="#" class="text-gray-700 block px-4 py-2 text-sm" role="menuitem" tabindex="-1" id="user-menu-item-2">Notifications</a>
									</div>
									<div class="py-1" role="none">
										<a href="#" class="text-gray-700 block px-4 py-2 text-sm" role="menuitem" tabindex="-1" id="user-menu-item-3">Get desktop app</a>
										<a href="#" class="text-gray-700 block px-4 py-2 text-sm" role="menuitem" tabindex="-1" id="user-menu-item-4">Support</a>
									</div>
									<div class="py-1" role="none">
										<a href="#" class="text-gray-700 block px-4 py-2 text-sm" role="menuitem" tabindex="-1" id="user-menu-item-5">Logout</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<main class="flex-1">
			
					<TopPanel title="My tours"/>
				
					{/* <PinnedPanel/> */}
					
					<div class="lg:col-span-3">
					
						<div class="grid m-8  h-96 lg:h-full">
							<div class="mt-6 grid grid-cols-1 gap-y-2 sm:grid-cols-3 gap-x-6">
								<a href="/detail/23232" class="group">
									<div class="w-full aspect-w-1 aspect-h-1 bg-gray-200 rounded-lg overflow-hidden xl:aspect-w-7 xl:aspect-h-8">
										<img src={imgProduct2} alt="Olive drab green insulated bottle with flared screw lid and flat top." class="w-full h-full object-center object-cover group-hover:opacity-75"/>
									</div>
									<h3 class="mt-4 text-sm text-gray-700">Nomad Tumbler</h3>
									<p class="mt-1 text-lg font-medium text-gray-900">$35</p>
								</a>

								<a href="/detail/23232" class="group">
									<div class="w-full aspect-w-1 aspect-h-1 bg-gray-200 rounded-lg overflow-hidden xl:aspect-w-7 xl:aspect-h-8">
										<img src={imgProduct2} alt="Olive drab green insulated bottle with flared screw lid and flat top." class="w-full h-full object-center object-cover group-hover:opacity-75"/>
									</div>
									<h3 class="mt-4 text-sm text-gray-700">Nomad Tumbler</h3>
									<p class="mt-1 text-lg font-medium text-gray-900">$35</p>
								</a>

								<a href="/detail/23232" class="group">
									<div class="w-full aspect-w-1 aspect-h-1 bg-gray-200 rounded-lg overflow-hidden xl:aspect-w-7 xl:aspect-h-8">
										<img src={imgProduct2} alt="Olive drab green insulated bottle with flared screw lid and flat top." class="w-full h-full object-center object-cover group-hover:opacity-75"/>
									</div>
									<h3 class="mt-4 text-sm text-gray-700">Nomad Tumbler</h3>
									<p class="mt-1 text-lg font-medium text-gray-900">$35</p>
								</a>

								<a href="/detail/23232" class="group">
									<div class="w-full aspect-w-1 aspect-h-1 bg-gray-200 rounded-lg overflow-hidden xl:aspect-w-7 xl:aspect-h-8">
										<img src={imgProduct2} alt="Olive drab green insulated bottle with flared screw lid and flat top." class="w-full h-full object-center object-cover group-hover:opacity-75"/>
									</div>
									<h3 class="mt-4 text-sm text-gray-700">Nomad Tumbler</h3>
									<p class="mt-1 text-lg font-medium text-gray-900">$35</p>
								</a>

								<a href="/detail/23232" class="group">
									<div class="w-full aspect-w-1 aspect-h-1 bg-gray-200 rounded-lg overflow-hidden xl:aspect-w-7 xl:aspect-h-8">
										<img src={imgProduct2} alt="Olive drab green insulated bottle with flared screw lid and flat top." class="w-full h-full object-center object-cover group-hover:opacity-75"/>
									</div>
									<h3 class="mt-4 text-sm text-gray-700">Nomad Tumbler</h3>
									<p class="mt-1 text-lg font-medium text-gray-900">$35</p>
								</a>

								<a href="/detail/23232" class="group">
									<div class="w-full aspect-w-1 aspect-h-1 bg-gray-200 rounded-lg overflow-hidden xl:aspect-w-7 xl:aspect-h-8">
										<img src={imgProduct2} alt="Olive drab green insulated bottle with flared screw lid and flat top." class="w-full h-full object-center object-cover group-hover:opacity-75"/>
									</div>
									<h3 class="mt-4 text-sm text-gray-700">Nomad Tumbler</h3>
									<p class="mt-1 text-lg font-medium text-gray-900">$35</p>
								</a>

							</div>

						</div>
						<Pagination/>
					</div>
				</main>
			</div>
		</div>

	);
}

export default MyTours;