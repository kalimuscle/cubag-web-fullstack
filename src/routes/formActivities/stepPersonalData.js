import { h } from 'preact';
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'preact-router/match';
import {useEffect, useState, useContext} from "preact/hooks";
import { TranslateContext } from '@denysvuika/preact-translate';
import heroImage from '../../assets/imgs/img8.jpeg'
import { gql, useLazyQuery  } from '@apollo/client';
import Loading from '../../components/loading';
import {ErrorGraphql} from '../../components/errorModal';

import { create_people_id} from '../../store/actions/';

// Define query
const PEOPLES_SEARCH_BY_EMAIL_QUERY = gql`
  query peopleByEmail($email: String!, $tourCreatorId: ID!){
	peopleByEmail(email: $email, tourCreatorId: $tourCreatorId){
		id
		email
	}
}
`;


const FormPickPersonalData = ({follow}) => {
	const [queryFunction, { data, loading, error }] = useLazyQuery (PEOPLES_SEARCH_BY_EMAIL_QUERY);
	const { setLang, t, lang } = useContext(TranslateContext);
	const [showSearchedInfo, setShowSearchedInfo] = useState(false);
	const [email, setEmail] = useState('');

	const tourCreatorId = useSelector(state => state.tourCreatorId);

	const dispatch = useDispatch();

	const gotoStepOptions = () => {
		dispatch(create_people_id(data.peopleByEmail.id));
		setShowSearchedInfo(false);
		follow('options');
	}

	const queryByEmail = async () => {

		const query = await queryFunction({
			variables: { 
				email,
				tourCreatorId
			} 
		});
		//console.log(data);
		setShowSearchedInfo(query.data.peopleByEmail.id.length > 0);
	}

	if (loading) return <Loading />

  	if (error ) return <ErrorGraphql follow={(step)=> follow(step)}/>;

	const filteredPeople = showSearchedInfo ? (
		<div class="-mx-4 mt-1 overflow-hidden shadow ring-1 ring-black ring-opacity-5 sm:-mx-6 md:mx-0 md:rounded-lg">
									
			<table class="min-w-full divide-y divide-gray-300">
				<thead class="bg-gray-50">
					<tr>
						<th scope="col" class="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6">Profile founded</th>							
						<th scope="col" class="relative py-3.5 pl-3 pr-4 sm:pr-6">
							<span class="sr-only">Edit</span>
						</th>
					</tr>
				</thead>
				<tbody class="divide-y divide-gray-200 bg-white">
					<tr>
						<td class="whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-6">{data.peopleByEmail.email}</td>
													
						<td class="whitespace-nowrap py-4 pl-3 pr-4 text-right text-sm font-medium sm:pr-6">
							<a onClick={gotoStepOptions} class="text-indigo-600 cursor-pointer hover:text-indigo-900">Select<span class="sr-only">, {data.peopleByEmail.email}</span></a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	) : null;

	const validateEmail = (value) => {
		const expEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		const pattern_email = new RegExp(expEmail);

		return pattern_email.test(value)
	}

	const isValidInput = (value) => {
		const inputClass = "mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm";
		const invalidClass = "mt-1 block w-full border border-red-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-red-500 focus:border-red-500 sm:text-sm"

		return value ? inputClass : invalidClass
	}

	const isValidForm = (value) => {
		const buttonClass = "bg-blue-600 w-full mt-2 mb-4 border border-transparent rounded-md shadow-sm py-2 px-4 inline-flex justify-center text-sm font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500";
		const disabledClass = "opacity-50 bg-blue-600 w-full mt-2 mb-4 border border-transparent rounded-md shadow-sm py-2 px-4 inline-flex justify-center text-sm font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500";
		
		return value ? buttonClass : disabledClass
	}

	return (
		<main>
			<div class="min-h-full flex">
				<div class="hidden lg:block relative w-0 flex-1">
					<img class="absolute inset-0 w-full object-cover h-screen"  src={heroImage} alt=""/>
				</div>
				<div class="flex-1 flex flex-col justify-center py-12 px-4 sm:px-6 lg:flex-none lg:px-20 xl:px-24">
					<div class="w-full max-w-sm ">
						<div class="lg:col-start-2 lg:pl-8">
							<div class="text-base max-w-prose mx-auto lg:max-w-lg lg:ml-auto lg:mr-0">
								<h2 class="leading-6 text-blue-600 font-semibold tracking-wide uppercase">{t('stepPersonalData.quote')}</h2>
								<h3 class="mt-2 text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">{t('stepPersonalData.quote')}</h3>
								<p class="mt-8 text-lg text-gray-500">
									{t('stepPersonalData.p1')}
								</p>
								
								<div class="px-4 sm:px-6 lg:px-8">
									<input type="email" name="email-address" id="email-address" placeholder='Type email to search profile' autocomplete="email"  value={email} onChange={(evt)=>setEmail(evt.target.value)}  class={isValidInput(email.length == 0 || validateEmail(email))}/>
									
									<button onClick={() => queryByEmail()} type="button" class={isValidForm(validateEmail(email))}>
										Search Profile
									</button>
									{filteredPeople}
								</div>
								<div className='my-10'>
									<button onClick={() => follow('create-profile')} type="button" class="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500">
										{t('stepPersonalData.create_profile')}
									</button>
								</div>		
							</div>
						</div>
					</div>
				</div>	
			</div>
		</main>
	);
}

export default FormPickPersonalData;