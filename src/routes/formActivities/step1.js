import { h } from 'preact';
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'preact-router/match';
import {useEffect, useState, useContext} from "preact/hooks";
import { TranslateContext } from '@denysvuika/preact-translate';
import heroImage from '../../assets/imgs/img8.jpeg'

const FormActivitiesStep1 = ({follow}) => {
	const { setLang, t, lang } = useContext(TranslateContext);

	return (
		<main>
			<div class="min-h-full flex">
				<div class="hidden lg:block relative w-0 flex-1">
					<img class="absolute inset-0 w-full object-cover h-screen"  src={heroImage} alt=""/>
				</div>
				<div class="flex-1 flex flex-col justify-center py-12 px-4 sm:px-6 lg:flex-none lg:px-20 xl:px-24">
					<div class="w-full max-w-sm ">
						<div class="lg:col-start-2 lg:pl-8">
							<div class="text-base max-w-prose mx-auto lg:max-w-lg lg:ml-auto lg:mr-0">
								<h2 class="leading-6 text-blue-600 font-semibold tracking-wide uppercase">{t('step1.quote')}</h2>
								<h3 class="mt-2 text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">{t('step1.quote')}</h3>
								<p class="mt-8 text-lg text-gray-500">
									{t('step1.p1')}
								</p>
								<p class="mt-8 text-lg text-gray-500">
									{t('step1.p2')}
								</p>
								<p class="mt-8 text-lg text-gray-500">
									{t('step1.p3')}
								</p>
								<div className='my-10'>
									<button onClick={() => follow('personal-data')} type="button" class="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500">
										{t('step1.start_process')}
									</button>
								</div>		
							</div>
						</div>
					</div>
				</div>	
			</div>
		</main>
	);
}

export default FormActivitiesStep1;