import { h } from 'preact';
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'preact-router/match';
import {useEffect, useState, useContext} from "preact/hooks";
import { TranslateContext } from '@denysvuika/preact-translate';
import heroImage from '../../assets/imgs/img8.jpeg'
import FormPersonalData from './forms/formPersonalData';

const FormCreateProfile = ({follow}) => {
	const { setLang, t, lang } = useContext(TranslateContext);

	const setStyleButtonActivated = ( button) => {
		const normalStyle = 'text-gray-900 hover:text-gray-900 hover:bg-gray-50 group rounded-md px-3 py-2 flex items-center text-sm font-medium';
		const activatedStyle = 'text-gray-900 hover:text-gray-900 hover:bg-blue-100 bg-blue-100 group rounded-md px-3 py-2 flex items-center text-sm font-medium';

		return option == button ? activatedStyle : normalStyle
	}
	
	return (
		<main>
			<div class="max-w-7xl mx-auto py-6 sm:px-6 lg:px-8">
				<h2 class="leading-6 text-blue-600 font-semibold tracking-wide uppercase">
                  <a class="cursor-pointer" onClick={()=>follow('personal-data')}>Volver</a>
                </h2>
				<h3 class="mt-2 text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">Create profile</h3>
				<FormPersonalData follow={(val)=>follow(val)}/>
			</div>
		
		</main>

	);
}

export default FormCreateProfile;