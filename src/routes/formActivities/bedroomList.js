import { h } from 'preact';
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'preact-router/match';
import {useEffect, useState, useContext} from "preact/hooks";
import { TranslateContext } from '@denysvuika/preact-translate';
import { gql, useQuery, useMutation, useLazyQuery  } from '@apollo/client';
import TimeAgo from 'react-timeago'
import Loading from '../../components/loading';
import {ErrorGraphql} from '../../components/errorModal';

// Define query
const GET_BEDROOM_QUERY = gql`
  query getLodging($id: ID!){
	getLodging(id: $id){
		id
		name
		phone1
		bedrooms{
			items{
      			id
				updatedAt
			}
		}
    	
	}
}
`;

const DELETE_BEDROOM_MUTATION = gql`
	mutation deleteBedroom($id: ID!){
		deleteBedroom(id: $id){
			id
		}
	}
`;

const BedroomList = ({lodgingId, follow, backOption = false, modify = true}) => {
	const peopleId = useSelector(state => state.peopleId);
	const { loading, error, data } = useQuery(GET_BEDROOM_QUERY, {
		variables: { id: lodgingId },
	
		onCompleted: (data) => {  
			console.log({data});
		}
	  });
	const [deleteFunction, { loading: dloading, error: derror }] = useMutation(DELETE_BEDROOM_MUTATION, {
		onCompleted: (data) => {  
			console.log({data});
		}
	});
	const { setLang, t, lang } = useContext(TranslateContext);

	if (loading || dloading) return <Loading />

  	if (error || derror) return <ErrorGraphql follow={(step)=> follow(step)}/>;

	const remove = async (id) => {
		await deleteFunction({
			variables: {
				id
			}
		});
		await refetch({  id: lodgingId  });
	}
	const items = data.getLodging.bedrooms.items;
	

	const list_li = items.map((item)=>(
		<li>
			<div class="group flex items-center justify-between px-4 py-4 hover:bg-gray-50 sm:px-6">
				<span class="flex items-center truncate space-x-3">
					<span class="font-medium truncate text-sm leading-6">
						<a class="cursor-pointer text-blue-600" onClick={()=> follow(`edit-bedroom;${item.id}`)}>{item.id}</a>
		
					</span>
				</span>
				<button onClick={()=> remove(item.id)} class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800">{t('buttons.delete')}</button>
			</div>
		</li>
	  ));

	  const list_tr = items.map((item)=>(
		<tr>
			<td class="px-6 py-3 max-w-0 w-full whitespace-nowrap text-sm font-medium text-gray-900">
				<div class="flex items-center space-x-3 lg:pl-2">
					<p class="hover:text-gray-600 text-gray-500">
						<a class="cursor-pointer text-blue-600" onClick={()=> follow(`edit-bedroom;${item.id}`)}>{item.id}</a>
					</p>
				</div>
			</td>
			<td class="px-6 py-3 text-sm text-gray-500 font-medium">
				<div class="flex items-center space-x-2">
					{data.getLodging.name}
				</div>
			</td>
			<td class="hidden md:table-cell px-6 py-3 whitespace-nowrap text-sm text-gray-500 text-right">{data.getLodging.phone1}</td>
			<td class="hidden md:table-cell px-6 py-3 whitespace-nowrap text-sm text-gray-500 text-right">
				<TimeAgo date={new Date(item.updatedAt).toDateString() }  />
			</td>
			<td class="px-6 py-3 whitespace-nowrap text-right text-sm font-medium">
				<button onClick={()=> remove(item.id)} class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800">{t('buttons.delete')}</button>
			</td>
		</tr>
	  ));

	  const buttonAdd = modify ? (
		<button type="button" onClick={()=>follow(`create-bedroom;${lodgingId}`)} class="inline-flex items-center justify-center rounded-md border border-transparent bg-blue-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2 sm:w-auto">
			{t('buttons.new.bedroom')}
	 	</button>
	  ): null


	const backLinkHeader = backOption ? (
		<div class="m-4 sm:flex sm:items-center">
			<div class="sm:flex-auto">
				<h2 class="leading-6 text-blue-600 font-semibold tracking-wide uppercase">
						<a class="cursor-pointer" onClick={()=>follow('options')}>{t('buttons.back')}</a>
					</h2>
	 				<h3 class="mt-2 text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">
					 {t('labels.bedrooms')}
	 				</h3>
	 			</div>
				
	 			<div class="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
	 				{buttonAdd}
	 			</div>
			</div>
	): (
		<div class="border-b border-gray-200 px-4 py-4 sm:flex sm:items-center sm:justify-between sm:px-6 lg:px-8">
				<div class="flex-1 min-w-0">
					<h1 class="text-lg font-medium leading-6 text-gray-900 sm:truncate">{t('labels.bedrooms')}</h1>
				</div>
				<div class="mt-4 flex sm:mt-0 sm:ml-4">
					{buttonAdd}
				</div>
			</div>
	);

	return (
		<main>
			{backLinkHeader}
			<Fragment>
				<div class="mt-10 sm:hidden">
					<div class="px-4 sm:px-6">
						<h2 class="text-gray-500 text-xs font-medium uppercase tracking-wide">{t('labels.bedrooms.title')}</h2>
					</div>
					<ul role="list" class="mt-3 border-t border-gray-200 divide-y divide-gray-100">
						{list_li}

					</ul>
				</div>

				<div class="hidden mt-8 sm:block">
					<div class="align-middle inline-block min-w-full border-b border-gray-200">
						<table class="min-w-full">
							<thead>
								<tr class="border-t border-gray-200">
									<th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
										<span class="lg:pl-2">{t('labels.bedrooms.title')}</span>
									</th>
									<th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">{t('labels.lodgings.title')}</th>
									<th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">{t('labels.bedrooms.phone')}</th>
									<th class="hidden md:table-cell px-6 py-3 border-b border-gray-200 bg-gray-50 text-right text-xs font-medium text-gray-500 uppercase tracking-wider">{t('labels.updated')}</th>
									<th class="pr-6 py-3 border-b border-gray-200 bg-gray-50 text-right text-xs font-medium text-gray-500 uppercase tracking-wider"></th>
								</tr>
							</thead>
							<tbody class="bg-white divide-y divide-gray-100">
								{list_tr}
							</tbody>
						</table>
					</div>
				</div>
			</Fragment>
		</main>
  );
}

export default BedroomList;