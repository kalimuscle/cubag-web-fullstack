import { h } from 'preact';
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'preact-router/match';
import { gql, useLazyQuery, useMutation } from '@apollo/client';
import {useEffect, useState, useContext} from "preact/hooks";
import { TranslateContext } from '@denysvuika/preact-translate';

import ErrorModal from '../../../components/errorModal';
import ImageUploader from '../../../components/imageUpload';
import Loading from '../../../components/loading';
import {ErrorGraphql} from '../../../components/errorModal';

// Define mutation
const CREATE_GUIDE_MUTATION = gql`
  mutation createGuide($input: CreateGuideInput!){
	createGuide(input: $input){
		id
	}
}
`;

const GUIDE_BY_ID_QUERY = gql`
  query getGuide($id: ID!){
	getGuide(id: $id){
		id
		description
		priceDay
		priceHour
		spanish
		english
		french
		german
		italian
		portuguese
		russian
		chinese
		japanese
		dutch
		poland
		danish
		master
		general
		trustman
		travel_guide
		music
		visual_arts
		education
		political
		history
		local
		rum
		cigar
		coffee
		religion
		gourmet
		flower
		locality
		province
		nature
		near_province
		western
		eastern
		central
		help
		activities
		fulltime
		images
	}
}
`;

const FormGuideActivity = ({id, follow}) => {
	const [queryFunction, { data, loading, error }] = useLazyQuery(GUIDE_BY_ID_QUERY, {
		variables: { id },
		pollInterval: 500,
	  });
	const [mutateFunction, {data: mdata, loading: mloading, error: merror}] = useMutation(CREATE_GUIDE_MUTATION, {onCompleted: (data) => {  console.log({data});  }});

	const { setLang, t, lang } = useContext(TranslateContext);
	const [images, setImages] = useState([]);

	const tourCreatorId = useSelector(state => state.tourCreatorId);
	const peopleId = useSelector(state => state.peopleId);


	const [description, setDescription] = useState('');
	const [priceDay, setPriceDay] = useState(0);
	const [priceHour, setPriceHour] = useState(0);

	const [spanish, setSpanish] = useState(false);
	const [english, setEnglish] = useState(false);
	const [french, setFrench] = useState(false);
	const [german, setGerman] = useState(false);
	const [italian, setItalian] = useState(false);
	const [portuguese, setPortuguese] = useState(false);
	const [russian, setRussian] = useState(false);
	const [chinese, setChinese] = useState(false);
	const [japanese, setJapanese] = useState(false);
	const [dutch, setDutch] = useState(false);
	const [poland, setPoland] = useState(false);
	const [danish, setDanish] = useState(false);
	const [master, setMaster] = useState(false);
	const [general, setGeneral] = useState(false);
	const [trustman, setTrustman] = useState(false);
	const [travel_guide, setTravelGuide] = useState(false);
	const [music, setMusic] = useState(false);
	const [dance, setDance] = useState(false);
	const [visual_arts, setVisualArts] = useState(false);
	const [education, setEducation] = useState(false);
	const [political, setPolitical] = useState(false);
	const [history, setHistory] = useState(false);
	const [local, setLocal] = useState(false);
	const [rum, setRum] = useState(false);
	const [cigar, setCigar] = useState(false);
	const [coffee, setCoffee] = useState(false);
	const [religion, setReligion] = useState(false);
	const [gourmet, setGourmet] = useState(false);
	const [flower, setFlower] = useState(false);
	const [locality, setLocality] = useState(false);
	const [province, setProvince] = useState(false);
	const [nature, setNature] = useState(false);
	const [near_province, setNearProvince] = useState(false);
	const [western, setWestern] = useState(false);
	const [eastern, setEastern] = useState(false);
	const [central, setCentral] = useState(false);
	const [help, setHelp] = useState(false);
	const [activities, setActivities] = useState(false);
	const [fulltime, setFulltime] = useState(false);

	useEffect(async() => {
		if(id !== ''){
			const query = await queryFunction({
				variables:{
					id
				}
			});
			setDescription(query.data.getGuide.description);
			setPriceDay(query.data.getGuide.priceDay);
			setPriceHour(query.data.getGuide.priceHour);
			setSpanish(query.data.getGuide.spanish);
			setEnglish(query.data.getGuide.english);
			setFrench(query.data.getGuide.french);
			setGerman(query.data.getGuide.german);
			setItalian(query.data.getGuide.italian);
			setPortuguese(query.data.getGuide.portuguese);
			setRussian(query.data.getGuide.russian);
			setChinese(query.data.getGuide.chinese);
			setJapanese(query.data.getGuide.japanese);
			setDutch(query.data.getGuide.dutch);
			setPoland(query.data.getGuide.poland);
			setDanish(query.data.getGuide.danish);
			setMaster(query.data.getGuide.master);
			setGeneral(query.data.getGuide.general);
			setTrustman(query.data.getGuide.trustman);

			setTravelGuide(query.data.getGuide.travel_guide);
			setMusic(query.data.getGuide.music);
			setVisualArts(query.data.getGuide.visual_arts);
			setEducation(query.data.getGuide.education);
			setPolitical(query.data.getGuide.political);
			setHistory(query.data.getGuide.history);
			setLocal(query.data.getGuide.local);
			setRum(query.data.getGuide.rum);
			setCigar(query.data.getGuide.cigar);
			setCoffee(query.data.getGuide.coffee);

			setReligion(query.data.getGuide.religion);
			setGourmet(query.data.getGuide.gourmet);
			setFlower(query.data.getGuide.flower);
			setLocality(query.data.getGuide.locality);
			setProvince(query.data.getGuide.province);
			setNature(query.data.getGuide.nature);
			setNearProvince(query.data.getGuide.near_province);
			setWestern(query.data.getGuide.western);
			setEastern(query.data.getGuide.eastern);
			setCentral(query.data.getGuide.central);

			setHelp(query.data.getGuide.help);
			setFulltime(query.data.getGuide.fulltime);
			setActivities(query.data.getGuide.activities);
			setImages(query.data.getGuide.images);
		
		}
		
	  },[]);

	if (loading || mloading) return (
		<Loading />
	);

  	if (error || merror) return <ErrorGraphql follow={(step)=> follow(step)}/>;

	

	const OnChangeImages = async ({imageList, addUpdateIndex}) => {
		console.log(imageList, addUpdateIndex);
		setImages(imageList)
	}

	const save = async () => {
		//mutateFunction.arguments = imageList[0].file;

		const elemId = id != "" ? id : undefined; console.log('id**************************', elemId);
		const mutation = await mutateFunction({ 
			variables: { 
				input: {
					id: elemId,
					tourCreatorId,
					peopleId,
					description,
					priceDay,
					priceHour,
					spanish,
					english,
					french,
					german,
					italian,
					portuguese,
					russian,
					chinese,
					japanese,
					dutch,
					poland,
					danish,
					master,
					general,
					trustman,
					travel_guide,
					music,
					visual_arts,
					education,
					political,
					history,
					local,
					rum,
					cigar,
					coffee,
					religion,
					gourmet,
					flower,
					locality,
					province,
					nature,
					near_province,
					western,
					eastern,
					central,
					help,
					activities,
					fulltime,
					images: []
				}
			} 
		});

		follow('guide-list');
	}


	const isValidInput = (value) => {
		const inputClass = "mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm";
		const invalidClass = "mt-1 block w-full border border-red-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-red-500 focus:border-red-500 sm:text-sm"

		return value ? inputClass : invalidClass
	}

	const isValidForm = (value) => {
		const buttonClass = "bg-blue-600 border border-transparent rounded-md shadow-sm py-2 px-4 inline-flex justify-center text-sm font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500";
		const disabledClass = "opacity-50 bg-blue-600 border border-transparent rounded-md shadow-sm py-2 px-4 inline-flex justify-center text-sm font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500";
		
		return value ? buttonClass : disabledClass
	}

	const validateForm = () => (
		description.length > 8 &&
		(
			priceDay > 0 ||
			priceHour > 0
		)
	)

	return (
		<form action="#" method="POST">
			<div class="shadow sm:rounded-md sm:overflow-hidden">
				<div class="bg-white py-6 px-4 space-y-6 sm:p-6">
				<div>
					<h3 class="text-lg leading-6 font-medium text-gray-900">{t('formGuide.title')}</h3>
					<p class="mt-1 text-sm text-gray-500">{t('formGuide.description')}</p>
				</div>

				<div class="grid grid-cols-6 gap-6">
					<div class="col-span-6 sm:col-span-4">
						<label for="about" class="block text-sm font-medium text-gray-700"> {t('formGuide.knowledge')} </label>
						<div class="mt-1">
							<textarea id="description" name="description" rows="3" value={description} onChange={(evt)=>setDescription(evt.target.value)} class={isValidInput( description.length > 8)}></textarea>
						</div>
						<p class="mt-2 text-sm text-gray-500">{t('formGuide.knowledge_label')}</p>
					</div>
					<div class="col-span-6 sm:col-span-4">
						<legend class="text-base font-medium text-gray-900">{t('formGuide.price_label')}</legend>

						<div class="col-span-6 sm:col-span-3">
							<label for="priceHour" class="block text-sm font-medium text-gray-700">{t('formGuide.price_hour')}</label>
							<input type="number" name="priceHour" min="0" id="priceHour" value={priceHour} onChange={(evt)=>setPriceHour(evt.target.valueAsNumber)} class={isValidInput(priceHour >= 0)}/>
						</div>

						<div class="col-span-6 sm:col-span-4">
							<label for="priceDay" class="block text-sm font-medium text-gray-700">{t('formGuide.price_day')}</label>
							<input type="number" name="priceDay" min="0" id="priceDay" value={priceDay} onChange={(evt)=>setPriceDay(evt.target.valueAsNumber)} class={isValidInput(priceDay >= 0)}/>
						</div>
					</div>
					<div class="col-span-6 sm:col-span-4">
						<fieldset>
							<legend class="text-base font-medium text-gray-900">{t('formGuide.language_known')}</legend>
							<div class="col-span-3 sm:col-span-3 mt-4 space-y-4 ">
								<div class="flex items-start">
									<div class="h-5 flex items-center">
										<input id="spanish" name="spanish" type="checkbox" checked={spanish} onChange={()=> setSpanish(!spanish)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
									</div>
									<div class="ml-3 text-sm">
										<label for="spanish" class="font-medium text-gray-700">{t('formGuide.language.spanish')}</label>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="english" name="english" type="checkbox" checked={english} onChange={()=> setEnglish(!english)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="english" class="font-medium text-gray-700">{t('formGuide.language.english')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="french" name="french" type="checkbox" checked={french} onChange={()=> setFrench(!french)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="french" class="font-medium text-gray-700">{t('formGuide.language.french')}</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-span-3 sm:col-span-3 mt-4 space-y-4 ">
								<div class="flex items-start">
									<div class="h-5 flex items-center">
										<input id="german" name="german" type="checkbox" checked={german} onChange={()=> setGerman(!german)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
									</div>
									<div class="ml-3 text-sm">
										<label for="german" class="font-medium text-gray-700">{t('formGuide.language.german')}</label>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="italian" name="italian" type="checkbox" checked={italian} onChange={()=> setItalian(!italian)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="italian" class="font-medium text-gray-700">{t('formGuide.language.italian')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="portuguese" name="portuguese" type="checkbox" checked={portuguese} onChange={()=> setPortuguese(!portuguese)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="portuguese" class="font-medium text-gray-700">{t('formGuide.language.portuguese')}</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-span-3 sm:col-span-3 mt-4 space-y-4 ">
								<div class="flex items-start">
									<div class="h-5 flex items-center">
										<input id="russian" name="russian" type="checkbox" checked={russian} onChange={()=> setRussian(!russian)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
									</div>
									<div class="ml-3 text-sm">
										<label for="russian" class="font-medium text-gray-700">{t('formGuide.language.russian')}</label>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="chinese" name="chinese" type="checkbox" checked={chinese} onChange={()=> setChinese(!chinese)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="chinese" class="font-medium text-gray-700">{t('formGuide.language.chinese')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="japanese" name="japanese" type="checkbox" checked={japanese} onChange={()=> setJapanese(!japanese)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="japanese" class="font-medium text-gray-700">{t('formGuide.language.japanese')}</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-span-3 sm:col-span-3 mt-4 space-y-4 ">
								<div class="flex items-start">
									<div class="h-5 flex items-center">
										<input id="dutch" name="dutch" type="checkbox" checked={dutch} onChange={()=> setDutch(!dutch)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
									</div>
									<div class="ml-3 text-sm">
										<label for="dutch" class="font-medium text-gray-700">{t('formGuide.language.dutch')}</label>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="poland" name="poland" type="checkbox" checked={poland} onChange={()=> setPoland(!poland)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="poland" class="font-medium text-gray-700">{t('formGuide.language.poland')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="danish" name="danish" type="checkbox" checked={danish} onChange={()=> setDanish(!danish)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="danish" class="font-medium text-gray-700">{t('formGuide.language.danish')}</label>
										</div>
									</div>
								</div>
							</div>
						</fieldset>
					</div>

					<div class="col-span-6 sm:col-span-4">
						<fieldset>
							<legend class="text-base font-medium text-gray-900">{t('formGuide.specialties')}</legend>
							<div class="col-span-3 sm:col-span-3 mt-4 space-y-4 ">
								<div class="flex items-start">
									<div class="h-5 flex items-center">
										<input id="master" name="master" type="checkbox" checked={master} onChange={()=> setMaster(!master)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
									</div>
									<div class="ml-3 text-sm">
										<label for="master" class="font-medium text-gray-700">{t('formGuide.specialties.master')}</label>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="general" name="general" type="checkbox" checked={general} onChange={()=> setGeneral(!general)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="general" class="font-medium text-gray-700">{t('formGuide.specialties.general')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="trustman" name="trustman" type="checkbox" checked={trustman} onChange={()=> setTrustman(!trustman)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="trustman" class="font-medium text-gray-700">{t('formGuide.specialties.trustman')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="travel_guide" name="travel_guide" type="checkbox" checked={travel_guide} onChange={()=> setTravelGuide(!travel_guide)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="travel_guide" class="font-medium text-gray-700">{t('formGuide.specialties.travel_guide')}</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-span-3 sm:col-span-3 mt-4 space-y-4 ">
								<div class="flex items-start">
									<div class="h-5 flex items-center">
										<input id="music" name="music" type="checkbox" checked={music} onChange={()=> setMusic(!music)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
									</div>
									<div class="ml-3 text-sm">
										<label for="music" class="font-medium text-gray-700">{t('formGuide.specialties.music')}</label>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="dance" name="dance" type="checkbox" checked={dance} onChange={()=> setDance(!dance)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="dance" class="font-medium text-gray-700">{t('formGuide.specialties.dance')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="visual_arts" name="visual_arts" type="checkbox" checked={visual_arts} onChange={()=> setVisualArts(!visual_arts)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="visual_arts" class="font-medium text-gray-700">{t('formGuide.specialties.visual_arts')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="education" name="education" type="checkbox" checked={education} onChange={()=> setEducation(!education)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="education" class="font-medium text-gray-700">{t('formGuide.specialties.education')}</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-span-3 sm:col-span-3 mt-4 space-y-4 ">
								<div class="flex items-start">
									<div class="h-5 flex items-center">
										<input id="political" name="political" type="checkbox"  checked={political} onChange={()=> setPolitical(!political)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
									</div>
									<div class="ml-3 text-sm">
										<label for="political" class="font-medium text-gray-700">{t('formGuide.specialties.political')}</label>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="history" name="history" type="checkbox" checked={history} onChange={()=> setHistory(!history)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="history" class="font-medium text-gray-700">{t('formGuide.specialties.history')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="local" name="local" type="checkbox" checked={local} onChange={()=> setLocal(!local)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="local" class="font-medium text-gray-700">{t('formGuide.specialties.local')}</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-span-3 sm:col-span-3 mt-4 space-y-4 ">
								<div class="flex items-start">
									<div class="h-5 flex items-center">
										<input id="rum" name="rum" type="checkbox" checked={rum} onChange={()=> setRum(!rum)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
									</div>
									<div class="ml-3 text-sm">
										<label for="rum" class="font-medium text-gray-700">{t('formGuide.specialties.rum')}</label>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="cigar" name="cigar" type="checkbox" checked={cigar} onChange={()=> setCigar(!cigar)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="cigar" class="font-medium text-gray-700">{t('formGuide.specialties.cigar')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="coffee" name="coffee" type="checkbox"  checked={coffee} onChange={()=> setCoffee(!coffee)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="coffee" class="font-medium text-gray-700">{t('formGuide.specialties.coffee')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="religion" name="religion" type="checkbox" checked={religion} onChange={()=> setReligion(!religion)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="religion" class="font-medium text-gray-700">{t('formGuide.specialties.religion')}</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-span-3 sm:col-span-3 mt-4 space-y-4 ">
								<div class="flex items-start">
									<div class="h-5 flex items-center">
										<input id="gourmet" name="gourmet" type="checkbox" checked={gourmet} onChange={()=> setGourmet(!gourmet)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
									</div>
									<div class="ml-3 text-sm">
										<label for="gourmet" class="font-medium text-gray-700">{t('formGuide.specialties.gourmet')}</label>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="nature" name="nature" type="checkbox" checked={nature} onChange={()=> setNature(!nature)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="nature" class="font-medium text-gray-700">{t('formGuide.specialties.nature')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="flower" name="flower" type="checkbox" checked={flower} onChange={()=> setFlower(!flower)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="flower" class="font-medium text-gray-700">{t('formGuide.specialties.flower')}</label>
										</div>
									</div>
								</div>
							</div>
						</fieldset>
					</div>

					<div class="col-span-6 sm:col-span-4">
						<fieldset>
							<legend class="text-base font-medium text-gray-900">{t('formGuide.location_availability')} </legend>
							<div class="col-span-3 sm:col-span-3 mt-4 space-y-4 ">
								<div class="flex items-start">
									<div class="h-5 flex items-center">
										<input id="locality" name="locality" type="checkbox" checked={locality} onChange={()=> setLocality(!locality)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
									</div>
									<div class="ml-3 text-sm">
										<label for="locality" class="font-medium text-gray-700">{t('formGuide.location.locality')}</label>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="province" name="province" type="checkbox" checked={province} onChange={()=> setProvince(!province)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="province" class="font-medium text-gray-700">{t('formGuide.location.province')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="near_province" name="near_province" type="checkbox" checked={near_province} onChange={()=> setNearProvince(!near_province)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="near_province" class="font-medium text-gray-700">{t('formGuide.location.near_province')}</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-span-3 sm:col-span-3 mt-4 space-y-4 ">
								<div class="flex items-start">
									<div class="h-5 flex items-center">
										<input id="western" name="western" type="checkbox" checked={western} onChange={()=> setWestern(!western)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
									</div>
									<div class="ml-3 text-sm">
										<label for="western" class="font-medium text-gray-700">{t('formGuide.location.western')}</label>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="central" name="central" type="checkbox" checked={central} onChange={()=> setCentral(!central)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="central" class="font-medium text-gray-700">{t('formGuide.location.central')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="eastern" name="eastern" type="checkbox" checked={eastern} onChange={()=> setEastern(!eastern)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="eastern" class="font-medium text-gray-700">{t('formGuide.location.eastern')}</label>
										</div>
									</div>
								</div>
							</div>
						</fieldset>
					</div>

					<div class="col-span-6 sm:col-span-4">
						<fieldset>
							<legend class="text-base font-medium text-gray-900">{t('formGuide.services')} </legend>
							<div class="mt-4 space-y-4">
								<div class="flex items-start">
									<div class="h-5 flex items-center">
										<input id="help" name="help" type="checkbox" checked={help} onChange={()=> setHelp(!help)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
									</div>
									<div class="ml-3 text-sm">
										<label for="help" class="font-medium text-gray-700">{t('formGuide.services.help')}</label>
										<p class="text-gray-500">{t('formGuide.services.help.label')}</p>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="activities" name="activities" type="checkbox" checked={activities} onChange={()=> setActivities(!activities)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="activities" class="font-medium text-gray-700">{t('formGuide.services.activities')}</label>
											<p class="text-gray-500">{t('formGuide.services.activities.label')}</p>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="fulltime" name="fulltime" type="checkbox" checked={fulltime} onChange={()=> setFulltime(!fulltime)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="fulltime" class="font-medium text-gray-700">{t('formGuide.services.fulltime')}</label>
											<p class="text-gray-500">{t('formGuide.services.fulltime.label')}</p>
										</div>
									</div>
								</div>
							</div>
						</fieldset>
					</div>
					<div class="col-span-6">
						<ImageUploader 
							images={images}
							onChange={OnChangeImages}
							maxNumber={4} 
						/>
					</div>
				</div>
				</div>
				<div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
					<button type="button" onClick={()=> save()} class={isValidForm(validateForm())}>{t('buttons.save')}</button>
				</div>
			</div>
		</form>

	);
}

export default FormGuideActivity;