import { h } from 'preact';
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'preact-router/match';
import {useEffect, useState, useContext} from "preact/hooks";
import Select from '../../../components/select';
import ImageUploader from '../../../components/imageUpload';
import { TranslateContext } from '@denysvuika/preact-translate';
import PhoneInput from 'react-phone-input-2';
import { gql, useLazyQuery, useMutation } from '@apollo/client';
import Loading from '../../../components/loading';
import {ErrorGraphql} from '../../../components/errorModal';

const CREATE_TRANSPORTATION_MUTATION = gql`
  mutation createTransportation($input: CreateTransportationInput!){
	createTransportation(input: $input){
		id
	}
}
`;

const TRANSPORTATION_BY_ID_QUERY = gql`
  query getTransportation($id: ID!){
	getTransportation(id: $id){
		id
		images
		license
		property
		transportationType
		phone1
		phone2
		pricePerson
		priceHour
		matricule
		passengers
		luggage
		air_conditioning
		rent_bycicle
		reservation
		handicapped_accessible
		availability_day
		availability_nigth
		trailer
		service24h

	}
}
`;


const FormTransportation = ({id, follow}) => {
	const { setLang, t, lang } = useContext(TranslateContext);
	const [queryFunction, { data, loading, error }] = useLazyQuery(TRANSPORTATION_BY_ID_QUERY, {
		variables: { id },
		pollInterval: 500,
	  });
	const [mutateFunction, {data: mdata, loading: mloading, error: merror}] = useMutation(CREATE_TRANSPORTATION_MUTATION, {onCompleted: (data) => {  console.log({data});  }});
	const dispatch = useDispatch();

	const tourCreatorId = useSelector(state => state.tourCreatorId);
	const peopleId = useSelector(state => state.peopleId);

	const [images, setImages] = useState([]);

	const [license, setLicense] = useState('');
	const [property, setProperty] = useState('private');
	const [transportationType, setTransportationType] = useState('american-car');
	const [phone1, setPhone1] = useState('');
	const [phone2, setPhone2] = useState('');
	const [pricePerson, setPricePerson] = useState(0);
	const [priceHour, setPriceHour] = useState(0);
	const [matricule, setMatricule] = useState('');
	const [passengers, setPassengers] = useState(1);

	const [luggage, setLuggage] = useState(false);
	const [air_conditioning, setAirConditioning] = useState(false);
	const [rent_bycicle, setRentBycicle] = useState(false);
	const [reservation, setReservation] = useState(false);
	const [trailer, setTrailer] = useState(false);
	const [handicapped_accessible, setHandicappedAccessible] = useState(false);
	const [availability_day, setAvailabilityDay] = useState(false);
	const [availability_nigth, setAvailabilityNigth] = useState(false);
	const [service24h, setService24h] = useState(false);

	useEffect(async() => {
		if(id !== ''){

			const query = await queryFunction({
				variables:{
					id
				}
			});

			setImages(query.data.getTransportation.images);
			setLicense(query.data.getTransportation.license);
			setProperty(query.data.getTransportation.property);
			setTransportationType(query.data.getTransportation.transportationType);
			setPhone1(query.data.getTransportation.phone1);
			setPhone2(query.data.getTransportation.phone2);
			setPricePerson(query.data.getTransportation.pricePerson);
			setPriceHour(query.data.getTransportation.priceHour);
			setMatricule(query.data.getTransportation.matricule);
			setPassengers(query.data.getTransportation.passengers);

			setLuggage(query.data.getTransportation.luggage);
			setAirConditioning(query.data.getTransportation.air_conditioning);
			setRentBycicle(query.data.getTransportation.rent_bycicle);
			setReservation(query.data.getTransportation.reservation);
			setTrailer(query.data.getTransportation.trailer);
			setHandicappedAccessible(query.data.getTransportation.handicapped_accessible);
			setAvailabilityDay(query.data.getTransportation.availability_day);
			setAvailabilityNigth(query.data.getTransportation.availability_nigth);
			setService24h(query.data.getTransportation.service24h);
		
		}
		
	  },[]);

	if (loading || mloading) return (
		<Loading />
	);

  	if (error || merror) return <ErrorGraphql follow={(step)=> follow(step)}/>;

	
	  const save = async () => {
	
		const elemId = id != "" ? id : undefined;
		const mutation = await mutateFunction({ 
			variables: { 
				input: {
					id: elemId,
					tourCreatorId,
					peopleId,
					
					license,
					property,
					transportationType,
					phone1,
					phone2,
					pricePerson,
					priceHour,
					matricule,
					passengers,
					luggage,
					air_conditioning,
					rent_bycicle,
					reservation,
					handicapped_accessible,
					availability_day,
					availability_nigth,
					trailer,
					service24h,
					images: []
				}
			} 
		});
	
		follow('transportation-list');
		
	}

	const properties = [
		{
			key: 'private',
			name: lang == 'en' ? 'Privated transportation' : 'Transporte privado'
		},
		{
			key: 'state',
			name: lang == 'en' ? 'State' : 'Estatal'
		}
	];

	const transportationTypes = [
		{
			key: 'taxi',
			name: lang == 'en' ? 'Taxi' : 'Taxi'
		},
		{
			key: 'american-car',
			name: lang == 'en' ? 'American car' : 'Carro americano'
		},
		{
			key: 'russian-car',
			name: lang == 'en' ? 'Russian car' : 'Carro ruso'
		},
		{
			key: 'convertible-car',
			name: lang == 'en' ? 'Convertible car' : 'Carro descapotable'
		},
		{
			key: 'modern-car',
			name: lang == 'en' ? 'Modern car' : 'Carro moderno'
		},
		{
			key: 'limusine',
			name: lang == 'en' ? 'Limusine' : 'Limusina'
		},
		{
			key: 'bus',
			name: lang == 'en' ? 'Bus' : 'Omnibus/Guagua'
		},
		{
			key: 'motocycle',
			name: lang == 'en' ? 'Motocycle' : 'moto'
		},
		{
			key: 'train',
			name: lang == 'en' ? 'Train' : 'Tren'
		},
		{
			key: 'boat',
			name: lang == 'en' ? 'Boat' : 'Embarcación'
		},
		{
			key: 'animal-traction-vehicle',
			name: lang == 'en' ? 'Animal traction vehicle' : 'Vehículo de tracción animal'
		},
		{
			key: 'bycicle',
			name: lang == 'en' ? 'Bycicle' : 'Bicicleta'
		},
		{
			key: 'other-vehicle',
			name: lang == 'en' ? 'Other vehicles' : 'Otros vehículos'
		}
	];

	const isValidInput = (value) => {
		const inputClass = "mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm";
		const invalidClass = "mt-1 block w-full border border-red-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-red-500 focus:border-red-500 sm:text-sm"

		return value ? inputClass : invalidClass
	}

	const isValidForm = (value) => {
		const buttonClass = "bg-blue-600 border border-transparent rounded-md shadow-sm py-2 px-4 inline-flex justify-center text-sm font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500";
		const disabledClass = "opacity-50 bg-blue-600 border border-transparent rounded-md shadow-sm py-2 px-4 inline-flex justify-center text-sm font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500";
		
		return value ? buttonClass : disabledClass
	}

	const validateForm = () => (
		license.length >= 8 &&
		phone1.length == 10 &&
		(
			priceHour > 0 || 
			pricePerson > 0
		)
	)

	return (
		<form action="#" method="POST">
			<div class="shadow sm:rounded-md sm:overflow-hidden">
				<div class="bg-white py-6 px-4 space-y-6 sm:p-6">
				<div>
					<h3 class="text-lg leading-6 font-medium text-gray-900">
						{t('formTransportation.title')}
					</h3>
					<p class="mt-1 text-sm text-gray-500">
						{t('formTransportation.description')}
					</p>
				</div>

				<div class="grid grid-cols-6 gap-6">
					<div class="col-span-6 sm:col-span-3">
						<label for="first-name" class="block text-sm font-medium text-gray-700">
							{t('formTransportation.license')}
							<span class="text-red-500">*</span>
						</label>
						<input type="text" name="license" id="license" value={license} onChange={(evt)=>setLicense(evt.target.value)} class={isValidInput( license.length > 8)}/>
					</div>

					<div class="col-span-6 sm:col-span-3 lg:col-span-2">
						<Select
							required={true}
							label={t('formTransportation.propertyType')}
							value={property}
							onChange={({value})=>setProperty(value)}
							items={properties}
						/>
					</div>

					<div class="col-span-6 sm:col-span-3 lg:col-span-2">
						<Select
							required={true}
							label={t('formTransportation.transportationType')}
							value={transportationType}
							onChange={({value})=>setTransportationType(value)}
							items={transportationTypes}
						/>
					</div>

					<div class="col-span-6 sm:col-span-3">
						<label for="last-name" class="block text-sm font-medium text-gray-700">{t('formLodging.phone1')} <span class="text-red-500">*</span></label>
						<PhoneInput
							country={'cu'}
							value={phone1}
							onChange={(value)=>setPhone1(value)}
							inputClass={isValidInput(phone1.length == 10)}
							specialLabel=""
							placeholder='+53 XXX XXX XX'
						/>
					</div>

					<div class="col-span-6 sm:col-span-3">
						<label for="last-name" class="block text-sm font-medium text-gray-700">{t('formLodging.phone2')}</label>
						<PhoneInput
							country={'cu'}
							value={phone2}
							onChange={(value)=>setPhone2(value)}
							inputClass={isValidInput(phone2.length == 0 || phone2.length == 10)}
							specialLabel=""
							placeholder='+53 XXX XXX XX'
						/>
					</div>

					<div class="col-span-6 sm:col-span-4">
						<legend class="text-base font-medium text-gray-900">{t('formTransportation.detail_technical_label')} </legend>

						<div class="col-span-6 sm:col-span-4">
							<label for="matricule" class="block text-sm font-medium text-gray-700">{t('formTransportation.matricule')}</label>
							<input type="text" name="matricule" id="matricule" placeholder='xxxxxxxxxxxxxx' value={matricule} onChange={(evt)=>setMatricule(evt.target.value)} class={isValidInput(matricule.length == 0 || matricule.length >= 8)}/>
						</div>
						<div class="col-span-6 sm:col-span-4">
							<label for="passengers" class="block text-sm font-medium text-gray-700">{t('formTransportation.passengers')}</label>
							<input type="number" name="passengers" min="1" id="passengers" value={passengers} onChange={(evt)=>setPassengers(evt.target.valueAsNumber)} class={isValidInput(passengers > 0)}/>
						</div>
					</div>
					<div class="col-span-6 sm:col-span-4">
						<legend class="text-base font-medium text-gray-900">{t('formTransportation.price_label')}</legend>

						<div class="col-span-6 sm:col-span-3">
							<label for="pricePerson" class="block text-sm font-medium text-gray-700">{t('formTransportation.price_person')}</label>
							<input type="number" name="pricePerson" min="0" id="pricePerson" value={pricePerson} onChange={(evt)=>setPricePerson(evt.target.valueAsNumber)} class={isValidInput(pricePerson >= 0)}/>
						</div>

						<div class="col-span-6 sm:col-span-4">
							<label for="priceHour" class="block text-sm font-medium text-gray-700">{t('formTransportation.price_hour')}</label>
							<input type="number" name="priceHour" min="0" id="priceHour" value={priceHour} onChange={(evt)=>setPriceHour(evt.target.valueAsNumber)} class={isValidInput(priceHour >= 0)}/>
						</div>
					</div>	
					
					<div class="col-span-6 sm:col-span-4">
						<fieldset>
							<legend class="text-base font-medium text-gray-900">{t('formTransportation.details')} </legend>
							<div class="col-span-3 sm:col-span-3 mt-4 space-y-4 ">
								<div class="flex items-start">
									<div class="h-5 flex items-center">
										<input id="luggage" name="luggage" type="checkbox" checked={luggage} onChange={()=> setLuggage(!luggage)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
									</div>
									<div class="ml-3 text-sm">
										<label for="luggage" class="font-medium text-gray-700">{t('formTransportation.features.luggage')}</label>
									</div>
								</div>
								<div class="flex items-start">
									<div class="h-5 flex items-center">
										<input id="handicapped_accessible" name="handicapped_accessible" type="checkbox" checked={handicapped_accessible} onChange={()=> setHandicappedAccessible(!handicapped_accessible)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
									</div>
									<div class="ml-3 text-sm">
										<label for="handicapped_accessible" class="font-medium text-gray-700">{t('formTransportation.features.handicapped_accessible')}</label>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="air_conditioning" name="air_conditioning" type="checkbox" checked={air_conditioning} onChange={()=> setAir_conditioning(!air_conditioning)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="air_conditioning" class="font-medium text-gray-700">{t('formTransportation.features.air_conditioning')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="rent_bycicle" name="rent_bycicle" type="checkbox" checked={rent_bycicle} onChange={()=> setRentBycicle(!rent_bycicle)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="rent_bycicle" class="font-medium text-gray-700">{t('formTransportation.features.rent_bycicle')}</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-span-3 sm:col-span-3 mt-4 space-y-4 ">
								<div class="flex items-start">
									<div class="h-5 flex items-center">
										<input id="reservation" name="reservation" type="checkbox" checked={reservation} onChange={()=> setReservation(!reservation)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
									</div>
									<div class="ml-3 text-sm">
										<label for="reservation" class="font-medium text-gray-700">{t('formTransportation.features.reservation')}</label>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="trailer" name="trailer" type="checkbox" checked={trailer} onChange={()=> setTrailer(!trailer)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="trailer" class="font-medium text-gray-700">{t('formTransportation.features.trailer')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="availability_day" name="availability_day" type="checkbox" checked={availability_day} onChange={()=> setAvailabilityDay(!availability_day)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="availability_day" class="font-medium text-gray-700">{t('formTransportation.features.availability_day')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="availability_nigth" name="availability_nigth" type="checkbox" checked={availability_nigth} onChange={()=> setAvailabilityNigth(!availability_nigth)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="availability_nigth" class="font-medium text-gray-700">{t('formTransportation.features.availability_nigth')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="service24h" name="service24h" type="checkbox" checked={service24h} onChange={()=> setService24h(!service24h)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="service24h" class="font-medium text-gray-700">{t('formTransportation.features.service24h')}</label>
										</div>
									</div>
								</div>
							</div>
						</fieldset>
					</div>

					<div class="col-span-6">
						<ImageUploader maxNumber={5} />
					</div>
				</div>
				</div>
				<div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
					<button type="button" onClick={()=>save()} class={isValidForm(validateForm())}>{t('buttons.save')}</button>
				</div>
			</div>
		</form>
	);
}

export default FormTransportation;