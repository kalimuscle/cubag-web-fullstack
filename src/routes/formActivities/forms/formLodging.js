import { h } from 'preact';
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'preact-router/match';
import {useEffect, useState, useContext} from "preact/hooks";
import ErrorModal from '../../../components/errorModal';
import Select from '../../../components/select';
import ImageUploader from '../../../components/imageUpload';
import { TranslateContext } from '@denysvuika/preact-translate';
import PhoneInput from 'react-phone-input-2';
import { gql, useLazyQuery, useMutation } from '@apollo/client';
import Loading from '../../../components/loading';
import {ErrorGraphql} from '../../../components/errorModal';

const CREATE_LODGING_MUTATION = gql`
  mutation createLodging($input: CreateLodgingInput!){
	createLodging(input: $input){
		id
	}
}
`;

const LODGING_BY_ID_QUERY = gql`
  query getLodging($id: ID!){
	getLodging(id: $id){
		id
		images
		name
		email
		phone1
		phone2
		address
		city
		state
		zip
		latitude
		longitude
		rooms
		maxCapacity
		priceBed
		checkin
		closingHour
		breakfastPrice
		dinnerPrice
		laundryPrice
		rentType
		floor

		colonial
		tradicionalAmerican
		tradicionalFrench
		mansion
		farm
		building

		freeWasher
		freeKitchen
		freeFan
		freeFilteredWater
		freeFridge
		freeRefrigerator
		freeTelephone

		private_entrance
		elevator
		handicapped_accessible
		terrace
		balcony
		tv
		hot_water
		air_conditioning

		ventilator
		fridge
		refrigerator
		filtered_water
		phone
		security_safe
		jacuzzi
		hair_dryer

		wifi
		own_kitchen
		tan_chamber
		linens
		towels
		closet
		toilet_paper
		bar
		service_24h
		seaview
		garden
		street_view
		country_view
		updatedAt
	}
}
`;


const FormLodging = ({id, follow}) => { console.log('ENTRO *************************');
	const { setLang, t, lang } = useContext(TranslateContext);
	const [queryFunction, { data, loading, error }] = useLazyQuery(LODGING_BY_ID_QUERY, {
		variables: { id },
		pollInterval: 500,
	  });
	const [mutateFunction, {data: mdata, loading: mloading, error: merror}] = useMutation(CREATE_LODGING_MUTATION, {onCompleted: (data) => {  console.log({data});  }});
	const dispatch = useDispatch();

	const tourCreatorId = useSelector(state => state.tourCreatorId);
	const peopleId = useSelector(state => state.peopleId);
	const [images, setImages] = useState([]);

	const [name, setName] = useState('');
	const [email, setEmail] = useState('');
	const [phone1, setPhone1] = useState('');
	const [phone2, setPhone2] = useState('');
	const [address, setAddress] = useState('');
	const [city, setCity] = useState('');
	const [state, setState] = useState('habana');
	const [zip, setZip] = useState('');
	const [latitude, setLatitude] = useState('');
	const [longitude, setLongitude] = useState('');
	const [rooms, setRooms] = useState(0);
	const [maxCapacity, setMaxCapacity] = useState(0);
	const [priceBed, setPriceBed] = useState(0);
	const [checkin, setCheckin] = useState('');
	const [closingHour, setClosingHour] = useState('');
	const [breakfastPrice, setBreakfastPrice] = useState(0);
	const [dinnerPrice, setDinnerPrice] = useState(0);
	const [laundryPrice, setLaundryPrice] = useState(0);

	const [rentType, setRentType] = useState('hotel');
	const [floor, setFloor] = useState('first-floor');

	const [colonial, setColonial] = useState(false);
	const [tradicionalAmerican, setTradicionalAmerican] = useState(false);
	const [tradicionalFrench, setTradicionalFrench] = useState(false);
	const [mansion, setMansion] = useState(false);
	const [farm, setFarm] = useState(false);
	const [building, setBuilding] = useState(false);

	const [freeWasher, setFreeWasher] = useState(false);
	const [freeKitchen, setFreeKitchen] = useState(false);
	const [freeFan, setFreeFan] = useState(false);
	const [freeFilteredWater, setFilteredWater] = useState(false);
	const [freeFridge, setFreeFridge] = useState(false);
	const [freeRefrigerator, setFreeRefrigerator] = useState(false);
	const [freeTelephone, setFreeTelephone] = useState(false);

	const [private_entrance, setPrivate_entrance] = useState(false);
	const [elevator, setElevator] = useState(false);
	const [handicapped_accessible, setHandicapped_accessible] = useState(false);
	const [terrace, setTerrace] = useState(false);
	const [balcony, setBalcony] = useState(false);
	const [tv, setTV] = useState(false);
	const [hot_water, setHot_water] = useState(false);
	const [air_conditioning, setAir_conditioning] = useState(false);

	const [ventilator, setVentilator] = useState(false);
	const [fridge, setFridge] = useState(false);
	const [refrigerator, setRefrigerator] = useState(false);
	const [filtered_water, setFiltered_water] = useState(false);
	const [phone, setPhone] = useState(false);
	const [security_safe, setSecurity_safe] = useState(false);
	const [jacuzzi, setJacuzzi] = useState(false);
	const [hair_dryer, setHair_dryer] = useState(false);

	const [wifi, setWifi] = useState(false);
	const [own_kitchen, setOwn_kitchen] = useState(false);
	const [tan_chamber, setTan_chamber] = useState(false);
	const [linens, setLinens] = useState(false);
	const [towels, setTowels] = useState(false);
	const [closet, setCloset] = useState(false);
	const [toilet_paper, setToilet_paper] = useState(false);
	const [bar, setBar] = useState(false);
	const [service_24h, setService24h] = useState(false);

	const [other_feaures, setOther_features] = useState('');

	const [seaview, setSeaview] = useState(false);
	const [garden, setGarden] = useState(false);
	const [street_view, setStreet_view] = useState(false);
	const [country_view, setCountry_view] = useState(false);

	useEffect(async() => {
		if(id !== ''){

			const query = await queryFunction({
				variables:{
					id
					
				}
			});

			setImages(query.data.getLodging.images);
			setName(query.data.getLodging.name);
			setEmail(query.data.getLodging.email);
			setPhone1(query.data.getLodging.phone1);
			setPhone2(query.data.getLodging.phone2);
			setAddress(query.data.getLodging.address);
			setCity(query.data.getLodging.city);
			setState(query.data.getLodging.state);
			setZip(query.data.getLodging.zip);
			setLatitude(query.data.getLodging.latitude);
			setLongitude(query.data.getLodging.longitude);
			setRooms(query.data.getLodging.rooms);
			setMaxCapacity(query.data.getLodging.maxCapacity);
			setPriceBed(query.data.getLodging.priceBed);
			setCheckin(query.data.getLodging.checkin);
			setClosingHour(query.data.getLodging.closingHour);
			setBreakfastPrice(query.data.getLodging.breakfastPrice);
			setDinnerPrice(query.data.getLodging.dinnerPrice);
			setLaundryPrice(query.data.getLodging.laundryPrice);

			setRentType(query.data.getLodging.rentType);
			setFloor(query.data.getLodging.floor);
			setColonial(query.data.getLodging.colonial);
			setTradicionalAmerican(query.data.getLodging.tradicionalAmerican);
			setTradicionalFrench(query.data.getLodging.tradicionalFrench);
			setMansion(query.data.getLodging.mansion);
			setFarm(query.data.getLodging.farm);
			setBuilding(query.data.getLodging.building);

			setFreeWasher(query.data.getLodging.freeWasher);
			setFreeKitchen(query.data.getLodging.freeKitchen);
			setFreeFan(query.data.getLodging.freeFan);
			setFilteredWater(query.data.getLodging.freeFilteredWater);
			setFreeFridge(query.data.getLodging.freeFridge);
			setFreeRefrigerator(query.data.getLodging.freeRefrigerator);
			setFreeTelephone(query.data.getLodging.freeTelephone);

			setPrivate_entrance(query.data.getLodging.private_entrance);
			setElevator(query.data.getLodging.elevator);
			setHandicapped_accessible(query.data.getLodging.handicapped_accessible);
			setTerrace(query.data.getLodging.terrace);
			setBalcony(query.data.getLodging.balcony);
			setTV(query.data.getLodging.tv);
			setHot_water(query.data.getLodging.hot_water);
			setAir_conditioning(query.data.getLodging.air_conditioning);

			setVentilator(query.data.getLodging.ventilator);
			setFridge(query.data.getLodging.fridge);
			setRefrigerator(query.data.getLodging.refrigerator);
			setFiltered_water(query.data.getLodging.filtered_water);
			setPhone(query.data.getLodging.phone);
			setSecurity_safe(query.data.getLodging.security_safe);
			setJacuzzi(query.data.getLodging.jacuzzi);
			setHair_dryer(query.data.getLodging.hair_dryer);

			setWifi(query.data.getLodging.wifi);
			setOwn_kitchen(query.data.getLodging.own_kitchen);
			setTan_chamber(query.data.getLodging.tan_chamber);
			setLinens(query.data.getLodging.linens);
			setTowels(query.data.getLodging.towels);
			setCloset(query.data.getLodging.closet);
			setToilet_paper(query.data.getLodging.toilet_paper);
			setBar(query.data.getLodging.bar);
			setService24h(query.data.getLodging.service_24h);

			setOther_features(query.data.getLodging.other_feaures);
			setSeaview(query.data.getLodging.seaview);
			setGarden(query.data.getLodging.garden);
			setStreet_view(query.data.getLodging.street_view);
			setCountry_view(query.data.getLodging.country_view);
		}
		
	  },[]);

	if (loading || mloading) return (
		<Loading />
	);

  	if (error || merror) return <ErrorGraphql follow={(step)=> follow(step)}/>;


	const items = [
		{
			key: 'hotel',
			name: lang == 'en' ? 'Hotel' : 'Hotel'
		},
		{
			key: 'rent',
			name: lang == 'en' ? 'Rent (per room)' : 'Alquiler (por cuarto)'
		},
		{
			key: 'hostel',
			name: lang == 'en' ? 'Hostel (per bed)' : 'Hostal (por cama)'
		},
		{
			key: 'apartment',
			name: lang == 'en' ? 'Apartment' : 'Apartamento'
		}
	];

	const itemsFloor = [
		{
			key: 'first-floor',
			name: lang == 'en' ? 'First Floor' : 'Primer piso'
		},
		{
			key: 'second-floor',
			name: lang == 'en' ? 'Second Floor' : 'Segundo piso'
		},
		{
			key: 'third-floor',
			name: lang == 'en' ? 'Third Floor' : 'Tercer piso'
		},
		{
			key: 'fouth-floor',
			name: lang == 'en' ? 'Fourth Floor' : 'Cuarto piso'
		},
		{
			key: 'fifth-floor',
			name: lang == 'en' ? 'Fifth Floor' : 'Quinto piso'
		}
	];

	const states = [
		{
			key: 'pinar-del-rio',
			name: 'Pinar del Rio'
		},
		{
			key: 'artemisa',
			name: 'Artemisa'
		},
		{
			key: 'habana',
			name: 'Habana'
		},
		{
			key: 'mayabeque',
			name: 'Mayabeque'
		},
		{
			key: 'matanzas',
			name: 'Matanzas'
		},
		{
			key: 'cienfuegos',
			name: 'Cienfuegos'
		},
		{
			key: 'santi-spiritus',
			name: 'Santi Spiritus'
		},
		{
			key: 'villa-clara',
			name: 'Villa Clara'
		},
		{
			key: 'ciego-de-avila',
			name: 'Ciego de Avila'
		},
		{
			key: 'camaguey',
			name: 'Camaguey'
		},
		{
			key: 'holguin',
			name: 'Holguin'
		},
		{
			key: 'granma',
			name: 'Granma'
		},
		{
			key: 'santiago-de-cuba',
			name: 'Santiago de Cuba'
		},
		{
			key: 'guantanamo',
			name: 'Guantanamo'
		},
		{
			key: 'isla-de-la-juventud',
			name: 'Isla de la Juventud'
		},
	];

	const save = async () => {
	
		const elemId = id != "" ? id : undefined;
		const mutation = await mutateFunction({ 
			variables: { 
				input: {
					id: elemId,
					tourCreatorId,
					peopleId,
					name,
					email,
					phone1,
					phone2,
					address,
					city,
					state,
					zip,
					latitude,
					longitude,
					rooms,
					maxCapacity,
					priceBed,
					checkin,
					closingHour,
					breakfastPrice,
					dinnerPrice,
					laundryPrice,
					rentType,
					floor,
					colonial,
					tradicionalAmerican,
					tradicionalFrench,
					mansion,
					farm,
					building,
					freeWasher,
					freeKitchen,
					freeFan,
					freeFilteredWater,
					freeFridge,
					freeRefrigerator,
					freeTelephone,
					private_entrance,
					elevator,
					handicapped_accessible,
					terrace,
					balcony,
					tv,
					hot_water,
					air_conditioning,
					ventilator,
					fridge,
					refrigerator,
					filtered_water,
					phone,
					security_safe,
					jacuzzi,
					hair_dryer,
					wifi,
					own_kitchen,
					tan_chamber,
					linens,
					towels,
					closet,
					toilet_paper,
					bar,
					service_24h,
					seaview,
					garden,
					street_view,
					country_view,
					images: []
				}
			} 
		});
	
		follow('lodging-list');
		
	}

	const validateEmail = (value) => {
		const expEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		const pattern_email = new RegExp(expEmail);

		return pattern_email.test(value)
	}

	const validateForm = () => (
		name.length >= 8 &&
		validateEmail(email) &&
		phone1.length == 10 &&
		address.length > 8 &&
		city.length > 8 &&
		zip > 100
	)
	

	const isValidInput = (value) => {
		const inputClass = "mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm";
		const invalidClass = "mt-1 block w-full border border-red-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-red-500 focus:border-red-500 sm:text-sm"

		return value ? inputClass : invalidClass
	}

	const isValidForm = (value) => {
		const buttonClass = "bg-blue-600 border border-transparent rounded-md shadow-sm py-2 px-4 inline-flex justify-center text-sm font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500";
		const disabledClass = "opacity-50 bg-blue-600 border border-transparent rounded-md shadow-sm py-2 px-4 inline-flex justify-center text-sm font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500";
		
		return value ? buttonClass : disabledClass
	}
		

	return (
		<form action="#" method="POST">
			<div class="shadow sm:rounded-md sm:overflow-hidden">
				<div class="bg-white py-6 px-4 space-y-6 sm:p-6">
					<div>
						<h3 class="text-lg leading-6 font-medium text-gray-900">{t('formLodging.title')}</h3>
						<p class="mt-1 text-sm text-gray-500">{t('formLodging.description')}</p>
					</div>

					<div class="grid grid-cols-6 gap-6">
						<div class="col-span-6 sm:col-span-3">
							<label for="first-name" class="block text-sm font-medium text-gray-700">{t('formLodging.name')} <span class="text-red-500">*</span></label>
							<input type="text" name="name" id="name" autocomplete="name" value={name} onChange={(evt)=>setName(evt.target.value)} class={isValidInput( name.length > 8)}/>
						</div>

						<div class="col-span-6 sm:col-span-4">
							<label for="email-address" class="block text-sm font-medium text-gray-700">{t('formLodging.email')} <span class="text-red-500">*</span></label>
							<input type="email" name="email-address" id="email-address" autocomplete="email"  value={email} onChange={(evt)=>setEmail(evt.target.value)} class={isValidInput(validateEmail(email))}/>
						</div>

						<div class="col-span-6 sm:col-span-3">
							<label for="last-name" class="block text-sm font-medium text-gray-700">{t('formLodging.phone1')}</label>
							<PhoneInput
								country={'cu'}
								value={phone1}
								onChange={(value)=>setPhone1(value)}
								inputClass={isValidInput(phone1.length == 10)}
								specialLabel=""
								placeholder='+53 XXX XXX XX'
							/>
						</div>

						<div class="col-span-6 sm:col-span-3">
							<label for="last-name" class="block text-sm font-medium text-gray-700">{t('formLodging.phone2')}</label>
							<PhoneInput
								country={'cu'}
								value={phone2}
								onChange={(value)=>setPhone2(value)}
								inputClass={isValidInput(phone2.length == 0 || phone2.length == 10)}
								specialLabel=""
								placeholder='+53 XXX XXX XX'
							/>
						</div>

						{/* <div class="col-span-6 sm:col-span-3">
							<label for="country" class="block text-sm font-medium text-gray-700">{t('formLodging.country')}</label>
							<select id="country" name="country" autocomplete="country-name" class="mt-1 block w-full bg-white border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm">
								<option>United States</option>
								<option>Canada</option>
								<option>Mexico</option>
							</select>
						</div> */}

						<div class="col-span-6">
							<label for="street-address" class="block text-sm font-medium text-gray-700">{t('formLodging.street_address')} <span class="text-red-500">*</span></label>
							<input type="text" name="street-address" id="street-address" autocomplete="street-address" value={address} onChange={(evt)=>setAddress(evt.target.value)} class={isValidInput(address.length > 8)}/>
						</div>

						<div class="col-span-6 sm:col-span-6 lg:col-span-2">
							<label for="city" class="block text-sm font-medium text-gray-700">{t('formLodging.city')} <span class="text-red-500">*</span></label>
							<input type="text" name="city" id="city" autocomplete="address-level2" value={city} onChange={(evt)=>setCity(evt.target.value)} class={isValidInput(city.length > 8)}/>
						</div>

						<div class="col-span-6 sm:col-span-3 lg:col-span-2">
							<Select
								required={true}
								label={t('formLodging.state')}
								value={state}
								onChange={({value})=>setState(value)}
								items={states}
							/>
						</div>

						<div class="col-span-6 sm:col-span-3 lg:col-span-2">
							<label for="postal-code" class="block text-sm font-medium text-gray-700">{t('formLodging.zip')} <span class="text-red-500">*</span></label>
							<input type="number" name="postal-code" id="postal-code" autocomplete="postal-code" value={zip} onChange={(evt)=>setZip(evt.target.valueAsNumber)} class={isValidInput(zip > 100)}/>
						</div>

						
						<div class="col-span-6 sm:col-span-4">
							<legend class="text-base font-medium text-gray-900">{t('formLodging.coordinates_label')} </legend>

							<div class="col-span-6 sm:col-span-3 lg:col-span-2">
								<label for="latitude" class="block text-sm font-medium text-gray-700">{t('formLodging.latitude')}</label>
								<input type="number" min="0" name="latitude" id="latitude" value={latitude} onChange={(evt)=>setLatitude(evt.target.valueAsNumber)} class={isValidInput(latitude >= 0)}/>
							</div>

							<div class="col-span-6 sm:col-span-3 lg:col-span-2">
								<label for="longitude" class="block text-sm font-medium text-gray-700">{t('formLodging.longitude')}</label>
								<input type="number" min="0" name="longitude" id="longitude" value={longitude} onChange={(evt)=>setLongitude(evt.target.valueAsNumber)} class={isValidInput(longitude >= 0)}/>
							</div>
						</div>
						<div class="col-span-6 sm:col-span-4">
							<legend class="text-base font-medium text-gray-900">{t('formLodging.availability')} </legend>

							<div class="col-span-3 sm:col-span-3">
								<label for="rooms" class="block text-sm font-medium text-gray-700">{t('formLodging.rooms')}</label>
								<input type="number" min="0" name="rooms" id="rooms" value={rooms} onChange={(evt)=>setRooms(evt.target.valueAsNumber)} class={isValidInput(rooms >= 0)}/>
							</div>

							<div class="col-span-3 sm:col-span-4">
								<label for="max_capability" class="block text-sm font-medium text-gray-700">{t('formLodging.max_capability')}</label>
								<input type="number" min="0" name="max_capability" id="max_capability" value={maxCapacity} onChange={(evt)=>setMaxCapacity(evt.target.valueAsNumber)} class={isValidInput(maxCapacity >= 0)}/>
							</div>
							<div class="col-span-3 sm:col-span-4">
								<label for="price_bed" class="block text-sm font-medium text-gray-700">{t('formLodging.price_bed')}</label>
								<input type="number" min="0" name="price_bed" id="price_bed" value={priceBed} onChange={(evt)=>setPriceBed(evt.target.valueAsNumber)} class={isValidInput(priceBed >= 0)}/>
							</div>
						</div>
						<div class="col-span-6 sm:col-span-4">
							<legend class="text-base font-medium text-gray-900">{t('formLodging.others_information_label')} </legend>

							<div class="col-span-6 sm:col-span-3">
								<label for="checkin" class="block text-sm font-medium text-gray-700">{t('formLodging.checkin')}</label>
								<input type="time" name="checkin" id="checkin" value={checkin} onChange={(evt)=>setCheckin(evt.target.value)} class={isValidInput(true)}/>
							</div>

							<div class="col-span-6 sm:col-span-4">
								<label for="closingHour" class="block text-sm font-medium text-gray-700">{t('formLodging.checkout')}</label>
								<input type="time" name="closingHour" id="closingHour" value={closingHour} onChange={(evt)=>setClosingHour(evt.target.value)} class={isValidInput(true)}/>
							</div>
						</div>
						
						<div class="col-span-6 ">
							<legend class="text-base font-medium text-gray-900">{t('formLodging.services_label')}</legend>
							<div class="col-span-6 sm:col-span-6 lg:col-span-2">
								<label for="breakfast" class="block text-sm font-medium text-gray-700">{t('formLodging.breakfast')}</label>
								<input type="number" min="0" name="breakfast" id="breakfast" value={breakfastPrice} onChange={(evt)=>setBreakfastPrice(evt.target.valueAsNumber)} class={isValidInput(breakfastPrice >= 0)}/>
							</div>

							<div class="col-span-6 sm:col-span-3 lg:col-span-2">
								<label for="dinner" class="block text-sm font-medium text-gray-700">{t('formLodging.dinner')}</label>
								<input type="number" min="0" name="dinner" id="dinner" value={dinnerPrice} onChange={(evt)=>setDinnerPrice(evt.target.valueAsNumber)} class={isValidInput(dinnerPrice >= 0)}/>
							</div>

							<div class="col-span-6 sm:col-span-3 lg:col-span-2">
								<label for="laundry" class="block text-sm font-medium text-gray-700">{t('formLodging.laundry')}</label>
								<input type="number" min="0" name="laundry" id="laundry" value={laundryPrice} onChange={(evt)=>setLaundryPrice(evt.target.valueAsNumber)} class={isValidInput(laundryPrice >= 0)}/>
							</div>
						</div>
						<div class="col-span-6 sm:col-span-4">
							<legend class="text-base font-medium text-gray-900">{t('formLodging.detail_label')}</legend>

							<Select
								label={t('formLodging.rentType')}
								value={rentType}
								onChange={({value})=>setRentType(value)}
								items={items}
							/>

							<Select
								label={t('formLodging.floor')}
								value={floor}
								onChange={({value})=>setFloor(value)}
								items={itemsFloor}
							/>
							
						</div>
						<div class="col-span-6">
							<div class="col-span-3 mt-4 space-y-4 ">
								<fieldset>
									<legend class="text-base font-medium text-gray-900">{t('formLodging.architecture_label')}</legend>
									<div class="col-span-3 sm:col-span-3 mt-4 space-y-4 ">
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="colonial" name="colonial" type="checkbox" checked={colonial} onChange={()=> setColonial(!colonial)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="colonial" class="font-medium text-gray-700">{t('formLodging.architecture.colonial')}</label>
											</div>
										</div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="tradicionalAmerican" name="tradicionalAmerican" type="checkbox" checked={tradicionalAmerican} onChange={()=> setTradicionalAmerican(!tradicionalAmerican)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="tradicionalAmerican" class="font-medium text-gray-700">{t('formLodging.architecture.american')}</label>
											</div>
										</div>
										<div>
											<div class="flex items-start">
												<div class="h-5 flex items-center">
													<input id="tradicionalFrench" name="tradicionalFrench" type="checkbox" checked={tradicionalFrench} onChange={()=> setTradicionalFrench(!tradicionalFrench)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
												</div>
												<div class="ml-3 text-sm">
													<label for="tradicionalFrench" class="font-medium text-gray-700">{t('formLodging.architecture.french')}</label>
												</div>
											</div>
										</div>
										<div>
											<div class="flex items-start">
												<div class="h-5 flex items-center">
													<input id="mansion" name="mansion" type="checkbox" checked={mansion} onChange={()=> setMansion(!mansion)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
												</div>
												<div class="ml-3 text-sm">
													<label for="mansion" class="font-medium text-gray-700">{t('formLodging.architecture.mansion')}</label>
												</div>
											</div>
										</div>
										<div>
											<div class="flex items-start">
												<div class="h-5 flex items-center">
													<input id="farm" name="farm" type="checkbox" checked={farm} onChange={()=> setFarm(!farm)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
												</div>
												<div class="ml-3 text-sm">
													<label for="farm" class="font-medium text-gray-700">{t('formLodging.architecture.farm')}</label>
												</div>
											</div>
										</div>
										<div>
											<div class="flex items-start">
												<div class="h-5 flex items-center">
													<input id="building" name="building" type="checkbox" checked={building} onChange={()=> setBuilding(!building)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
												</div>
												<div class="ml-3 text-sm">
													<label for="building" class="font-medium text-gray-700">{t('formLodging.architecture.building')}</label>
												</div>
											</div>
										</div>
									</div>
								</fieldset>
							</div>
							<div class="col-span-3 mt-4 space-y-4 ">
								<fieldset>
									<legend class="text-base font-medium text-gray-900">{t('formLodging.free_use')}</legend>
									<div class="col-span-3 sm:col-span-3 mt-4 space-y-4 ">
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="freeWasher" name="freeWasher" type="checkbox"  checked={freeWasher} onChange={()=> setFreeWasher(!freeWasher)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="freeWasher" class="font-medium text-gray-700">{t('formLodging.free_use.whasher')}</label>
											</div>
										</div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="freeKitchen" name="freeKitchen" type="checkbox" checked={freeKitchen} onChange={()=> setFreeKitchen(!freeKitchen)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="freeKitchen" class="font-medium text-gray-700">{t('formLodging.free_use.kitchen')}</label>
											</div>
										</div>
										<div>
											<div class="flex items-start">
												<div class="h-5 flex items-center">
													<input id="freeFan" name="freeFan" type="checkbox" checked={freeFan} onChange={()=> setFreeFan(!freeFan)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
												</div>
												<div class="ml-3 text-sm">
													<label for="freeFan" class="font-medium text-gray-700">{t('formLodging.free_use.ventilator')}</label>
												</div>
											</div>
										</div>
										<div>
											<div class="flex items-start">
												<div class="h-5 flex items-center">
													<input id="freeFilteredWater" name="freeFilteredWater" type="checkbox" checked={freeFilteredWater} onChange={()=> setFilteredWater(!freeFilteredWater)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
												</div>
												<div class="ml-3 text-sm">
													<label for="freeFilteredWater" class="font-medium text-gray-700">{t('formLodging.free_use.filtered_water')}</label>
												</div>
											</div>
										</div>
										<div>
											<div class="flex items-start">
												<div class="h-5 flex items-center">
													<input id="freeFridge" name="freeFridge" type="checkbox" checked={freeFridge} onChange={()=> setFreeFridge(!freeFridge)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
												</div>
												<div class="ml-3 text-sm">
													<label for="offers" class="font-medium text-gray-700">{t('formLodging.free_use.fridge')}</label>
												</div>
											</div>
										</div>
										<div>
											<div class="flex items-start">
												<div class="h-5 flex items-center">
													<input id="freeRefrigerator" name="freeRefrigerator" type="checkbox" checked={freeRefrigerator} onChange={()=> setFreeRefrigerator(!freeRefrigerator)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
												</div>
												<div class="ml-3 text-sm">
													<label for="freeRefrigerator" class="font-medium text-gray-700">{t('formLodging.free_use.refrigerator')}</label>
												</div>
											</div>
										</div>
										<div>
											<div class="flex items-start">
												<div class="h-5 flex items-center">
													<input id="freeTelephone" name="freeTelephone" type="checkbox" checked={freeTelephone} onChange={()=> setFreeTelephone(!freeTelephone)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
												</div>
												<div class="ml-3 text-sm">
													<label for="freeTelephone" class="font-medium text-gray-700">{t('formLodging.free_use.phone')}</label>
												</div>
											</div>
										</div>
									</div>
								</fieldset>
							</div>
						</div>
						<div class="col-span-6">
							<fieldset>
								<legend class="text-base font-medium text-gray-900"> 
									{t('formLodging.features')}
								</legend>
								<div class="col-span-3 mt-4 space-y-4 ">
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="private_entrance" name="private_entrance" checked={private_entrance} onChange={()=> setPrivate_entrance(!private_entrance)} type="checkbox" class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="private_entrance" class="font-medium text-gray-700">
												{t('formLodging.features.private_entrance')}
											</label>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="elevator" name="elevator" type="checkbox" checked={elevator} onChange={()=> setElevator(!elevator)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="elevator" class="font-medium text-gray-700">
												{t('formLodging.features.elevator')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="handicapped_accessible" name="handicapped_accessible" type="checkbox" checked={handicapped_accessible} onChange={()=> setHandicapped_accessible(!handicapped_accessible)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="offers" class="font-medium text-gray-700">
													{t('formLodging.features.handicapped_accessible')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="terrace" name="terrace" type="checkbox" checked={terrace} onChange={()=> setTerrace(!terrace)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="terrace" class="font-medium text-gray-700">
													{t('formLodging.features.terrace')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="balcony" name="balcony" type="checkbox" checked={balcony} onChange={()=> setBalcony(!balcony)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="balcony" class="font-medium text-gray-700">
													{t('formLodging.features.balcony')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="tv" name="tv" type="checkbox" checked={tv} onChange={()=> setTV(!tv)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="tv" class="font-medium text-gray-700">
													{t('formLodging.features.tv')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="hot_water" name="hot_water" type="checkbox" checked={hot_water} onChange={()=> setHot_water(!hot_water)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="hot_water" class="font-medium text-gray-700">
													{t('formLodging.features.hot_water')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="air_conditioning" name="air_conditioning" type="checkbox" checked={air_conditioning} onChange={()=> setAir_conditioning(!air_conditioning)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="air_conditioning" class="font-medium text-gray-700">
													{t('formLodging.features.air_conditioning')}
												</label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-span-3 mt-4 space-y-4 ">
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="ventilator" name="ventilator" type="checkbox" checked={ventilator} onChange={()=> setVentilator(!ventilator)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="ventilator" class="font-medium text-gray-700">
												{t('formLodging.features.ventilator')}
											</label>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="fridge" name="fridge" type="checkbox" checked={fridge} onChange={()=> setFridge(!fridge)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="fridge" class="font-medium text-gray-700">
													{t('formLodging.features.fridge')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="refrigerator" name="refrigerator" checked={refrigerator} onChange={()=> setRefrigerator(!refrigerator)} type="checkbox" class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="refrigerator" class="font-medium text-gray-700">
													{t('formLodging.features.refrigerator')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="filtered_water" name="filtered_water" checked={filtered_water} onChange={()=> setFiltered_water(!filtered_water)} type="checkbox" class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="filtered_water" class="font-medium text-gray-700">
													{t('formLodging.features.filtered_water')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="phone" name="phone" type="checkbox" checked={phone} onChange={()=> setPhone(!phone)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="phone" class="font-medium text-gray-700">
													{t('formLodging.features.phone')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="security_safe" name="security_safe" type="checkbox" checked={security_safe} onChange={()=> setSecurity_safe(!security_safe)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="security_safe" class="font-medium text-gray-700">
													{t('formLodging.features.security_safe')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="jacuzzi" name="jacuzzi" type="checkbox" checked={jacuzzi} onChange={()=> setJacuzzi(!jacuzzi)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="jacuzzi" class="font-medium text-gray-700">
													{t('formLodging.features.jacuzzi')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="hair_dryer" name="hair_dryer" type="checkbox" checked={hair_dryer} onChange={()=> setHair_dryer(!hair_dryer)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="hair_dryer" class="font-medium text-gray-700">
													{t('formLodging.features.hair_dryer')}
												</label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-span-3 mt-4 space-y-4 ">
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="wifi" name="wifi" type="checkbox" checked={wifi} onChange={()=> setWifi(!wifi)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="wifi" class="font-medium text-gray-700">
												{t('formLodging.features.wifi')}
											</label>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="own_kitchen" name="own_kitchen" type="checkbox" checked={own_kitchen} onChange={()=> setOwn_kitchen(!own_kitchen)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="own_kitchen" class="font-medium text-gray-700">
													{t('formLodging.features.own_kitchen')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="tan_chamber" name="tan_chamber" type="checkbox" checked={tan_chamber} onChange={()=> setTan_chamber(!tan_chamber)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="tan_chamber" class="font-medium text-gray-700">
													{t('formLodging.features.tan_chamber')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="closet" name="closet" type="checkbox" checked={closet} onChange={()=> setCloset(!closet)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="closet" class="font-medium text-gray-700">
													{t('formLodging.features.closet')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="linens" name="linens" type="checkbox" checked={linens} onChange={()=> setLinens(!linens)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="linens" class="font-medium text-gray-700">
													{t('formLodging.features.linens')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="towels" name="towels" type="checkbox" checked={towels} onChange={()=> setTowels(!towels)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="towels" class="font-medium text-gray-700">
													{t('formLodging.features.towels')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="toilet_paper" name="toilet_paper" type="checkbox" checked={toilet_paper} onChange={()=> setToilet_paper(!toilet_paper)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="toilet_paper" class="font-medium text-gray-700">
													{t('formLodging.features.toilet_paper')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="bar" name="bar" type="checkbox" checked={bar} onChange={()=> setBar(!bar)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="toilet_paper" class="font-medium text-gray-700">
													{t('formLodging.features.bar')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="service_24h" name="service_24h" type="checkbox" checked={service_24h} onChange={()=> setService24h(!service_24h)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="service_24h" class="font-medium text-gray-700">
													{t('formLodging.features.service_24h')}
												</label>
											</div>
										</div>
									</div>
								</div>
							</fieldset>
						</div>
						<div class="col-span-6 sm:col-span-4">
							<label for="about" class="block text-sm font-medium text-gray-700">
								{t('formLodging.features.others')}
							</label>
							<div class="mt-1">
								<textarea id="about" name="about" rows="3" value={other_feaures} onChange={(evt)=>setOther_features(evt.target.value)} class="shadow-sm focus:ring-blue-500 focus:border-blue-500 mt-1 block w-full sm:text-sm border border-gray-300 rounded-md" placeholder=""></textarea>
							</div>
							<p class="mt-2 text-sm text-gray-500">
								{t('formLodging.features.others.description')}
							</p>
						</div>
						<div class="col-span-6 sm:col-span-4">
							<fieldset>
								<legend class="text-base font-medium text-gray-900">{t('formLodging.panorama')} </legend>
								<div class="col-span-3 sm:col-span-3 mt-4 space-y-4 ">
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="seaview" name="seaview" type="checkbox" checked={seaview} onChange={()=> setSeaview(!seaview)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="seaview" class="font-medium text-gray-700">
												{t('formLodging.panorama.seaview')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="garden" name="garden" type="checkbox" checked={garden} onChange={()=> setGarden(!garden)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="garden" class="font-medium text-gray-700">
													{t('formLodging.panorama.garden')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="street_view" name="street_view" type="checkbox" checked={street_view} onChange={()=> setStreet_view(!street_view)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="street_view" class="font-medium text-gray-700">
													{t('formLodging.panorama.street_view')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="country_view" name="country_view" type="checkbox" checked={country_view} onChange={()=> setCountry_view(!country_view)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="country_view" class="font-medium text-gray-700">
													{t('formLodging.panorama.country_view')}
												</label>
											</div>
										</div>
									</div>
								</div>
							</fieldset>
						</div>
						<div class="col-span-6">
							<ImageUploader maxNumber={5} />
						</div>
					</div>
				</div>
				<div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
					<button type="button" onClick={()=>save()} class={isValidForm(validateForm())}>{t('buttons.save')}</button>
				</div>
			</div>
		</form>

	);
}

export default FormLodging;