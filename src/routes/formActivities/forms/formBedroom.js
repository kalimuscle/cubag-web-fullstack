import { h } from 'preact';
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'preact-router/match';
import {useEffect, useState} from "preact/hooks";
import Select from '../../../components/select';
import { useContext } from 'preact/hooks';
import { TranslateContext } from '@denysvuika/preact-translate';
import ImageUploader from '../../../components/imageUpload';
import { gql, useLazyQuery, useMutation } from '@apollo/client';
import Loading from '../../../components/loading';
import {ErrorGraphql} from '../../../components/errorModal';

const CREATE_BEDROOM_MUTATION = gql`
  mutation createBedroom($input: CreateBedroomInput!){
	createBedroom(input: $input){
		id
	}
}
`;

const BEDROOM_BY_ID_QUERY = gql`
  query getBedroom($id: ID!){
	getBedroom(id: $id){
		id
		lodgingId
		images
		floor
		beds
		other_feaures
		private_entrance
		elevator
		handicapped_accessible
		terrace
		balcony
		tv
		hot_water
		air_conditioning

		ventilator
		fridge
		refrigerator
		filtered_water
		phone
		security_safe
		jacuzzi
		hair_dryer

		wifi
		own_kitchen
		tan_chamber
		linens
		towels
		closet
		toilet_paper

		seaview
		garden
		street_view
		country_view

	}
}
`;


const FormBedroom = ({id, follow, action}) => {
	const { setLang, t, lang } = useContext(TranslateContext);
	const [queryFunction, { data, loading, error }] = useLazyQuery(BEDROOM_BY_ID_QUERY, {
		variables: { id },
		pollInterval: 500,
	  });
	const [mutateFunction, {data: mdata, loading: mloading, error: merror}] = useMutation(CREATE_BEDROOM_MUTATION, {onCompleted: (data) => {  console.log({data});  }});
	const dispatch = useDispatch();

	const [lodgingId, setLodgingId] = useState(id);

	const [images, setImages] = useState([]);

	const [floor, setFloor] = useState('first-floor');
	const [beds, setBeds] = useState(1);

	const [private_entrance, setPrivate_entrance] = useState(false);
	const [elevator, setElevator] = useState(false);
	const [handicapped_accessible, setHandicapped_accessible] = useState(false);
	const [terrace, setTerrace] = useState(false);
	const [balcony, setBalcony] = useState(false);
	const [tv, setTV] = useState(false);
	const [hot_water, setHot_water] = useState(false);
	const [air_conditioning, setAir_conditioning] = useState(false);

	const [ventilator, setVentilator] = useState(false);
	const [fridge, setFridge] = useState(false);
	const [refrigerator, setRefrigerator] = useState(false);
	const [filtered_water, setFiltered_water] = useState(false);
	const [phone, setPhone] = useState(false);
	const [security_safe, setSecurity_safe] = useState(false);
	const [jacuzzi, setJacuzzi] = useState(false);
	const [hair_dryer, setHair_dryer] = useState(false);

	const [wifi, setWifi] = useState(false);
	const [own_kitchen, setOwn_kitchen] = useState(false);
	const [tan_chamber, setTan_chamber] = useState(false);
	const [linens, setLinens] = useState(false);
	const [towels, setTowels] = useState(false);
	const [closet, setCloset] = useState(false);
	const [toilet_paper, setToilet_paper] = useState(false);

	const [other_feaures, setOther_features] = useState('');

	const [seaview, setSeaview] = useState(false);
	const [garden, setGarden] = useState(false);
	const [street_view, setStreet_view] = useState(false);
	const [country_view, setCountry_view] = useState(false);


	useEffect(async() => {
		if(id !== '' && action == 'edit-bedroom'){

			const query = await queryFunction({
				variables:{
					id
				}
			});

			setLodgingId(query.data.getBedroom.lodgingId);
			setImages(query.data.getBedroom.images);
			setFloor(query.data.getBedroom.floor);
			setBeds(query.data.getBedroom.beds);

			setPrivate_entrance(query.data.getBedroom.private_entrance);
			setElevator(query.data.getBedroom.elevator);
			setHandicapped_accessible(query.data.getBedroom.handicapped_accessible);
			setTerrace(query.data.getBedroom.terrace);
			setBalcony(query.data.getBedroom.balcony);
			setTV(query.data.getBedroom.tv);
			setHot_water(query.data.getBedroom.hot_water);
			setAir_conditioning(query.data.getBedroom.air_conditioning);

			setVentilator(query.data.getBedroom.ventilator);
			setFridge(query.data.getBedroom.fridge);
			setRefrigerator(query.data.getBedroom.refrigerator);
			setFiltered_water(query.data.getBedroom.filtered_water);
			setPhone(query.data.getBedroom.phone);
			setSecurity_safe(query.data.getBedroom.security_safe);
			setJacuzzi(query.data.getBedroom.jacuzzi);
			setHair_dryer(query.data.getBedroom.hair_dryer);

			setWifi(query.data.getBedroom.wifi);
			setOwn_kitchen(query.data.getBedroom.own_kitchen);
			setTan_chamber(query.data.getBedroom.tan_chamber);
			setLinens(query.data.getBedroom.linens);
			setTowels(query.data.getBedroom.towels);
			setCloset(query.data.getBedroom.closet);
			setToilet_paper(query.data.getBedroom.toilet_paper);

			setOther_features(query.data.getBedroom.other_feaures);
			setSeaview(query.data.getBedroom.seaview);
			setGarden(query.data.getBedroom.garden);
			setStreet_view(query.data.getBedroom.street_view);
			setCountry_view(query.data.getBedroom.country_view);
		
		}
		
	  },[]);

	if (loading || mloading) return (
		<Loading />
	);

  	if (error || merror) return <ErrorGraphql follow={(step)=> follow(step)}/>;


	  const save = async () => { console.log(action)

		console.log('lodgingId:', lodgingId);
	
		const mutation = await mutateFunction({ 
			variables: { 
				input: {
					id: action == 'create-bedroom' ? undefined : id,
					lodgingId,
					floor,
					beds,
					other_feaures,
					private_entrance,
					elevator,
					handicapped_accessible,
					terrace,
					balcony,
					tv,
					hot_water,
					air_conditioning,
					ventilator,
					fridge,
					refrigerator,
					filtered_water,
					phone,
					security_safe,
					jacuzzi,
					hair_dryer,
					wifi,
					own_kitchen,
					tan_chamber,
					linens,
					towels,
					closet,
					toilet_paper,
					seaview,
					garden,
					street_view,
					country_view,
					images: []
				}
			} 
		});
	
		follow(`bedrooms-list;${lodgingId}`);
		
	}
	
	const items = [
		{
			key: 'first-floor',
			name: lang == 'en' ? 'First Floor' : 'Primer piso'
		},
		{
			key: 'second-floor',
			name: lang == 'en' ? 'Second Floor' : 'Segundo piso'
		},
		{
			key: 'third-floor',
			name: lang == 'en' ? 'Third Floor' : 'Tercer piso'
		},
		{
			key: 'fouth-floor',
			name: lang == 'en' ? 'Fourth Floor' : 'Cuarto piso'
		},
		{
			key: 'fifth-floor',
			name: lang == 'en' ? 'Fifth Floor' : 'Quinto piso'
		}
	];

	return (
		<form action="#" method="POST">
			<div class="shadow sm:rounded-md sm:overflow-hidden">
				<div class="bg-white py-6 px-4 space-y-6 sm:p-6">
					<div>
						<h3 class="text-lg leading-6 font-medium text-gray-900">
							{t('formBedroom.title')}
						</h3>
						<p class="mt-1 text-sm text-gray-500">
							{t('formBedroom.description')}
						</p>
					</div>

					<div class="grid grid-cols-6 gap-6">
						<div class="col-span-3">
							<Select
								label="Floor"
								value={floor}
								onChange={({value})=>setFloor(value)}
								items={items}/>
						</div>
						
						<div class="col-span-6 sm:col-span-4">

							<div class="col-span-6 sm:col-span-3">
								<label for="last-name" class="block text-sm font-medium text-gray-700">
									{t('formBedroom.bedNumber.input')}
								</label>
								<input type="number" name="beds" id="beds" value={beds} onChange={(evt)=>setBeds(evt.target.valueAsNumber)} min="1" class="mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm"/>
							</div>
						</div>
						<div class="col-span-6 sm:col-span-4">
							<fieldset>
								<legend class="text-base font-medium text-gray-900"> 
									{t('formBedroom.features')}
								</legend>
								<div class="col-span-3 sm:col-span-3 mt-4 space-y-4 ">
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="private_entrance" name="private_entrance" checked={private_entrance} onChange={()=> setPrivate_entrance(!private_entrance)} type="checkbox" class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="private_entrance" class="font-medium text-gray-700">
												{t('formBedroom.features.private_entrance')}
											</label>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="elevator" name="elevator" type="checkbox" checked={elevator} onChange={()=> setElevator(!elevator)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="elevator" class="font-medium text-gray-700">
												{t('formBedroom.features.elevator')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="handicapped_accessible" name="handicapped_accessible" type="checkbox" checked={handicapped_accessible} onChange={()=> setHandicapped_accessible(!handicapped_accessible)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="offers" class="font-medium text-gray-700">
													{t('formBedroom.features.handicapped_accessible')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="terrace" name="terrace" type="checkbox" checked={terrace} onChange={()=> setTerrace(!terrace)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="terrace" class="font-medium text-gray-700">
													{t('formBedroom.features.terrace')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="balcony" name="balcony" type="checkbox" checked={balcony} onChange={()=> setBalcony(!balcony)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="balcony" class="font-medium text-gray-700">
													{t('formBedroom.features.balcony')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="tv" name="tv" type="checkbox" checked={tv} onChange={()=> setTV(!tv)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="tv" class="font-medium text-gray-700">
													{t('formBedroom.features.tv')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="hot_water" name="hot_water" type="checkbox" checked={hot_water} onChange={()=> setHot_water(!hot_water)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="hot_water" class="font-medium text-gray-700">
													{t('formBedroom.features.hot_water')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="air_conditioning" name="air_conditioning" type="checkbox" checked={air_conditioning} onChange={()=> setAir_conditioning(!air_conditioning)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="air_conditioning" class="font-medium text-gray-700">
													{t('formBedroom.features.air_conditioning')}
												</label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-span-3 sm:col-span-3 mt-4 space-y-4 ">
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="ventilator" name="ventilator" type="checkbox" checked={ventilator} onChange={()=> setVentilator(!ventilator)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="ventilator" class="font-medium text-gray-700">
												{t('formBedroom.features.ventilator')}
											</label>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="fridge" name="fridge" type="checkbox" checked={fridge} onChange={()=> setFridge(!fridge)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="fridge" class="font-medium text-gray-700">
													{t('formBedroom.features.fridge')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="refrigerator" name="refrigerator" checked={refrigerator} onChange={()=> setRefrigerator(!refrigerator)} type="checkbox" class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="refrigerator" class="font-medium text-gray-700">
													{t('formBedroom.features.refrigerator')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="filtered_water" name="filtered_water" checked={filtered_water} onChange={()=> setFiltered_water(!filtered_water)} type="checkbox" class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="filtered_water" class="font-medium text-gray-700">
													{t('formBedroom.features.filtered_water')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="phone" name="phone" type="checkbox" checked={phone} onChange={()=> setPhone(!phone)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="phone" class="font-medium text-gray-700">
													{t('formBedroom.features.phone')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="security_safe" name="security_safe" type="checkbox" checked={security_safe} onChange={()=> setSecurity_safe(!security_safe)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="security_safe" class="font-medium text-gray-700">
													{t('formBedroom.features.security_safe')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="jacuzzi" name="jacuzzi" type="checkbox" checked={jacuzzi} onChange={()=> setJacuzzi(!jacuzzi)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="jacuzzi" class="font-medium text-gray-700">
													{t('formBedroom.features.jacuzzi')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="hair_dryer" name="hair_dryer" type="checkbox" checked={hair_dryer} onChange={()=> setHair_dryer(!hair_dryer)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="hair_dryer" class="font-medium text-gray-700">
													{t('formBedroom.features.hair_dryer')}
												</label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-span-3 sm:col-span-3 mt-4 space-y-4 ">
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="wifi" name="wifi" type="checkbox" checked={wifi} onChange={()=> setWifi(!wifi)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="wifi" class="font-medium text-gray-700">
												{t('formBedroom.features.wifi')}
											</label>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="own_kitchen" name="own_kitchen" type="checkbox" checked={own_kitchen} onChange={()=> setOwn_kitchen(!own_kitchen)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="own_kitchen" class="font-medium text-gray-700">
													{t('formBedroom.features.own_kitchen')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="tan_chamber" name="tan_chamber" type="checkbox" checked={tan_chamber} onChange={()=> setTan_chamber(!tan_chamber)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="tan_chamber" class="font-medium text-gray-700">
													{t('formBedroom.features.tan_chamber')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="closet" name="closet" type="checkbox" checked={closet} onChange={()=> setCloset(!closet)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="closet" class="font-medium text-gray-700">
													{t('formBedroom.features.closet')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="linens" name="linens" type="checkbox" checked={linens} onChange={()=> setLinens(!linens)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="linens" class="font-medium text-gray-700">
													{t('formBedroom.features.linens')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="towels" name="towels" type="checkbox" checked={towels} onChange={()=> setTowels(!towels)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="towels" class="font-medium text-gray-700">
													{t('formBedroom.features.towels')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="toilet_paper" name="toilet_paper" type="checkbox" checked={toilet_paper} onChange={()=> setToilet_paper(!toilet_paper)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="toilet_paper" class="font-medium text-gray-700">
													{t('formBedroom.features.toilet_paper')}
												</label>
											</div>
										</div>
									</div>
								</div>
							</fieldset>
						</div>
						<div class="col-span-6 sm:col-span-4">
							<label for="about" class="block text-sm font-medium text-gray-700">
								{t('formBedroom.features.others')}
							</label>
							<div class="mt-1">
								<textarea id="about" name="about" rows="3" value={other_feaures} onChange={(evt)=>setOther_features(evt.target.value)} class="shadow-sm focus:ring-blue-500 focus:border-blue-500 mt-1 block w-full sm:text-sm border border-gray-300 rounded-md" placeholder=""></textarea>
							</div>
							<p class="mt-2 text-sm text-gray-500">
								{t('formBedroom.features.others.description')}
							</p>
						</div>

						<div class="col-span-6 sm:col-span-4">
							<fieldset>
								<legend class="text-base font-medium text-gray-900">{t('formBedroom.panorama')} </legend>
								<div class="col-span-3 sm:col-span-3 mt-4 space-y-4 ">
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="seaview" name="seaview" type="checkbox" checked={seaview} onChange={()=> setSeaview(!seaview)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="seaview" class="font-medium text-gray-700">
												{t('formBedroom.panorama.seaview')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="garden" name="garden" type="checkbox" checked={garden} onChange={()=> setGarden(!garden)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="garden" class="font-medium text-gray-700">
													{t('formBedroom.panorama.garden')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="street_view" name="street_view" type="checkbox" checked={street_view} onChange={()=> setStreet_view(!street_view)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="street_view" class="font-medium text-gray-700">
													{t('formBedroom.panorama.street_view')}
												</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="country_view" name="country_view" type="checkbox" checked={country_view} onChange={()=> setCountry_view(!country_view)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="country_view" class="font-medium text-gray-700">
													{t('formBedroom.panorama.country_view')}
												</label>
											</div>
										</div>
									</div>
								</div>
							</fieldset>
						</div>
						<div class="col-span-6">
							<ImageUploader maxNumber={5}/>
						</div>
					</div>
				</div>
				<div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
					<button type="button" onClick={()=>save()} class="bg-blue-600 border border-transparent rounded-md shadow-sm py-2 px-4 inline-flex justify-center text-sm font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500">
						{t('buttons.save')}
					</button>
				</div>
			</div>
		</form>

	);
}

export default FormBedroom;