import { h } from 'preact';
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'preact-router/match';
import {useEffect, useState, useContext} from "preact/hooks";
import ErrorModal from '../../../components/errorModal';
import { TranslateContext } from '@denysvuika/preact-translate';
import PhoneInput from 'react-phone-input-2';
import Select from '../../../components/select';
import { gql, useMutation } from '@apollo/client';
import { create_people_id} from '../../../store/actions/';

// Define mutation
const CREATE_PROFILE_MUTATION = gql`
	mutation createpeople($input: CreatePeopleInput!){
		createPeople(input: $input){
			id
		}
	}
`;

const FormPersonalData = ({follow}) => {
	const [mutateFunction, { data, loading, error }] = useMutation(CREATE_PROFILE_MUTATION);
	const { setLang, t, lang } = useContext(TranslateContext);
	const [menu, setMenu] = useState(false);

	const dispatch = useDispatch();
	const tourCreatorId = useSelector(state=> state.tourCreatorId);

	const [fullName, setFullName] = useState('');
	const [email, setEmail] = useState('');
	const [phone1, setPhone1] = useState('');
	const [phone2, setPhone2] = useState('');
	const [address, setAddress] = useState('');
	const [city, setCity] = useState('');
	const [state, setState] = useState('habana');
	const [zip, setZip] = useState('');
	const [nit, setNit] = useState('');

	const [swift, setSwift] = useState(false);
	const [cash, setCash] = useState(false);
	const [card, setCard] = useState(false);
	const [cryptocurrency, setCryptocurrency] = useState(false);

	if (loading) return 'Submitting';
  	if (error) return `Submission error! ${error.message}`;

	const states = [
		{
			key: 'pinar-del-rio',
			name: 'Pinar del Rio'
		},
		{
			key: 'artemisa',
			name: 'Artemisa'
		},
		{
			key: 'habana',
			name: 'Habana'
		},
		{
			key: 'mayabeque',
			name: 'Mayabeque'
		},
		{
			key: 'matanzas',
			name: 'Matanzas'
		},
		{
			key: 'cienfuegos',
			name: 'Cienfuegos'
		},
		{
			key: 'santi-spiritus',
			name: 'Santi Spiritus'
		},
		{
			key: 'villa-clara',
			name: 'Villa Clara'
		},
		{
			key: 'ciego-de-avila',
			name: 'Ciego de Avila'
		},
		{
			key: 'camaguey',
			name: 'Camaguey'
		},
		{
			key: 'holguin',
			name: 'Holguin'
		},
		{
			key: 'granma',
			name: 'Granma'
		},
		{
			key: 'santiago-de-cuba',
			name: 'Santiago de Cuba'
		},
		{
			key: 'guantanamo',
			name: 'Guantanamo'
		},
		{
			key: 'isla-de-la-juventud',
			name: 'Isla de la Juventud'
		},
	];

	const save = async () => {
		const mutation = await mutateFunction({
			variables: { 
				input:{
					tourCreatorId,
					fullName,
					email,
					phone1,
					phone2,
					address,
					city,
					state,
					zip,
					nit,
					swift,
					cash,
					card,
					cryptocurrency
				}	
			} 
		});
		console.log({mutation});
		dispatch(create_people_id(mutation.data.createPeople.id));
		follow('options');
	}

	const validateEmail = (value) => {
		const expEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		const pattern_email = new RegExp(expEmail);

		return pattern_email.test(value)
	}

	const validateForm = () => (
		fullName.length >= 8 &&
		validateEmail(email) &&
		phone1.length == 10 &&
		address.length > 8 &&
		city.length > 8 &&
		(!isNaN(zip) && parseInt(zip) > 100)&&
		nit.length > 8 &&
		(
			swift ||
			cash ||
			card ||
			cryptocurrency
		)
	)
	

	const isValidInput = (value) => {
		const inputClass = "mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm";
		const invalidClass = "mt-1 block w-full border border-red-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-red-500 focus:border-red-500 sm:text-sm"

		return value ? inputClass : invalidClass
	}

	const isValidForm = (value) => {
		const buttonClass = "bg-blue-600 border border-transparent rounded-md shadow-sm py-2 px-4 inline-flex justify-center text-sm font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500";
		const disabledClass = "opacity-50 bg-blue-600 border border-transparent rounded-md shadow-sm py-2 px-4 inline-flex justify-center text-sm font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500";
		
		return value ? buttonClass : disabledClass
	}

	return (
		<form action="#" method="POST">
			<div class="shadow sm:rounded-md sm:overflow-hidden">
				<div class="bg-white py-6 px-4 space-y-6 sm:p-6">
					<div>
						<h3 class="text-lg leading-6 font-medium text-gray-900">{t('formPersonalData.title')}</h3>
					</div>

					<div class="grid grid-cols-6 gap-6">
						<div class="col-span-6 sm:col-span-3">
							<label for="full-name" class="block text-sm font-medium text-gray-700">fullName<span class="text-red-500">*</span></label>
							<input type="text" name="fulll-name" id="full-name" value={fullName} onChange={(evt)=>setFullName(evt.target.value)} class={isValidInput(fullName.length == 0 || fullName.length >= 8)}/>
						</div>

						<div class="col-span-6 sm:col-span-4">
							<label for="email-address" class="block text-sm font-medium text-gray-700">{t('formPersonalData.email')} <span class="text-red-500">*</span></label>
							<input type="email" name="email-address" id="email-address" autocomplete="email"  value={email} onChange={(evt)=>setEmail(evt.target.value)} class={isValidInput(email.length == 0 || validateEmail(email))}/>
						</div>

						<div class="col-span-6 sm:col-span-3">
							<label for="last-name" class="block text-sm font-medium text-gray-700">{t('formPersonalData.phone1')}</label>
							<PhoneInput
								country={'cu'}
								value={phone1}
								onChange={(value)=>setPhone1(value)}
								inputClass={isValidInput(phone1.length == 0 || phone1.length == 10)}
								specialLabel=""
								placeholder='+53 XXX XXX XX'
							/>
						</div>

						<div class="col-span-6 sm:col-span-3">
							<label for="last-name" class="block text-sm font-medium text-gray-700">{t('formPersonalData.phone2')}</label>
							<PhoneInput
								country={'cu'}
								value={phone2}
								onChange={(value)=>setPhone2(value)}
								inputClass={isValidInput(phone2.length == 0 || phone2.length == 10)}
								specialLabel=""
								placeholder='+53 XXX XXX XX'
							/>
						</div>

						<div class="col-span-6">
							<label for="street-address" class="block text-sm font-medium text-gray-700">{t('formPersonalData.street_address')} <span class="text-red-500">*</span></label>
							<input type="text" name="street-address" id="street-address" autocomplete="street-address" value={address} onChange={(evt)=>setAddress(evt.target.value)} class={isValidInput(address.length == 0 || address.length > 8)}/>
						</div>

						<div class="col-span-6 sm:col-span-6 lg:col-span-2">
							<label for="city" class="block text-sm font-medium text-gray-700">{t('formPersonalData.city')} <span class="text-red-500">*</span></label>
							<input type="text" name="city" id="city" autocomplete="address-level2" value={city} onChange={(evt)=>setCity(evt.target.value)} class={isValidInput(city.length == 0 || city.length > 8)}/>
						</div>

						<div class="col-span-6 sm:col-span-3 lg:col-span-2">
							<Select
								required={true}
								label={t('formPersonalData.state')}
								value={state}
								onChange={({value})=>setState(value)}
								items={states}
							/>
						</div>

						<div class="col-span-6 sm:col-span-3 lg:col-span-2">
							<label for="postal-code" class="block text-sm font-medium text-gray-700">{t('formPersonalData.zip')} <span class="text-red-500">*</span></label>
							<input type="text" name="postal-code" id="postal-code" autocomplete="postal-code" value={zip} onChange={(evt)=>setZip(evt.target.value)} class={isValidInput( zip.length == 0|| (!isNaN(zip) && parseInt(zip) > 100))}/>
						</div>

						<div class="col-span-6 sm:col-span-3 lg:col-span-2">
							<label for="nit" class="block text-sm font-medium text-gray-700">{t('formPersonalData.nit')}<span class="text-red-500">*</span></label>
							<input type="text" name="nit" id="nit" value={nit} onChange={(evt)=>setNit(evt.target.value)} class={isValidInput(nit.length == 0 || nit.length >= 8)}/>
						</div>
						<div class="col-span-6">
							<fieldset>
								<legend class="text-base font-medium text-gray-900">{t('formPersonalData.payments')} </legend>
								<div class="col-span-3 sm:col-span-3 mt-4 space-y-4 ">
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="swift" name="swift" type="checkbox" value={swift} onChange={()=> setSwift(!swift)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="swift" class="font-medium text-gray-700">{t('formPersonalData.payments.swift')}</label>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="cash" name="cash" type="checkbox" value={cash} onChange={()=> setCash(!cash)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="cash" class="font-medium text-gray-700">{t('formPersonalData.payments.cash')}</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="card" name="card" type="checkbox" value={card} onChange={()=> setCard(!card)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="card" class="font-medium text-gray-700">{t('formPersonalData.payments.card')}</label>
											</div>
										</div>
									</div>
									<div>
										<div class="flex items-start">
											<div class="h-5 flex items-center">
												<input id="cryptocurrency" name="cryptocurrency" type="checkbox" value={cryptocurrency} onChange={()=> setCryptocurrency(!cryptocurrency)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
											</div>
											<div class="ml-3 text-sm">
												<label for="cryptocurrency" class="font-medium text-gray-700">{t('formPersonalData.payments.cryptocurrency')}</label>
											</div>
										</div>
									</div>
								</div>
							</fieldset>
						</div>
					</div>
					
				</div>
				<div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
					<button type="button" onClick={()=>save()} class={isValidForm(validateForm())}>{t('buttons.save')}</button>
				</div>
			</div>
		</form>

	);
}

export default FormPersonalData;