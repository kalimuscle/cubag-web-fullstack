import { h } from 'preact';
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'preact-router/match';
import {useEffect, useState, useContext} from "preact/hooks";
import { TranslateContext } from '@denysvuika/preact-translate';
import ImageUploader from '../../../components/imageUpload';
import PhoneInput from 'react-phone-input-2';
import { gql, useLazyQuery, useMutation } from '@apollo/client';
import Loading from '../../../components/loading';
import {ErrorGraphql} from '../../../components/errorModal';

const CREATE_ACTIVITY_MUTATION = gql`
  mutation createActivity($input: CreateActivityInput!){
	createActivity(input: $input){
		id
	}
}
`;

const ACTIVITY_BY_ID_QUERY = gql`
  query getActivity($id: ID!){
	getActivity(id: $id){
		id
		images

		name
		description
		phone1
		phone2
		priceDay
		pricePerson
		latitude
		longitude
		duration
		restaurant
		bar
		club
		monument
		museum
		gallery
		theatre
		cinema
		square
		park
		festival
		concert
		indoor
		outdoor
		reservation
		horse
		diving
		walking
		running
		trekking
		cyclism
		sports
		rum
		cigar
		coffee
		religion
		kitchen
		architecture
		nightLife
		marketPlace
		river
		lake
		sea
		beachSand
		beachStone
		mountain
		country
		nature
		music
		dance
		visualArts
		education
		political
		history
		localHistory
	}
}
`;


const FormActivities = ({id, follow}) => {
	const { setLang, t, lang } = useContext(TranslateContext);
	const [queryFunction, { data, loading, error }] = useLazyQuery(ACTIVITY_BY_ID_QUERY, {
		variables: { id },
		pollInterval: 500,
	  });
	const [mutateFunction, {data: mdata, loading: mloading, error: merror}] = useMutation(CREATE_ACTIVITY_MUTATION, {onCompleted: (data) => {  console.log({data});  }});
	const dispatch = useDispatch();

	const tourCreatorId = useSelector(state => state.tourCreatorId);
	const peopleId = useSelector(state => state.peopleId);

	const [images, setImages] = useState([]);

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [phone1, setPhone1] = useState('');
	const [phone2, setPhone2] = useState('');
	const [priceDay, setPriceDay] = useState(0);
	const [pricePerson, setPricePerson] = useState(0);
	const [latitude, setLatitude] = useState('');
	const [longitude, setLongitude] = useState('');
	const [duration, setDuration] = useState(0);

	const [restaurant, setRestaurant] = useState(false);
	const [bar, setBar] = useState(false);
	const [club, setClub] = useState(false);
	const [monument, setMonument] = useState(false);
	const [museum, setMuseum] = useState(false);
	const [gallery, setGallery] = useState(false);
	const [theatre, setTheatre] = useState(false);
	const [cinema, setCinema] = useState(false);
	const [square, setSquare] = useState(false);
	const [park, setPark] = useState(false);
	const [festival, setFestival] = useState(false);
	const [concert, setConcert] = useState(false);
	const [indoor, setIndoor] = useState(false);
	const [outdoor, setOutdoor] = useState(false);
	const [reservation, setReservation] = useState(false);

	const [horse, setHorse] = useState(false);
	const [diving, setDiving] = useState(false);
	const [walking, setWalking] = useState(false);
	const [running, setRunning] = useState(false);
	const [trekking, setTrekking] = useState(false);
	const [cyclism, setCyclism] = useState(false);
	const [sports, setSports] = useState(false);
	const [rum, setRum] = useState(false);
	const [cigar, setCigar] = useState(false);
	const [coffee, setCoffe] = useState(false);
	const [religion, setReligion] = useState(false);
	const [kitchen, setKitchen] = useState(false);
	const [architecture, setArchitecture] = useState(false);
	const [nightLife, setNightLife] = useState(false);
	const [marketPlace, setMarketPlace] = useState(false);
	const [river, setRiver] = useState(false);

	const [lake, setLake] = useState(false);
	const [sea, setSea] = useState(false);
	const [beachSand, setBeachSand] = useState(false);
	const [beachStone, setBeachStone] = useState(false);
	const [mountain, setMountain] = useState(false);
	const [country, setCountry] = useState(false);
	const [nature, setNature] = useState(false);
	const [music, setMusic] = useState(false);
	const [dance, setDance] = useState(false);
	const [visualArts, setVisualArts] = useState(false);
	const [education, setEducation] = useState(false);
	const [political, setPolitical] = useState(false);
	const [history, setHistory] = useState(false);
	const [localHistory, setLocalHistory] = useState(false);


	useEffect(async() => {
		if(id !== ''){
			const query = await queryFunction({
				variables:{
					id
				}
			});
			setImages(query.data.getActivity.images);
			setName(query.data.getActivity.name);
			setDescription(query.data.getActivity.description);
			setPhone1(query.data.getActivity.phone1);
			setPhone2(query.data.getActivity.phone2);
			setPriceDay(query.data.getActivity.priceDay);
			setPricePerson(query.data.getActivity.pricePerson);
			setLatitude(query.data.getActivity.latitude);
			setLongitude(query.data.getActivity.longitude);
			setDuration(query.data.getActivity.duration);

			setRestaurant(query.data.getActivity.restaurant);
			setBar(query.data.getActivity.bar);
			setClub(query.data.getActivity.club);
			setMonument(query.data.getActivity.monument);
			setMuseum(query.data.getActivity.museum);
			setGallery(query.data.getActivity.gallery);
			setTheatre(query.data.getActivity.theatre);
			setCinema(query.data.getActivity.cinema);
			setSquare(query.data.getActivity.square);
			setPark(query.data.getActivity.park);
			setFestival(query.data.getActivity.festival);
			setConcert(query.data.getActivity.concert);
			setIndoor(query.data.getActivity.indoor);
			setOutdoor(query.data.getActivity.outdoor);
			setReservation(query.data.getActivity.reservation);

			setHorse(query.data.getActivity.horse);
			setDiving(query.data.getActivity.diving);
			setWalking(query.data.getActivity.walking);
			setRunning(query.data.getActivity.running);
			setTrekking(query.data.getActivity.trekking);
			setCyclism(query.data.getActivity.cyclism);
			setSports(query.data.getActivity.sports);
			setRum(query.data.getActivity.rum);
			setCigar(query.data.getActivity.cigar);
			setCoffe(query.data.getActivity.coffee);
			setReligion(query.data.getActivity.religion);
			setKitchen(query.data.getActivity.kitchen);
			setArchitecture(query.data.getActivity.architecture);
			setNightLife(query.data.getActivity.nightLife);
			setMarketPlace(query.data.getActivity.marketPlace);
			setRiver(query.data.getActivity.river);

			setLake(query.data.getActivity.lake);
			setSea(query.data.getActivity.sea);
			setBeachSand(query.data.getActivity.beachSand);
			setBeachStone(query.data.getActivity.beachStone);
			setMountain(query.data.getActivity.mountain);
			setCountry(query.data.getActivity.country);
			setNature(query.data.getActivity.nature);
			setMusic(query.data.getActivity.music);
			setDance(query.data.getActivity.dance);
			setVisualArts(query.data.getActivity.visualArts);
			setEducation(query.data.getActivity.education);
			setPolitical(query.data.getActivity.political);
			setHistory(query.data.getActivity.history);
			setLocalHistory(query.data.getActivity.localHistory);
		
		}
		
	  },[]);

	if (loading || mloading) return (
		<Loading />
	);

  	if (error || merror) return <ErrorGraphql follow={(step)=> follow(step)}/>;


	const OnChangeImages = ({imageList, addUpdateIndex}) => {
		console.log(imageList, addUpdateIndex);
		setImages(imageList)
	}
	
	const isValidInput = (value) => {
		const inputClass = "mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm";
		const invalidClass = "mt-1 block w-full border border-red-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-red-500 focus:border-red-500 sm:text-sm"

		return value ? inputClass : invalidClass
	}

	const isValidForm = (value) => {
		const buttonClass = "bg-blue-600 border border-transparent rounded-md shadow-sm py-2 px-4 inline-flex justify-center text-sm font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500";
		const disabledClass = "opacity-50 bg-blue-600 border border-transparent rounded-md shadow-sm py-2 px-4 inline-flex justify-center text-sm font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500";
		
		return value ? buttonClass : disabledClass
	}

	const validateForm = () => (
		name.length >= 8 &&
		phone1.length == 10 &&
		description.length > 8 &&
		(
			priceDay > 0 ||
			pricePerson > 0
		)
	)

	

	const save = async () => {
	
		const elemId = id != "" ? id : undefined;
		const mutation = await mutateFunction({ 
			variables: { 
				input: {
					id: elemId,
					tourCreatorId,
					peopleId,
					name,
					description,
					phone1,
					phone2,
					priceDay,
					pricePerson,
					latitude,
					longitude,
					duration,

					restaurant,
					bar,
					club,
					monument,
					museum,
					gallery,
					theatre,
					cinema,
					square,
					park,
					festival,
					concert,
					indoor,
					outdoor,
					reservation,

					horse,
					diving,
					walking,
					running,
					trekking,
					cyclism,
					sports,
					rum,
					cigar,
					coffee,
					religion,
					kitchen,
					architecture,
					nightLife,
					marketPlace,
					river,

					lake,
					sea,
					beachSand,
					beachStone,
					mountain,
					country,
					nature,
					music,
					dance,
					visualArts,
					education,
					political,
					history,
					localHistory,
					images: []
				}
			} 
		});
	
		follow('activity-list');
		
	}

	return (
		<form action="#" method="POST">
			<div class="shadow sm:rounded-md sm:overflow-hidden">
				<div class="bg-white py-6 px-4 space-y-6 sm:p-6">
				<div>
					<h3 class="text-lg leading-6 font-medium text-gray-900">{t('formActivity.title')}</h3>
					<p class="mt-1 text-sm text-gray-500">{t('formActivity.description')}</p>
				</div>

				<div class="grid grid-cols-6 gap-6">
					<div class="col-span-6 sm:col-span-3">
						<label for="first-name" class="block text-sm font-medium text-gray-700">{t('formActivity.name')}</label>
						<input type="text" name="name" id="name" value={name} onChange={(evt)=>setName(evt.target.value)} class={isValidInput( name.length > 8)}/>
					</div>

					<div class="col-span-6">
						<label for="about" class="block text-sm font-medium text-gray-700"> {t('formActivity.details')} </label>
						<div class="mt-1">
							<textarea id="description" name="description" rows="3" value={description} onChange={(evt)=>setDescription(evt.target.value)} class={isValidInput( description.length > 8)}></textarea>
						</div>
						
					</div>

					<div class="col-span-6 sm:col-span-3">
						<label for="last-name" class="block text-sm font-medium text-gray-700">{t('formActivity.phone1')} <span class="text-red-500">*</span></label>
						<PhoneInput
							country={'cu'}
							value={phone1}
							onChange={(value)=>setPhone1(value)}
							inputClass={isValidInput(phone1.length == 10)}
							specialLabel=""
							placeholder='+53 XXX XXX XX'
						/>
					</div>

					<div class="col-span-6 sm:col-span-3">
						<label for="last-name" class="block text-sm font-medium text-gray-700">{t('formActivity.phone2')} <span class="text-red-500">*</span></label>
						<PhoneInput
							country={'cu'}
							value={phone2}
							onChange={(value)=>setPhone2(value)}
							inputClass={isValidInput( phone2.length == 0 || phone2.length == 10)}
							specialLabel=""
							placeholder='+53 XXX XXX XX'
						/>
					</div>

					<div class="col-span-6 sm:col-span-4">
						<legend class="text-base font-medium text-gray-900">{t('formActivity.location')}</legend>

						<div class="col-span-6 sm:col-span-3 lg:col-span-2">
							<label for="latitude" class="block text-sm font-medium text-gray-700">{t('formActivity.latitude')}</label>
							<input type="number" min="0" name="latitude" id="latitude" value={latitude} onChange={(evt)=>setLatitude(evt.target.valueAsNumber)} class={isValidInput(latitude >= 0)}/>
						</div>

						<div class="col-span-6 sm:col-span-3 lg:col-span-2">
							<label for="longitude" class="block text-sm font-medium text-gray-700">{t('formActivity.longitude')}</label>
							<input type="number" min="0" name="longitude" id="longitude" value={longitude} onChange={(evt)=>setLongitude(evt.target.valueAsNumber)} class={isValidInput(longitude >= 0)}/>
						</div>
					</div>
					
					<div class="col-span-6 sm:col-span-4">
						<legend class="text-base font-medium text-gray-900">{t('formActivity.price')} </legend>

						<div class="col-span-6 sm:col-span-3">
							<label for="pricePerson" class="block text-sm font-medium text-gray-700">{t('formActivity.price_person')}</label>
							<input type="number" min="0" name="pricePerson" id="pricePerson" value={pricePerson} onChange={(evt)=>setPricePerson(evt.target.valueAsNumber)} class={isValidInput(pricePerson >= 0)}/>
						</div>

						<div class="col-span-6 sm:col-span-4">
							<label for="priceDay" class="block text-sm font-medium text-gray-700">{t('formActivity.price_day')}</label>
							<input type="number" min="0" name="priceDay" id="priceDay" value={priceDay} onChange={(evt)=>setPriceDay(evt.target.valueAsNumber)} class={isValidInput(priceDay >= 0)}/>
						</div>
					</div>
					<div class="col-span-6 sm:col-span-3">
						<label for="duration" class="block text-sm font-medium text-gray-700">{t('formActivity.duration')}</label>
						<input type="number" min="0" name="duration" id="duration" value={duration} onChange={(evt)=>setDuration(evt.target.valueAsNumber)} class={isValidInput(duration >= 0)}/>
					</div>
					
					<div class="col-span-6 sm:col-span-4">
						<fieldset>
							<legend class="text-base font-medium text-gray-900">{t('formActivity.features')} </legend>
							<div class="col-span-3 sm:col-span-3 mt-4 space-y-4 ">
								<div class="flex items-start">
									<div class="h-5 flex items-center">
										<input id="restaurant" name="restaurant" type="checkbox" checked={restaurant} onChange={()=> setRestaurant(!restaurant)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
									</div>
									<div class="ml-3 text-sm">
										<label for="restaurant" class="font-medium text-gray-700">{t('formActivity.features.restaurant')}</label>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="bar" name="bar" type="checkbox" checked={bar} onChange={()=> setBar(!bar)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="bar" class="font-medium text-gray-700">{t('formActivity.features.bar')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="club" name="club" type="checkbox" checked={club} onChange={()=> setClub(!club)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="club" class="font-medium text-gray-700">{t('formActivity.features.club')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="monument" name="monument" type="checkbox" checked={monument} onChange={()=> setMonument(!monument)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="monument" class="font-medium text-gray-700">{t('formActivity.features.monument')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="museum" name="museum" type="checkbox" checked={museum} onChange={()=> setMuseum(!museum)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="museum" class="font-medium text-gray-700">{t('formActivity.features.museum')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="gallery" name="gallery" type="checkbox" checked={gallery} onChange={()=> setGallery(!gallery)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="gallery" class="font-medium text-gray-700">{t('formActivity.features.gallery')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="theatre" name="theatre" type="checkbox" checked={theatre} onChange={()=> setTheatre(!theatre)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="theatre" class="font-medium text-gray-700">{t('formActivity.features.theatre')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="cinema" name="cinema" type="checkbox" checked={cinema} onChange={()=> setCinema(!cinema)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="cinema" class="font-medium text-gray-700">{t('formActivity.features.cinema')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="square" name="square" type="checkbox" checked={square} onChange={()=> setSquare(!square)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="square" class="font-medium text-gray-700">{t('formActivity.features.square')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="park" name="park" type="checkbox" checked={park} onChange={()=> setPark(!park)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="park" class="font-medium text-gray-700">{t('formActivity.features.park')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="festival" name="festival" type="checkbox" checked={festival} onChange={()=> setFestival(!festival)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="festival" class="font-medium text-gray-700">{t('formActivity.features.festival')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="concert" name="concert" type="checkbox" checked={concert} onChange={()=> setConcert(!concert)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="concert" class="font-medium text-gray-700">{t('formActivity.features.concert')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="indoor" name="indoor" type="checkbox" checked={indoor} onChange={()=> setIndoor(!indoor)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="indoor" class="font-medium text-gray-700">{t('formActivity.features.indoor')}</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-span-3 sm:col-span-3 mt-4 space-y-4 ">
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="outdoor" name="outdoor" type="checkbox" checked={outdoor} onChange={()=> setOutdoor(!outdoor)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="outdoor" class="font-medium text-gray-700">{t('formActivity.features.outdoor')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="reservation" name="reservation" type="checkbox" checked={reservation} onChange={()=> setReservation(!reservation)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="reservation" class="font-medium text-gray-700">{t('formActivity.features.reservation')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="horse" name="horse" type="checkbox" checked={horse} onChange={()=> setHorse(!horse)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="horse" class="font-medium text-gray-700">{t('formActivity.features.horse')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="diving" name="diving" type="checkbox" checked={diving} onChange={()=> setDiving(!diving)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="diving" class="font-medium text-gray-700">{t('formActivity.features.diving')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="walking" name="walking" type="checkbox" checked={walking} onChange={()=> setWalking(!walking)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="walking" class="font-medium text-gray-700">{t('formActivity.features.walking')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="running" name="running" type="checkbox" checked={running} onChange={()=> setRunning(!running)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="running" class="font-medium text-gray-700">{t('formActivity.features.running')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="trekking" name="trekking" type="checkbox" checked={trekking} onChange={()=> setTrekking(!trekking)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="trekking" class="font-medium text-gray-700">{t('formActivity.features.trekking')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="cyclism" name="cyclism" type="checkbox" checked={cyclism} onChange={()=> setCyclism(!cyclism)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="cyclism" class="font-medium text-gray-700">{t('formActivity.features.cyclism')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="sports" name="sports" type="checkbox" checked={sports} onChange={()=> setSports(!sports)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="sports" class="font-medium text-gray-700">{t('formActivity.features.sports')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="rum" name="rum" type="checkbox" checked={rum} onChange={()=> setRum(!rum)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="rum" class="font-medium text-gray-700">{t('formActivity.features.rum')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="cigar" name="cigar" type="checkbox" checked={cigar} onChange={()=> setCigar(!cigar)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="cigar" class="font-medium text-gray-700">{t('formActivity.features.cigar')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="coffee" name="coffee" type="checkbox" checked={coffee} onChange={()=> setCoffe(!coffee)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="coffee" class="font-medium text-gray-700">{t('formActivity.features.coffee')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="religion" name="religion" type="checkbox" checked={religion} onChange={()=> setReligion(!religion)}  class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="religion" class="font-medium text-gray-700">{t('formActivity.features.religion')}</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-span-3 sm:col-span-3 mt-4 space-y-4 ">
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="kitchen" name="kitchen" type="checkbox" checked={kitchen} onChange={()=> setKitchen(!kitchen)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="kitchen" class="font-medium text-gray-700">{t('formActivity.features.kitchen')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="architecture" name="architecture" type="checkbox" checked={architecture} onChange={()=> setArchitecture(!architecture)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="architecture" class="font-medium text-gray-700">{t('formActivity.features.architecture')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="night_life" name="night_life" type="checkbox"  checked={nightLife} onChange={()=> setNightLife(!nightLife)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="night_life" class="font-medium text-gray-700">{t('formActivity.features.night_life')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="marketplace" name="marketplace" type="checkbox" checked={marketPlace} onChange={()=> setMarketPlace(!marketPlace)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="marketplace" class="font-medium text-gray-700">{t('formActivity.features.marketplace')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="river" name="river" type="checkbox" checked={river} onChange={()=> setRiver(!river)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="river" class="font-medium text-gray-700">{t('formActivity.features.river')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="lake" name="lake" checked={lake} onChange={()=> setLake(!lake)} type="checkbox" class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="lake" class="font-medium text-gray-700">{t('formActivity.features.lake')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="sea" name="sea" checked={sea} onChange={()=> setSea(!sea)} type="checkbox" class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="sea" class="font-medium text-gray-700">{t('formActivity.features.sea')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="beach_sand" name="beach_sand" type="checkbox" checked={beachSand} onChange={()=> setBeachSand(!beachSand)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="beach_sand" class="font-medium text-gray-700">{t('formActivity.features.beach_sand')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center"> 
											<input id="beach_stone" name="beach_stone" type="checkbox" checked={beachStone} onChange={()=> setBeachStone(!beachStone)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="beach_stone" class="font-medium text-gray-700">{t('formActivity.features.beach_stone')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="mountain" name="mountain" type="checkbox" checked={mountain} onChange={()=> setMountain(!mountain)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="mountain" class="font-medium text-gray-700">{t('formActivity.features.mountain')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="country" name="country" type="checkbox" checked={country} onChange={()=> setCountry(!country)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="country" class="font-medium text-gray-700">{t('formActivity.features.country')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="nature" name="nature" type="checkbox" checked={nature} onChange={()=> setNature(!nature)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="nature" class="font-medium text-gray-700">{t('formActivity.features.nature')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="music" name="music" type="checkbox" checked={music} onChange={()=> setMusic(!music)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="music" class="font-medium text-gray-700">{t('formActivity.features.music')}</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-span-3 sm:col-span-3 mt-4 space-y-4 ">
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="dance" name="dance" type="checkbox" checked={dance} onChange={()=> setDance(!dance)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="dance" class="font-medium text-gray-700">{t('formActivity.features.dance')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="visual_arts" name="visual_arts" type="checkbox" checked={visualArts} onChange={()=> setVisualArts(!visualArts)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="visual_arts" class="font-medium text-gray-700">{t('formActivity.features.visual_arts')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="education" name="education" type="checkbox" checked={education} onChange={()=> setEducation(!education)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="education" class="font-medium text-gray-700">{t('formActivity.features.education')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="political" name="political" type="checkbox" checked={political} onChange={()=> setPolitical(!political)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="political" class="font-medium text-gray-700">{t('formActivity.features.political')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="history" name="history" type="checkbox" checked={history} onChange={()=> setHistory(!history)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="history" class="font-medium text-gray-700">{t('formActivity.features.history')}</label>
										</div>
									</div>
								</div>
								<div>
									<div class="flex items-start">
										<div class="h-5 flex items-center">
											<input id="localHistory" name="localHistory" type="checkbox" checked={localHistory} onChange={()=> setLocalHistory(!localHistory)} class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded"/>
										</div>
										<div class="ml-3 text-sm">
											<label for="localHistory" class="font-medium text-gray-700">{t('formActivity.features.local')}</label>
										</div>
									</div>
								</div>
							</div>
						</fieldset>
					</div>
					<div class="col-span-6">
						<ImageUploader
							images={images}
							onChange={OnChangeImages}
							maxNumber={5} />
					</div>
				</div>
				</div>
				<div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
					<button type="button" onClick={()=>save()} class={isValidForm(validateForm())}>{t('buttons.save')}</button>
				</div>
			</div>
		</form>
	);
}

export default FormActivities;