import { h } from 'preact';
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'preact-router/match';
import {useEffect, useState, useContext} from "preact/hooks";
import { TranslateContext } from '@denysvuika/preact-translate';
import heroImage from '../../assets/imgs/img8.jpeg'
import FormPersonalData from './forms/formPersonalData'
import FormGuideActivity from './forms/formGuideActivity'
import FormDetail from './forms/formDetail'
import FormLodging from './forms/formLodging'
import FormActivities from './forms/formActivities'
import FormTransportation from './forms/formTransportation'
import FormBedroom from './forms/formBedroom'

const FormActivitiesStep3 = ({renderForm, follow}) => {
	const [option, setOption] = useState(renderForm);
	const { setLang, t, lang } = useContext(TranslateContext);
	let form;
	let id = option.split(";").length > 1 ? option.split(";")[1] : "";

	if(option == 'guide' || option.split(";")[0] == 'guide'){
		
		form = <FormGuideActivity id={id} follow={(step) => follow(step)}/>
	}
	
	if(option == 'lodging' || option.split(";")[0] == 'lodging' ){
		form = <FormLodging id={id} follow={(step) => follow(step)}/>
	}

	if(option == 'transportation' || option.split(";")[0] == 'transportation'){
		form = <FormTransportation id={id} follow={(step) => follow(step)}/>
	}
	
	if(option == 'activity' || option.split(";")[0] == 'activity'){
		form = <FormActivities id={id} follow={(step) => follow(step)}/>
	}

	if(option.split(";")[0] == 'create-bedroom' || 
		option.split(";")[0] == 'edit-bedroom'
	){
		form = <FormBedroom action={option.split(";")[0]} id={id} follow={(step) => follow(step)}/>
	}

	let title = id != "" ? 'Edit': 'New';
	let header = title +' '+ option.split(";")[0];

	if(option.split(";")[0] == 'create-bedroom'){
		header = 'Create bedroom'
	}

	if(option.split(";")[0] == 'edit-bedroom'){
		header = 'Edit bedroom'
	}
	
	return (
		<main>
			<div class="max-w-7xl mx-auto py-6 sm:px-6 lg:px-8">
				<h2 class="leading-6 text-blue-600 font-semibold tracking-wide uppercase">
					<a class="cursor-pointer" onClick={()=>follow('options')}>Volver</a>
				</h2>
				<h3 class="my-2 text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">{header}</h3>
				{form}
			</div>
		</main>
	);
}

export default FormActivitiesStep3;