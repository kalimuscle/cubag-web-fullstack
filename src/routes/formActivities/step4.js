import { h } from 'preact';
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'preact-router/match';
import {useEffect, useState} from "preact/hooks";
import heroImage from '../../assets/imgs/img8.jpeg'

const FormActivitiesStep4 = () => {
	const [menu, setMenu] = useState(false);

	return (
		<main>
			<div class="min-h-full flex">
				<div class="hidden lg:block relative w-0 flex-1">
					<img class="absolute inset-0 w-full object-cover h-screen" src={heroImage} alt=""/>
				</div>
				<div class="flex-1 flex flex-col justify-center py-12 px-4 sm:px-6 lg:flex-none lg:px-20 xl:px-24">
					<div class="w-full max-w-sm ">
						<div class="lg:col-start-2 lg:pl-8">
							<div class="text-base max-w-prose mx-auto lg:max-w-lg lg:ml-auto lg:mr-0">
								<h2 class="leading-6 text-blue-600 font-semibold tracking-wide uppercase">Work with us</h2>
								<h3 class="mt-2 text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">4/4 Ended Process</h3>
								<p class="mt-8 text-lg text-gray-500">
								Your service details have been added to the database. If you want to add a new element press "Add new service", otherwise you can visit our platform by pressing "Visit Cubag main page"
								</p>
								<div className='my-10'>
									<button type="button" class="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500">
										Add new service
									</button>
								</div>	
								<div className='my-10'>
									<button type="button" class="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500">
										Visit Cubag main page
									</button>
								</div>	
							</div>
							
						</div>
						
					</div>
				</div>
				
		</div>
	</main>

	);
}

export default FormActivitiesStep4;