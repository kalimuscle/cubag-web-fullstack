import { h } from 'preact';
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'preact-router/match';
import {useEffect, useState} from "preact/hooks";
import ErrorModal from '../../components/errorModal';

import ProfileForm from '../../components/profileForm'
import SidePanel from '../../components/sidepanel';
import TopPanel from '../../components/topPanel';
import FormActivitiesStep1 from './step1';
import FormPickPersonalData from './stepPersonalData';
import FormCreateProfile from './createProfile';
import FormOptions from './step2';
import FormActivitiesStep3 from './step3';
import GuideList from './guideList';
import ActivityList from './activityList';
import TransportationList from './transportationList';
import LodgingList from './lodgingList';
import BedroomList from './bedroomList';
// import FormActivitiesStep4 from './step4';

import { ref_tour_creator} from '../../store/actions/';

const FormActivities = ({tourCreatorId}) => {
	const dispatch = useDispatch();
	const [step, setStep] = useState('step1');

	useEffect(() => {
		dispatch(ref_tour_creator(tourCreatorId));
	  });

	  console.log(step);


	if(step == 'step1'){
		return (
			<FormActivitiesStep1 follow={(step) => setStep(step)}/>
		);
	}

	if(step == 'personal-data'){
		return (
			<FormPickPersonalData follow={(step) => setStep(step)}/>
		);
	}

	if(step == 'create-profile'){
		return (
			<FormCreateProfile follow={(step) => setStep(step)}/>
		);
	}

	if(step == 'options'){
		return (
			<FormOptions follow={(step) => setStep(step)} />
		);
	}

	if(step == 'guide-list'){
		return (
			<GuideList follow={(step) => setStep(step)}/>
		);
	}

	if(step == 'activity-list'){
		return (
			<ActivityList follow={(step) =>setStep(step) }/>
		);
	}

	if(step == 'transportation-list'){
		return (
			<TransportationList follow={(step) =>setStep(step) }/>
		);
	}

	if(step == 'lodging-list'){
		return (
			<LodgingList follow={(step) =>setStep(step) }/>
		);
	}

	if(step.split(";")[0] == 'bedrooms-list'){
		return (
			<BedroomList follow={(step) =>setStep(step)} lodgingId={step.split(";")[1]}/>
		);
	}

	if(step == 'guide' || step.split(";")[0]  == 'guide' ||
		step == 'activity' || step.split(";")[0]  == 'activity' ||
		step == 'transportation' || step.split(";")[0]  == 'transportation' ||
		step == 'lodging' || step.split(";")[0]  == 'lodging' ||
		step.split(";")[0]  == 'create-bedroom' || step.split(";")[0]  == 'edit-bedroom'
	){
		return (
			<FormActivitiesStep3 renderForm={step} follow={(step) => setStep(step)}/>
		);
	}
}

export default FormActivities;