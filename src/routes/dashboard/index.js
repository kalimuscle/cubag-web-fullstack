import { h } from 'preact';
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'preact-router/match';
import {useEffect, useState} from "preact/hooks";
import {route} from 'preact-router';
import ErrorModal from '../../components/errorModal';
import SidePanel from '../../components/sidepanel';

import TourRequestList from './tourRequestList';
import GuideList from '../formActivities/guideList';
import ActivityList from '../formActivities/activityList';
import TransportationList from '../formActivities/transportationList';
import LodgingList from '../formActivities/lodgingList';
import BedroomList from '../formActivities/bedroomList';
import profile from '../../assets/imgs/profile.jpg'

import { gql, useQuery, useMutation, useLazyQuery  } from '@apollo/client';
import Loading from '../../components/loading';
import {ErrorGraphql} from '../../components/errorModal';

import { signOutTourCreator} from '../../store/actions/';

const SIGNOUT_TOUR_CREATOR_MUTATION = gql`
	mutation tourCreatorSignOut($id: ID!){
		tourCreatorSignOut(id: $id){
			id
		}
	}
`;

const DashBoard = () => {
	const [signOutFunction, { loading, error }] = useMutation(SIGNOUT_TOUR_CREATOR_MUTATION, {
		onCompleted: (data) => {  
			console.log({data});
			route('/sign');
		}
	});
	const userSub = useSelector(state => state.userSub);
	const [menu, setMenu] = useState(false);
	const [options, setOptions] = useState('tours-request');

	const [menuHelper, setMenuHelper] = useState(false);

	const dispatch = useDispatch();

	if(userSub == ''){
		route('/sign');
	}

	if (loading) return (
		<Loading />
	);

	const logOut = async () => {
		await signOutFunction({
			variables: {
				id: userSub
			}
		});

		dispatch(signOutTourCreator());
	}

	const menuHelperComponent = menuHelper ? (
		<div class="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 divide-y divide-gray-200 focus:outline-none" role="menu" aria-orientation="vertical" aria-labelledby="user-menu-button" tabindex="-1">						
			<div class="py-1" role="none">
				<a onClick={()=> logOut()} class="text-gray-700 cursor-pointer block px-4 py-2 text-sm" role="menuitem" tabindex="-1" id="user-menu-item-5">Logout</a>
			</div>
		</div>
	) : null;

	const renderComponent = () => {
		switch(options){
			case 'tours-request' : return <TourRequestList />
			case 'tours' : return null
			case 'guides' : return <GuideList modify={false}/>
			case 'activities' : return <ActivityList modify={false}/>
			case 'lodgings' : return <LodgingList modify={false}/>
			case 'transportations' : return <TransportationList modify={false}/>
		}
	}

	return (
		<div class="min-h-full"> 	
			<SidePanel showMenu={menu} onClose={() => setMenu(false)} options={options} onClickOptions={(val) => setOptions(val)} onLogout={() => logOut()}/>
		
			<div class="lg:pl-64 flex flex-col">
			
				<div class="sticky top-0 z-10 flex-shrink-0 flex h-16 bg-white border-b border-gray-200 lg:hidden">
			
					<button onClick={()=> setMenu(true)} type="button" class="px-4 border-r border-gray-200 text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-blue-500 lg:hidden">
						<span class="sr-only">Open sidebar</span>
						
						<svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
							<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h8m-8 6h16" />
						</svg>
					</button>
					<div class="flex-1 flex justify-between px-4 sm:px-6 lg:px-8">
						<div class="flex-1 flex">
							<div class="w-full flex md:ml-0">
								
							</div>
						</div>
						<div class="flex items-center">
						
							<div class="ml-3 relative">
								<div>
									<button type="button" onClick={() => setMenuHelper(!menuHelper)} class="max-w-xs bg-white flex items-center text-sm rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500" id="user-menu-button" aria-expanded="false" aria-haspopup="true">
										<span class="sr-only">Open user menu</span>
										<img class="w-10 h-10 bg-gray-300 rounded-full flex-shrink-0" src={profile} alt=""/>
									</button>
								</div>

								
								{menuHelperComponent}
							</div>
						</div>
					</div>
				</div>
				<main class="flex-1">
					{renderComponent()}
				</main>
			</div>
		</div>

	);
}

export default DashBoard;