import { h, Fragment } from 'preact';
import {useEffect, useState} from "preact/hooks";
import { TranslateContext } from '@denysvuika/preact-translate';
import { gql, useQuery, useMutation, useLazyQuery  } from '@apollo/client';
import { useSelector, useDispatch } from 'react-redux'
import TimeAgo from 'react-timeago'
import Loading from '../../components/loading';
import {ErrorGraphql} from '../../components/errorModal';

// Define query
const TOURS_REQUEST_QUERY = gql`
  query toursRequest{
	toursRequest{
		count
    	items{
      		id
			description
			email
			phone
			updatedAt
		}
	}
}
`;

const DELETE_TOUR_REQUEST_MUTATION = gql`
	mutation deleteTourRequest($id: ID!){
		deleteTourRequest(id: $id){
			id
		}
	}
`;

const TourRequestList = ({follow}) => {
	const { loading, error, data, refetch } = useQuery(TOURS_REQUEST_QUERY, {
		
	  });
	  const [deleteFunction, { loading: dloading, error: derror }] = useMutation(DELETE_TOUR_REQUEST_MUTATION, {
		onCompleted: (data) => {  
			console.log({data});
		}
	});

	const remove = async (id) => {
		await deleteFunction({
			variables: {
				id
			}
		});

		await refetch();
	}

	const tourCreatorId = useSelector(state => state.tourCreatorId)

	if (loading || dloading) return <Loading />

  	if (error || derror) return <ErrorGraphql follow={(step)=> follow(step)}/>;

	  const items = data.toursRequest.items;
	  const list_li = items.map((item)=>(
		<li>
			<div class="group flex items-center justify-between px-4 py-4 hover:bg-gray-50 sm:px-6">
				<span class="flex items-center truncate space-x-3">
					<span class="font-medium truncate text-sm leading-6">
						<a href="#">{item.description}</a>
		
					</span>
				</span>
				<button onClick={()=> remove(item.id)} class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800">Delete</button>
			</div>
		</li>
	  ));

	  const list_tr = items.map((item)=>(
		<tr>
			<td class="px-6 py-3 max-w-0 w-full whitespace-nowrap text-sm font-medium text-gray-900">
				<div class="flex items-center space-x-3 lg:pl-2">
					<p class="hover:text-gray-600 text-gray-500">
						<a href="#">{item.description}</a>
					</p>
				</div>
			</td>
			<td class="px-6 py-3 text-sm text-gray-500 font-medium">
				<div class="flex items-center space-x-2">
					{item.email}
				</div>
			</td>
			<td class="hidden md:table-cell px-6 py-3 whitespace-nowrap text-sm text-gray-500 text-right">{item.phone}</td>
			<td class="hidden md:table-cell px-6 py-3 whitespace-nowrap text-sm text-gray-500 text-right">
				<TimeAgo date={new Date(item.updatedAt).toDateString() }  />
			</td>
			<td class="px-6 py-3 whitespace-nowrap text-right text-sm font-medium">
				<button onClick={()=> remove(item.id)} class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800">Delete</button>
			</td>
		</tr>
	  ));

	return (
		<main>
			<div class="border-b border-gray-200 px-4 py-4 sm:flex sm:items-center sm:justify-between sm:px-6 lg:px-8">
				<div class="flex-1 min-w-0">
					<h1 class="text-lg font-medium leading-6 text-gray-900 sm:truncate">Home</h1>
				</div>
				<div class="mt-4 flex sm:mt-0 sm:ml-4">
					<a href={"/factivities/" + tourCreatorId} target="_blank"class="order-1 ml-3 inline-flex items-center px-4 py-2 border border-gray-300 shadow-sm text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 sm:order-0 sm:ml-0">Infrastructure</a>
					<button type="button" class="order-0 inline-flex items-center px-4 py-2 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 sm:order-1 sm:ml-3">Create Tour</button>
				</div>
			</div>

			<Fragment>
				<div class="mt-10 sm:hidden">
					<div class="px-4 sm:px-6">
						<h2 class="text-gray-500 text-xs font-medium uppercase tracking-wide">Tours Request</h2>
					</div>
					<ul role="list" class="mt-3 border-t border-gray-200 divide-y divide-gray-100">
						{list_li}

					</ul>
				</div>

				<div class="hidden mt-8 sm:block">
					<div class="align-middle inline-block min-w-full border-b border-gray-200">
						<table class="min-w-full">
							<thead>
								<tr class="border-t border-gray-200">
									<th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
										<span class="lg:pl-2">Tours Request</span>
									</th>
									<th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Email</th>
									<th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Phone</th>
									<th class="hidden md:table-cell px-6 py-3 border-b border-gray-200 bg-gray-50 text-right text-xs font-medium text-gray-500 uppercase tracking-wider">Updated</th>
									<th class="pr-6 py-3 border-b border-gray-200 bg-gray-50 text-right text-xs font-medium text-gray-500 uppercase tracking-wider"></th>
								</tr>
							</thead>
							<tbody class="bg-white divide-y divide-gray-100">
								{list_tr}
							</tbody>
						</table>
					</div>
				</div>
			</Fragment>

		</main>
		
  );
}

export default TourRequestList;
