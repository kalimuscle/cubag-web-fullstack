import { h } from 'preact';
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'preact-router/match';
import {useEffect, useState, useContext} from "preact/hooks";
import { list_gateway, remove_gateway, close_modal_error } from '../../store/actions';
import ErrorModal from '../../components/errorModal';
import Hero from './hero';
import Stat from './stat';
import Testimonials from './testimonials';
import Reservations from './reservations';
import Header from '../../components/header';
import Footer from '../../components/footer';
import signImage from '../../assets/imgs/img8.jpeg'
import { TranslateContext } from '@denysvuika/preact-translate';

const Home = () =>{ 
	const { setLang, t, lang } = useContext(TranslateContext);
	const dispatch = useDispatch();

	const buttonLangClass = (language) => {

		let selected = "relative inline-flex items-center px-4 py-2 border border-blue-600 bg-blue-500 text-sm font-medium text-white hover:bg-blue-600 focus:z-10 focus:outline-none focus:ring-1 focus:ring-blue-500 focus:border-blue-500";
		let normal = "relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:z-10 focus:outline-none focus:ring-1 focus:ring-blue-500 focus:border-blue-500";

		return language == lang ? selected : normal;
	}


	//const modal = error ? <ErrorModal closeModal={(status)=> dispatch(close_modal_error())}/> : null;

	return(
	
		<div class="bg-gray-50">
			<div class="relative overflow-hidden">
				<div class="absolute inset-y-0 h-full w-full" aria-hidden="true">
					<div class="relative h-full">
						<svg class="absolute right-full transform translate-y-1/3 translate-x-1/4 md:translate-y-1/2 sm:translate-x-1/2 lg:translate-x-full" width="404" height="784" fill="none" viewBox="0 0 404 784">
						<defs>
							<pattern id="e229dbec-10e9-49ee-8ec3-0286ca089edf" x="0" y="0" width="20" height="20" patternUnits="userSpaceOnUse">
							<rect x="0" y="0" width="4" height="4" class="text-gray-200" fill="currentColor" />
							</pattern>
						</defs>
						<rect width="404" height="784" fill="url(#e229dbec-10e9-49ee-8ec3-0286ca089edf)" />
						</svg>
						<svg class="absolute left-full transform -translate-y-3/4 -translate-x-1/4 sm:-translate-x-1/2 md:-translate-y-1/2 lg:-translate-x-3/4" width="404" height="784" fill="none" viewBox="0 0 404 784">
						<defs>
							<pattern id="d2a68204-c383-44b1-b99f-42ccff4e5365" x="0" y="0" width="20" height="20" patternUnits="userSpaceOnUse">
							<rect x="0" y="0" width="4" height="4" class="text-gray-200" fill="currentColor" />
							</pattern>
						</defs>
						<rect width="404" height="784" fill="url(#d2a68204-c383-44b1-b99f-42ccff4e5365)" />
						</svg>
					</div>
				</div>

				<div class="relative pt-6 pb-16 sm:pb-24">
					<div>
						<div class="max-w-7xl mx-auto px-4 sm:px-6">
						<nav class="relative flex items-center justify-between sm:h-10 md:justify-center" aria-label="Global">
							<div class="flex items-center flex-1 md:absolute md:inset-y-0 md:left-0">
							<div class="flex items-center justify-between w-full md:w-auto">
								<a href="#">
								<span class="sr-only">Workflow</span>
								<div class="h-12 w-12">
									<svg fill="none" viewBox="0 0 35 32" xmlns="http://www.w3.org/2000/svg">
										<path fill="#4f46e5" d="M15.258 26.865a4.043 4.043 0 01-1.133 2.917A4.006 4.006 0 0111.253 31a3.992 3.992 0 01-2.872-1.218 4.028 4.028 0 01-1.133-2.917c.009-.698.2-1.382.557-1.981.356-.6.863-1.094 1.47-1.433-.024.109.09-.055 0 0l1.86-1.652a8.495 8.495 0 002.304-5.793c0-2.926-1.711-5.901-4.17-7.457.094.055-.036-.094 0 0A3.952 3.952 0 017.8 7.116a3.975 3.975 0 01-.557-1.98 4.042 4.042 0 011.133-2.918A4.006 4.006 0 0111.247 1a3.99 3.99 0 012.872 1.218 4.025 4.025 0 011.133 2.917 8.521 8.521 0 002.347 5.832l.817.8c.326.285.668.551 1.024.798.621.33 1.142.826 1.504 1.431a3.902 3.902 0 01-1.504 5.442c.033-.067-.063.036 0 0a8.968 8.968 0 00-3.024 3.183 9.016 9.016 0 00-1.158 4.244zM19.741 5.123c0 .796.235 1.575.676 2.237a4.01 4.01 0 001.798 1.482 3.99 3.99 0 004.366-.873 4.042 4.042 0 00.869-4.386 4.02 4.02 0 00-1.476-1.806 3.994 3.994 0 00-5.058.501 4.038 4.038 0 00-1.175 2.845zM23.748 22.84c-.792 0-1.567.236-2.226.678a4.021 4.021 0 00-1.476 1.806 4.042 4.042 0 00.869 4.387 3.99 3.99 0 004.366.873A4.01 4.01 0 0027.08 29.1a4.039 4.039 0 00-.5-5.082 4 4 0 00-2.832-1.18zM34 15.994c0-.796-.235-1.574-.675-2.236a4.01 4.01 0 00-1.798-1.483 3.99 3.99 0 00-4.367.873 4.042 4.042 0 00-.869 4.387 4.02 4.02 0 001.476 1.806 3.993 3.993 0 002.226.678 4.003 4.003 0 002.832-1.18A4.04 4.04 0 0034 15.993z"/>
										<path fill="#4f46e5" d="M5.007 11.969c-.793 0-1.567.236-2.226.678a4.021 4.021 0 00-1.476 1.807 4.042 4.042 0 00.869 4.386 4.001 4.001 0 004.366.873 4.011 4.011 0 001.798-1.483 4.038 4.038 0 00-.5-5.08 4.004 4.004 0 00-2.831-1.181z"/>
									</svg>
								</div>
								</a>
								<div class="-mr-2 flex items-center md:hidden">
								<button type="button" class="bg-gray-50 rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-blue-500" aria-expanded="false">
									<span class="sr-only">Open main menu</span>
									
									<svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" aria-hidden="true">
									<path stroke-linecap="round" stroke-linejoin="round" d="M4 6h16M4 12h16M4 18h16" />
									</svg>
								</button>
								</div>
							</div>
							</div>
							
							<div class="hidden md:absolute md:flex md:items-center md:justify-end md:inset-y-0 md:right-0">
								<span class="relative z-0 inline-flex shadow-sm rounded-md">
									<button type="button" onClick={()=> setLang('es')} class={'rounded-l-md ' + buttonLangClass('es')}>{t('labels.spanish')}</button>
									<button type="button" onClick={()=> setLang('en')} class={'-ml-px rounded-r-md ' + buttonLangClass('en')}>{t('labels.english')}</button>
								</span>
							</div>
						</nav>
						</div>

						<div class="absolute z-10 top-0 inset-x-0 p-2 transition transform origin-top-right md:hidden">
							<div class="rounded-lg shadow-md bg-white ring-1 ring-black ring-opacity-5 overflow-hidden">
								<div class="px-5 pt-4 flex items-center justify-between">
									<div class="h-12 w-12">
										<svg fill="none" viewBox="0 0 35 32" xmlns="http://www.w3.org/2000/svg">
											<path fill="#4f46e5" d="M15.258 26.865a4.043 4.043 0 01-1.133 2.917A4.006 4.006 0 0111.253 31a3.992 3.992 0 01-2.872-1.218 4.028 4.028 0 01-1.133-2.917c.009-.698.2-1.382.557-1.981.356-.6.863-1.094 1.47-1.433-.024.109.09-.055 0 0l1.86-1.652a8.495 8.495 0 002.304-5.793c0-2.926-1.711-5.901-4.17-7.457.094.055-.036-.094 0 0A3.952 3.952 0 017.8 7.116a3.975 3.975 0 01-.557-1.98 4.042 4.042 0 011.133-2.918A4.006 4.006 0 0111.247 1a3.99 3.99 0 012.872 1.218 4.025 4.025 0 011.133 2.917 8.521 8.521 0 002.347 5.832l.817.8c.326.285.668.551 1.024.798.621.33 1.142.826 1.504 1.431a3.902 3.902 0 01-1.504 5.442c.033-.067-.063.036 0 0a8.968 8.968 0 00-3.024 3.183 9.016 9.016 0 00-1.158 4.244zM19.741 5.123c0 .796.235 1.575.676 2.237a4.01 4.01 0 001.798 1.482 3.99 3.99 0 004.366-.873 4.042 4.042 0 00.869-4.386 4.02 4.02 0 00-1.476-1.806 3.994 3.994 0 00-5.058.501 4.038 4.038 0 00-1.175 2.845zM23.748 22.84c-.792 0-1.567.236-2.226.678a4.021 4.021 0 00-1.476 1.806 4.042 4.042 0 00.869 4.387 3.99 3.99 0 004.366.873A4.01 4.01 0 0027.08 29.1a4.039 4.039 0 00-.5-5.082 4 4 0 00-2.832-1.18zM34 15.994c0-.796-.235-1.574-.675-2.236a4.01 4.01 0 00-1.798-1.483 3.99 3.99 0 00-4.367.873 4.042 4.042 0 00-.869 4.387 4.02 4.02 0 001.476 1.806 3.993 3.993 0 002.226.678 4.003 4.003 0 002.832-1.18A4.04 4.04 0 0034 15.993z"/>
											<path fill="#4f46e5" d="M5.007 11.969c-.793 0-1.567.236-2.226.678a4.021 4.021 0 00-1.476 1.807 4.042 4.042 0 00.869 4.386 4.001 4.001 0 004.366.873 4.011 4.011 0 001.798-1.483 4.038 4.038 0 00-.5-5.08 4.004 4.004 0 00-2.831-1.181z"/>
										</svg>
									</div>
									<div class="-mr-2">
										<button type="button" class="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-blue-500">
											<span class="sr-only">Close main menu</span>
											
											<svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" aria-hidden="true">
												<path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
											</svg>
										</button>
									</div>
								</div>
								<div class="flex">
									<span class="relative mx-auto mb-4 z-0 inline-flex shadow-sm rounded-md">
										<button type="button" onClick={()=> setLang('es')} class={'rounded-l-md ' + buttonLangClass('es')}>{t('labels.spanish')}</button>
										<button type="button" onClick={()=> setLang('en')} class={'-ml-px rounded-r-md ' + buttonLangClass('en')}>{t('labels.english')}</button>
									</span>
								</div>
								
								{/* <a href="#" class="block w-full px-5 py-3 text-center font-medium text-blue-600 bg-gray-50 hover:bg-gray-100 hover:text-blue-700"> Log in </a> */}
							</div>
						</div>
					</div>

					<div class="mt-16 mx-auto max-w-7xl px-4 sm:mt-24 sm:px-6">
						<div class="text-center">
							<h1 class="text-4xl tracking-tight font-extrabold text-gray-900 sm:text-5xl md:text-6xl">
								<span class="block">{t('pages.home.title')}</span>
								<span class="block text-blue-600">{t('pages.home.title2')}</span>
							</h1>
							<p class="mt-3 max-w-md mx-auto text-base text-gray-500 sm:text-lg md:mt-5 md:text-xl md:max-w-3xl">{t('pages.home.comment')}</p>
							<div class="mt-10 max-w-sm mx-auto sm:max-w-none sm:flex sm:justify-center">
								<div class="space-y-4 sm:space-y-0 sm:mx-auto sm:inline-grid sm:grid-cols-1 sm:gap-5">
									{/* <a href="#" class="flex items-center justify-center px-4 py-3 border border-transparent text-base font-medium rounded-md shadow-sm text-blue-700 bg-white hover:bg-blue-50 sm:px-8"> {t('pages.home.contact')} </a> */}
									<a href="/contact" class="flex items-center justify-center px-4 py-3 border border-transparent text-base font-medium rounded-md shadow-sm text-white bg-blue-500 bg-opacity-80 hover:bg-opacity-70 sm:px-8"> {t('pages.home.contact')} </a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="relative">
					<div class="absolute inset-0 flex flex-col" aria-hidden="true">
						<div class="flex-1"></div>
						<div class="flex-1 w-full bg-blue-800"></div>
					</div>
					<div class="max-w-7xl mx-auto px-4 sm:px-6">
						<img class="relative rounded-lg shadow-lg" width="1233" height="888" src={signImage} alt="App screenshot"/>
					</div>
				</div>
			</div>
			{/* <div class="bg-gray-800">
				<div class="max-w-7xl mx-auto py-16 px-4 sm:py-24 sm:px-6 lg:px-8">
					<h2 class="text-center text-gray-400 text-sm font-semibold uppercase tracking-wide">Trusted by over 26,000 forward-thinking companies</h2>
					<div class="mt-8 grid grid-cols-2 gap-8 md:grid-cols-6 lg:grid-cols-5">
						<div class="col-span-1 flex justify-center md:col-span-2 lg:col-span-1">
						<img class="h-12" src="https://tailwindui.com/img/logos/tuple-logo-gray-400.svg" alt="Tuple"/>
						</div>
						<div class="col-span-1 flex justify-center md:col-span-2 lg:col-span-1">
						<img class="h-12" src="https://tailwindui.com/img/logos/mirage-logo-gray-400.svg" alt="Mirage"/>
						</div>
						<div class="col-span-1 flex justify-center md:col-span-2 lg:col-span-1">
						<img class="h-12" src="https://tailwindui.com/img/logos/statickit-logo-gray-400.svg" alt="StaticKit"/>
						</div>
						<div class="col-span-1 flex justify-center md:col-span-3 lg:col-span-1">
						<img class="h-12" src="https://tailwindui.com/img/logos/transistor-logo-gray-400.svg" alt="Transistor"/>
						</div>
						<div class="col-span-2 flex justify-center md:col-span-3 lg:col-span-1">
						<img class="h-12" src="https://tailwindui.com/img/logos/workcation-logo-gray-400.svg" alt="Workcation"/>
						</div>
					</div>
				</div>
			</div> */}
			<Stat/>
		 	<Testimonials/> 
			 <Footer/>
	
		</div>

	);
}

export default Home;