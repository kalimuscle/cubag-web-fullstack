import { h } from 'preact';
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'preact-router/match';
import {useEffect, useState, useContext} from "preact/hooks";
import { TranslateContext } from '@denysvuika/preact-translate';
import { list_gateway, remove_gateway, close_modal_error } from '../../store/actions';
import ErrorModal from '../../components/errorModal';
import style from './style.css';

const Stat = () => { 
	const { setLang, t, lang } = useContext(TranslateContext);

	return(
		<div class="bg-blue-800">
			<div class="max-w-7xl mx-auto py-12 px-4 sm:py-16 sm:px-6 lg:px-8 lg:py-20">
				<div class="max-w-4xl mx-auto text-center">
					<h2 class="text-3xl font-extrabold text-white sm:text-4xl">{t('pages.home.stat.title')}</h2>
					<p class="mt-3 text-xl text-blue-200 sm:mt-4">{t('pages.home.stat.subtitle')}</p>
				</div>
				<dl class="mt-10 text-center sm:max-w-3xl sm:mx-auto sm:grid sm:grid-cols-3 sm:gap-8">
					<div class="flex flex-col">
						<dt class="order-2 mt-2 text-lg leading-6 font-medium text-blue-200">{t('pages.home.stat.option1')}</dt>
						<dd class="order-1 text-5xl font-extrabold text-white">100%</dd>
					</div>
					<div class="flex flex-col mt-10 sm:mt-0">
						<dt class="order-2 mt-2 text-lg leading-6 font-medium text-blue-200">{t('pages.home.stat.option2')}</dt>
						<dd class="order-1 text-5xl font-extrabold text-white">24/7</dd>
					</div>
					<div class="flex flex-col mt-10 sm:mt-0">
						<dt class="order-2 mt-2 text-lg leading-6 font-medium text-blue-200">{t('pages.home.stat.option3')}</dt>
						<dd class="order-1 text-5xl font-extrabold text-white">100k+</dd>
					</div>
				</dl>
			</div>
		</div>
	);
}

export default Stat;