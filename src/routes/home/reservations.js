import { h } from 'preact';
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'preact-router/match';
import {useEffect, useState, useContext} from "preact/hooks";
import { TranslateContext } from '@denysvuika/preact-translate';
import { list_gateway, remove_gateway, close_modal_error } from '../../store/actions';
import ErrorModal from '../../components/errorModal';
import imgProduct1 from '../../assets/imgs/img2.jpeg'
import imgProduct2 from '../../assets/imgs/img4.jpeg'
import imgProduct3 from '../../assets/imgs/img5.jpeg'
import imgProduct4 from '../../assets/imgs/img6.jpeg'
import imgProduct5 from '../../assets/imgs/img12.jpeg'
import imgProduct6 from '../../assets/imgs/img14.jpeg'
import imgProduct7 from '../../assets/imgs/img8.jpeg'
import imgProduct8 from '../../assets/imgs/img1.jpeg'
import style from './style.css';

const Reservations = () => { 
	const { setLang, t, lang } = useContext(TranslateContext);

	return(
		<section class="py-12 bg-gray-50 overflow-hidden md:py-20 lg:py-24">
			<div class="bg-gray-50">
				<div class="max-w-2xl mx-auto py-16 px-4 sm:py-24 sm:px-6 lg:max-w-7xl lg:px-8">
					<h2 class="text-4xl tracking-tight font-extrabold text-gray-900 sm:text-5xl md:text-6xl lg:text-5xl xl:text-6xl">
						<span class="block xl:inline">{t('pages.home.reservation')}</span>
							<span class="block text-blue-600 xl:inline"> {t('pages.home.demanded')}</span>
					</h2>

					<div class="mt-6 grid grid-cols-1 gap-y-10 sm:grid-cols-2 gap-x-6 lg:grid-cols-3 xl:grid-cols-4 xl:gap-x-8">
						<a href="/detail/23232" class="group">
							<div class="w-full aspect-w-1 aspect-h-1 bg-gray-200 rounded-lg overflow-hidden xl:aspect-w-7 xl:aspect-h-8">
							<img src={imgProduct1} alt="Tall slender porcelain bottle with natural clay textured body and cork stopper." class="w-full h-full object-center object-cover group-hover:opacity-75"/>
							</div>
							<h3 class="mt-4 text-sm text-gray-700">Earthen Bottle</h3>
							<p class="mt-1 text-lg font-medium text-gray-900">$48</p>
						</a>

						<a href="/detail/23232" class="group">
							<div class="w-full aspect-w-1 aspect-h-1 bg-gray-200 rounded-lg overflow-hidden xl:aspect-w-7 xl:aspect-h-8">
							<img src={imgProduct2} alt="Olive drab green insulated bottle with flared screw lid and flat top." class="w-full h-full object-center object-cover group-hover:opacity-75"/>
							</div>
							<h3 class="mt-4 text-sm text-gray-700">Nomad Tumbler</h3>
							<p class="mt-1 text-lg font-medium text-gray-900">$35</p>
						</a>

						<a href="/detail/23232" class="group">
							<div class="w-full aspect-w-1 aspect-h-1 bg-gray-200 rounded-lg overflow-hidden xl:aspect-w-7 xl:aspect-h-8">
							<img src={imgProduct3} alt="Person using a pen to cross a task off a productivity paper card." class="w-full h-full object-center object-cover group-hover:opacity-75"/>
							</div>
							<h3 class="mt-4 text-sm text-gray-700">Focus Paper Refill</h3>
							<p class="mt-1 text-lg font-medium text-gray-900">$89</p>
						</a>

						<a href="/detail/23232" class="group">
							<div class="w-full aspect-w-1 aspect-h-1 bg-gray-200 rounded-lg overflow-hidden xl:aspect-w-7 xl:aspect-h-8">
							<img src={imgProduct4} alt="Hand holding black machined steel mechanical pencil with brass tip and top." class="w-full h-full object-center object-cover group-hover:opacity-75"/>
							</div>
							<h3 class="mt-4 text-sm text-gray-700">Machined Mechanical Pencil</h3>
							<p class="mt-1 text-lg font-medium text-gray-900">$35</p>
						</a>

					</div>

					<div class="grid grid-cols-1 gap-y-10 sm:grid-cols-2 gap-x-6 lg:grid-cols-3 xl:grid-cols-4 xl:gap-x-8">
						<a href="/detail/23232" class="group">
							<div class="w-full aspect-w-1 aspect-h-1 bg-gray-200 rounded-lg overflow-hidden xl:aspect-w-7 xl:aspect-h-8">
							<img src={imgProduct5} alt="Tall slender porcelain bottle with natural clay textured body and cork stopper." class="w-full h-full object-center object-cover group-hover:opacity-75"/>
							</div>
							<h3 class="mt-4 text-sm text-gray-700">Earthen Bottle</h3>
							<p class="mt-1 text-lg font-medium text-gray-900">$48</p>
						</a>

						<a href="/detail/23232" class="group">
							<div class="w-full aspect-w-1 aspect-h-1 bg-gray-200 rounded-lg overflow-hidden xl:aspect-w-7 xl:aspect-h-8">
							<img src={imgProduct6} alt="Olive drab green insulated bottle with flared screw lid and flat top." class="w-full h-full object-center object-cover group-hover:opacity-75"/>
							</div>
							<h3 class="mt-4 text-sm text-gray-700">Nomad Tumbler</h3>
							<p class="mt-1 text-lg font-medium text-gray-900">$35</p>
						</a>

						<a href="/detail/23232" class="group">
							<div class="w-full aspect-w-1 aspect-h-1 bg-gray-200 rounded-lg overflow-hidden xl:aspect-w-7 xl:aspect-h-8">
							<img src={imgProduct7} alt="Person using a pen to cross a task off a productivity paper card." class="w-full h-full object-center object-cover group-hover:opacity-75"/>
							</div>
							<h3 class="mt-4 text-sm text-gray-700">Focus Paper Refill</h3>
							<p class="mt-1 text-lg font-medium text-gray-900">$89</p>
						</a>

						<a href="/detail/23232" class="group">
							<div class="w-full aspect-w-1 aspect-h-1 bg-gray-200 rounded-lg overflow-hidden xl:aspect-w-7 xl:aspect-h-8">
							<img src={imgProduct8} alt="Hand holding black machined steel mechanical pencil with brass tip and top." class="w-full h-full object-center object-cover group-hover:opacity-75"/>
							</div>
							<h3 class="mt-4 text-sm text-gray-700">Machined Mechanical Pencil</h3>
							<p class="mt-1 text-lg font-medium text-gray-900">$35</p>
						</a>

					</div>
				</div>
			</div>
		</section>
	);
}

export default Reservations;