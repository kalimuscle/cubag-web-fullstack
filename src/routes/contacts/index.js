import { h } from 'preact';
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'preact-router/match';
import {useEffect, useState, useContext} from "preact/hooks";
import { TranslateContext } from '@denysvuika/preact-translate';
import ErrorModal from '../../components/errorModal';
import Header from '../../components/header';
import Footer from '../../components/footer';
import FormContact from './formContact';
import imgProduct1 from '../../assets/imgs/img2.jpeg'

const Contacts = () => {
	const { setLang, t, lang } = useContext(TranslateContext);

	return (
		<main>
			{/* <Header/> */}
			<section>
				<div class="relative bg-white">
				<div class="lg:absolute lg:inset-0">
					<div class="lg:absolute lg:inset-y-0 lg:right-0 lg:w-1/2">
					<img class="h-56 w-full object-cover lg:absolute lg:h-full" src={imgProduct1} alt=""/>
					</div>
				</div>
				<div class="relative py-16 px-4 sm:py-24 sm:px-6 lg:px-8 lg:max-w-7xl lg:mx-auto lg:py-32 lg:grid lg:grid-cols-2">
					<div class="lg:pr-8">
					<div class="max-w-md mx-auto sm:max-w-lg lg:mx-0">
						<h2 class="text-3xl font-extrabold tracking-tight sm:text-4xl">{t('pages.contact.title')}</h2>
						<p class="mt-4 text-lg text-gray-500 sm:mt-3">{t('pages.contact.comment')}</p>
						<FormContact />
					</div>
					</div>
				</div>
				</div>
			</section>
			<Footer/>
		</main>
	);
}

export default Contacts;