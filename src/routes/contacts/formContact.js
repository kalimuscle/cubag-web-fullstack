import { h } from 'preact';
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'preact-router/match';
import {useEffect, useState, useContext} from "preact/hooks";
import { TranslateContext } from '@denysvuika/preact-translate';
import PhoneInput from 'react-phone-input-2';
import { gql, useLazyQuery, useMutation } from '@apollo/client';
import Loading from '../../components/loading';
import {ErrorGraphql} from '../../components/errorModal';

const CREATE_TOUR_REQUEST_MUTATION = gql`
  mutation createTourRequest($input: CreateTourRequestInput!){
	createTourRequest(input: $input){
		id
	}
}
`;

const FormContact = () => {
	const { setLang, t, lang } = useContext(TranslateContext);
	const [mutateFunction, {data, loading, error}] = useMutation(CREATE_TOUR_REQUEST_MUTATION, {onCompleted: (data) => {  console.log({data});  }});
	
	const [fullName, setFullName] = useState('');
	const [email, setEmail] = useState('');
	const [phone, setPhone] = useState('');
	const [description, setDescription] = useState('');
	const [budget, setBudget] = useState('below-25k');
	const [channelInfo, setChannelInfo] = useState('');

	if (loading) return (
		<div class="h-full"><Loading /></div>
		
	);

  	if (error) return <ErrorGraphql follow={(step)=> follow(step)}/>;

	const submit = async () => { console.log('submit');
		const mutation = await mutateFunction({ 
			variables: { 
				input: {
					fullName,
					email,
					phone,
					description,
					budget,
					channelInfo
				}
			} 
		});
		
	}

	const validateEmail = (value) => {
		const expEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		const pattern_email = new RegExp(expEmail);

		return pattern_email.test(value)
	}

	const validateForm = () => (
		fullName.length >= 8 &&
		validateEmail(email) &&
		description.length >= 8 &&
		channelInfo.length >= 8 &&
		description.length >= 8 
	)
	
	const isValidInput = (value) => {
		const inputClass = "mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm";
		const invalidClass = "mt-1 block w-full border border-red-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-red-500 focus:border-red-500 sm:text-sm"

		return value ? inputClass : invalidClass
	}

	const isValidForm = (value) => {
		const buttonClass = "w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500";
		const disabledClass = "cursor-not-allowed opacity-50 w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500";
		
		return value ? buttonClass : disabledClass
	}
	

	return (
		<form action="#" method="POST" class="mt-9 grid grid-cols-1 gap-y-6 sm:grid-cols-2 sm:gap-x-8">
			<div class="sm:col-span-2">
				<label for="fullname" class="block text-sm font-medium text-gray-700">Fullname</label>
				<div class="mt-1">
					<input type="text" name="fullname" id="fullname"   value={fullName} onChange={(evt)=>setFullName(evt.target.value)} class={isValidInput(fullName.length == 0 || fullName.length >= 8)}/>
				</div>
			</div>
			<div class="sm:col-span-2">
				<label for="email" class="block text-sm font-medium text-gray-700">{t('formContact.email')}</label>
				<div class="mt-1">
					<input type="email" name="email-address" id="email-address" autocomplete="email"  value={email} onChange={(evt)=>setEmail(evt.target.value)} class={isValidInput(email.length == 0 || validateEmail(email))}/>
				</div>
			</div>
			<div class="sm:col-span-2">
				<div class="flex justify-between">
					<label for="phone" class="block text-sm font-medium text-gray-700">{t('formContact.phone')}</label>
					<span id="phone-description" class="text-sm text-gray-500">{t('formContact.phone_optional')}</span>
				</div>
				<div class="mt-1">
					<PhoneInput
						value={phone}
						onChange={(value)=>setPhone(value)}
						inputClass="mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm"
						specialLabel=""
					/>
				</div>
			</div>
			<div class="sm:col-span-2">
				<div class="flex justify-between">
					<label for="description" class="block text-sm font-medium text-gray-700">{t('formContact.description')}</label>
					<span id="how-can-we-help-description" class="text-sm text-gray-500">{t('formContact.max')}</span>
				</div>
				<div class="mt-1">
					<textarea id="description" name="description" aria-describedby="how-can-we-help-description" rows="4"  value={description} onChange={(evt)=>setDescription(evt.target.value)} class={isValidInput( description.length == 0 || description.length >= 8)}></textarea>
				</div>
			</div>
			<fieldset class="sm:col-span-2">
				<legend class="block text-sm font-medium text-gray-700">{t('formContact.budget')}</legend>
				<div class="mt-4 grid grid-cols-1 gap-y-4">
					<div class="flex items-center">
						<input id="budget-under-25k" name="budget" checked={budget == 'below-25k'} value="below-25k" onChange={(evt)=>setBudget(evt.target.value)} type="radio" class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300"/>
						<label for="budget-under-25k" class="ml-3">
							<span class="block text-sm text-gray-700">{t('formContact.budget.option1')}</span>
						</label>
					</div>
					<div class="flex items-center">
						<input id="budget-25k-50k" name="budget" checked={budget == '25k-50k'} value="25k-50k" onChange={(evt)=>setBudget(evt.target.value)} type="radio" class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300"/>
						<label for="budget-25k-50k" class="ml-3">
							<span class="block text-sm text-gray-700">{t('formContact.budget.option2')}</span>
						</label>
					</div>
					<div class="flex items-center">
						<input id="budget-50k-100k" name="budget" checked={budget == '50k-100k'} value="50k-100k" onChange={(evt)=>setBudget(evt.target.value)} type="radio" class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300"/>
						<label for="budget-50k-100k" class="ml-3">
							<span class="block text-sm text-gray-700">{t('formContact.budget.option3')}</span>
						</label>
					</div>
					<div class="flex items-center">
						<input id="budget-over-100k" name="budget" checked={budget == 'above-100k'} value="above-100k" onChange={(evt)=>setBudget(evt.target.value)} type="radio" class="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300"/>
						<label for="budget-over-100k" class="ml-3">
							<span class="block text-sm text-gray-700">{t('formContact.budget.option4')}</span>
						</label>
					</div>
				</div>
			</fieldset>
			<div class="sm:col-span-2">
				<label for="channelInfo" class="block text-sm font-medium text-gray-700">{t('formContact.howTo')}</label>
				<div class="mt-1">
					<input type="text" name="channelInfo" id="channelInfo"  value={channelInfo} onChange={(evt)=>setChannelInfo(evt.target.value)} class={isValidInput(channelInfo.length == 0 || channelInfo.length >= 8)}/>
				</div>
			</div>
			<div class="text-right sm:col-span-2">
				<button type="button" onClick={()=>submit()} class={isValidForm(validateForm())}>{t('buttons.submit')}</button>
			</div>
		</form>

	);
}

export default FormContact;