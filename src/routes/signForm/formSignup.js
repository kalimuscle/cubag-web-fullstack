import { h } from 'preact';
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'preact-router/match';
import { route } from 'preact-router';
import {useEffect, useState, useContext} from "preact/hooks";
import PhoneInput from 'react-phone-input-2';
import { TranslateContext } from '@denysvuika/preact-translate';
import { gql, useMutation } from '@apollo/client';
import { sign_tour_creator} from '../../store/actions/';

// Define mutation
const CREATE_TOUR_CREATOR_MUTATION = gql`
  mutation createTourCreator($input: CreateTourCreatorInput!){
	createTourCreator(input: $input){
		id
	}
  }
`;

const FormSignup = () => {
	const [mutateFunction, { data, loading, error }] = useMutation(CREATE_TOUR_CREATOR_MUTATION, {
		onCompleted: (data) => {  
			console.log({data}); 
			dispatch(sign_tour_creator(data.createTourCreator.id));
			route('/confirm');
		}
	});
	const { setLang, t, lang } = useContext(TranslateContext);

	const dispatch = useDispatch();
	
	const [name, setName] = useState('');
	const [email, setEmail] = useState('');
	const [phone, setPhone] = useState('');
	const [password, setPassword] = useState('');
	const [confirmPassword, setConfirmPassword] = useState('');

	if (loading) return 'Submitting';
  	if (error) return `Submission error! ${error.message}`;

	const sign = async () => {
		//setSaveActionClicked(true);
		await mutateFunction({
			variables: { 
				input:{
					name,
					email,
					password,
					phone
				}	
			} 
		});

		setName('');
		setEmail('');
		setPhone('');
		setPassword('');
		setConfirmPassword('');
	}

	const validateEmail = (value) => {
		const expEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		const pattern_email = new RegExp(expEmail);

		return pattern_email.test(value)
	}

	const validatePassword = (password) => {
		const expPassword = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
		const patternPassword = new RegExp(expPassword);
	
		return patternPassword.test(password);
	};

	const validateForm = () => (
		name.length >= 8 &&
		validatePassword(password) &&
		validateEmail(email) &&
		password == confirmPassword
	)
	

	const isValidInput = (value) => {
		const inputClass = "mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm";
		const invalidClass = "mt-1 block w-full border border-red-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-red-500 focus:border-red-500 sm:text-sm"

		return value ? inputClass : invalidClass
	}

	const isValidForm = (value) => {
		const buttonClass = "w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500";
		const disabledClass = "opacity-50 w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500";
		
		return value ? buttonClass : disabledClass
	}
	

	return (
		<form action="#" method="POST" class="space-y-6">
			<div>
				<label for="name" class="block text-sm font-medium text-gray-700"> {t('formAuth.name')} </label>
				<div class="mt-1">
					<input type="text" name="name" id="name" value={name} onChange={(evt)=>setName(evt.target.value)} class={isValidInput( name.length == 0 || name.length >= 8)}/>
				</div>
			</div>
			<div>
				<label for="email" class="block text-sm font-medium text-gray-700"> {t('formAuth.email')} </label>
				<div class="mt-1">
					<input id="email" name="email" type="email" autocomplete="email" value={email} onChange={(evt)=>setEmail(evt.target.value)} class={isValidInput( email.length == 0 || validateEmail(email))}/>
				</div>
			</div>

			<div>
				<label for="email" class="block text-sm font-medium text-gray-700"> {t('formAuth.phone')} </label>
				<div class="mt-1">
					<PhoneInput
						country={'cu'}
						value={phone}
						onChange={(value)=>setPhone(value)}
						inputClass={isValidInput(phone.length == 0 || phone.length == 10)}
						specialLabel=""
						placeholder='+53 XXX XXX XX'
					/>
				</div>
			</div>

			<div class="space-y-1">
				
				<div class="flex justify-between">
					<label for="password" class="block text-sm font-medium text-gray-700"> {t('formAuth.password')} </label>
				</div>
				<div class="mt-1">
					<input id="password" name="password" type="password" value={password} onChange={(evt)=>setPassword(evt.target.value)} class={isValidInput( password.length == 0 || validatePassword(password))}/>
				</div>
				<p class="mt-1 text-sm text-gray-500">{t('formAuth.password.condition')}</p>
			</div>

			<div class="space-y-1">
				<label for="confirmPassword" class="block text-sm font-medium text-gray-700"> {t('formAuth.confirm_password')} </label>
				<div class="mt-1">
					<input id="password" name="confirmPassword" type="password" value={confirmPassword} onChange={(evt)=>setConfirmPassword(evt.target.value)} class={isValidInput(password == confirmPassword)}/>
				</div>
			</div>

			{/* <div class="flex items-center justify-between">
				<div class="flex items-center">
					<input id="remember-me" name="remember-me" type="checkbox" class="h-4 w-4 text-blue-600 focus:ring-blue-500 border-gray-300 rounded"/>
					<label for="remember-me" class="ml-2 block text-sm text-gray-900"> Remember me </label>
				</div>

				<div class="text-sm">
					<a href="#" class="font-medium text-blue-600 hover:text-blue-500"> Forgot your password? </a>
				</div>
			</div> */}

			<div>
				<button type="button" onClick={()=>sign()} class={isValidForm(validateForm())}>{t('buttons.sign')}</button>
			</div>
		</form>

	);
}

export default FormSignup;