import { h } from 'preact';
import { useSelector, useDispatch } from 'react-redux'
import { route } from 'preact-router';
import { Link } from 'preact-router/match';
import {useEffect, useState, useContext} from "preact/hooks";
import { gql, useLazyQuery  } from '@apollo/client';
import { TranslateContext } from '@denysvuika/preact-translate';
import { sign_tour_creator} from '../../store/actions/';

// Define query
const AUTHENTICATE_TOUR_CREATOR_QUERY = gql`
  query authenticateTourCreator($input: AuthenticateTourCreatorInput!){
	authenticateTourCreator(input: $input){
		id
		email
		name
	}
}
`;

const FormSignin = () => {
const [queryFunction, { data, loading, error }] = useLazyQuery (AUTHENTICATE_TOUR_CREATOR_QUERY, {
		onCompleted: (data) => {  
			console.log({data}); 
			dispatch(sign_tour_creator({
				id: data.authenticateTourCreator.id,
				email: data.authenticateTourCreator.email,
				name: data.authenticateTourCreator.name
			}));
			route('/dashboard');
		}
	});
	const { setLang, t, lang } = useContext(TranslateContext);
	const [password, setPassword] = useState('');
	const [email, setEmail] = useState('');

	const dispatch = useDispatch();

	if (loading) return 'Submitting';
  	if (error) return `Submission error! ${error.message}`;

	const sign = async () => {
		await queryFunction({
			variables: { 
				input:{
					email,
					password
				}	
			} 
		});

		setEmail('');
		setPassword('');
	}

	const validateEmail = (value) => {
		const expEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		const pattern_email = new RegExp(expEmail);

		return pattern_email.test(value)
	}

	const validatePassword = (password) => {
		const expPassword = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
		const patternPassword = new RegExp(expPassword);
	
		return patternPassword.test(password);
	};

	const validateForm = () => (
		validatePassword(password) &&
		validateEmail(email)
	)

	const isValidInput = (value) => {
		const inputClass = "mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm";
		const invalidClass = "mt-1 block w-full border border-red-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-red-500 focus:border-red-500 sm:text-sm"

		return value ? inputClass : invalidClass
	}

	const isValidForm = (value) => {
		const buttonClass = "w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500";
		const disabledClass = "opacity-50 w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500";
		
		return value ? buttonClass : disabledClass
	}

	return (
		<form action="#" method="POST" class="space-y-6">
			<div>
				<label for="email" class="block text-sm font-medium text-gray-700"> {t('formAuth.email')} </label>
				<div class="mt-1">
					<input id="email" name="email" type="email" autocomplete="email" value={email} onChange={(evt)=>setEmail(evt.target.value)} class={isValidInput(email.length == 0 || validateEmail(email))}/>
				</div>
			</div>

			<div class="space-y-1">
				<label for="password" class="block text-sm font-medium text-gray-700"> {t('formAuth.password')} </label>
				<div class="mt-1">
				<input id="password" name="password" type="password" value={password} onChange={(evt)=>setPassword(evt.target.value)} class={isValidInput(password.length == 0 ||validatePassword(password))}/>
				</div>
			</div>

			<div>
				<button type="button" onClick={()=>sign()} class={isValidForm(validateForm())}>{t('buttons.sign')}</button>
			</div>
		</form>

	);
}

export default FormSignin;