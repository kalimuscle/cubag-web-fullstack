
export const SIGN_NEW_TOUR_CREATOR = 'SIGN_NEW_TOUR_CREATOR';
export const REF_TOUR_CREATOR = 'REF_TOUR_CREATOR';
export const CREATE_PEOPLE_ID = 'CREATE_PEOPLE_ID';
export const SIGNOUT_TOUR_CREATOR = 'SIGNOUT_TOUR_CREATOR';


export function sign_tour_creator( elem ) { 
    return (dispatch) => {
        dispatch({
            type: SIGN_NEW_TOUR_CREATOR,
            payload: elem
        });
    }
}

export function ref_tour_creator(id ) { 
    return (dispatch) => {
        dispatch({
            type: REF_TOUR_CREATOR,
            payload: id
        });
    }
}

export function create_people_id(id ) { 
    return (dispatch) => {
        dispatch({
            type: CREATE_PEOPLE_ID,
            payload: id
        });
    }
}

export function signOutTourCreator() { 
    return (dispatch) => {
        dispatch({
            type: SIGNOUT_TOUR_CREATOR
        });
    }
}