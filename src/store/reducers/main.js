/* eslint-disable no-case-declarations */
import * as types from '../actions';

const initialState = {
    language: 'es',
    userSub: null,
    name: '',
    email:'',
    tourCreatorId: '',
    peopleId: '',
    activities: [],
    guides: [],
    lodging: [],
    transportation: [],
    tours: []
};

const mainReducer = (state = initialState, action) => {
    switch (action.type) {
        case "SIGN_NEW_TOUR_CREATOR":
            return {
                ...state,
                userSub: action.payload.id,
                name: action.payload.name,
                email: action.payload.email
            };
        case "REF_TOUR_CREATOR":
            return {
                ...state,
                tourCreatorId: action.payload
            };
        case "CREATE_PEOPLE_ID":
             return {
                ...state,
                peopleId: action.payload
            };
        case "SIGNOUT_TOUR_CREATOR":
            return {
                ...state,
                userSub: '',
                name: '',
                email: ''
            };
        
        default:
            return state;
    }
};

export default mainReducer;
