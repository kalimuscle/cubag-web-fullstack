import './style/index.scss';
import App from './components/app';
import store from './store/store';

import 'preact/debug';
import 'preact/devtools';
import { Provider } from 'react-redux';
import {
    ApolloClient,
    InMemoryCache,
    ApolloProvider,
    HttpLink,
    from
  } from "@apollo/client";
  import { onError } from "@apollo/client/link/error";
  import { createUploadLink } from "apollo-upload-client";

  const httpLink = new HttpLink({
    uri: 'http://localhost:3000/dev/graphql'
  });

  const uploadLink = new createUploadLink({
    uri: 'http://localhost:3000/dev/graphql',
    
  })
  
  const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors)
      graphQLErrors.forEach(({ message, locations, path }) =>
        console.log(
          `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
        ),
      );
  
    if (networkError) console.log(`[Network error]: ${networkError}`);
  });
  
  const graphqlEndpoint = new ApolloClient({
    link: from([errorLink, uploadLink]),
    cache: new InMemoryCache()
  });

const Application = () => (
    <Provider store={store}>
        <ApolloProvider client={graphqlEndpoint}>
            <App />
        </ApolloProvider>
        
    </Provider>
);

export default Application;

