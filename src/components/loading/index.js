import { h } from 'preact';
import { SpinnerRound } from 'spinners-react';

const Loading = ({}) => (
    <div class="flex justify-center my-auto items-center">
		<SpinnerRound enabled={true} color="#0c2bb7"/>
	</div>
);

export default Loading;
