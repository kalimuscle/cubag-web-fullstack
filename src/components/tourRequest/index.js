import { h, Fragment } from 'preact';
import {useEffect, useState} from "preact/hooks";

const TourRequest = () => {

  return (
	<Fragment>
		<div class="mt-10 sm:hidden">
			<div class="px-4 sm:px-6">
				<h2 class="text-gray-500 text-xs font-medium uppercase tracking-wide">Tours Request</h2>
			</div>
			<ul role="list" class="mt-3 border-t border-gray-200 divide-y divide-gray-100">
				<li>
					<a href="#" class="group flex items-center justify-between px-4 py-4 hover:bg-gray-50 sm:px-6">
						<span class="flex items-center truncate space-x-3">
							<span class="w-2.5 h-2.5 flex-shrink-0 rounded-full bg-pink-600" aria-hidden="true"></span>
							<span class="font-medium truncate text-sm leading-6">
								GraphQL API
								<span class="truncate font-normal text-gray-500">in Engineering</span>
							</span>
						</span>
						<svg class="ml-4 h-5 w-5 text-gray-400 group-hover:text-gray-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
							<path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
						</svg>
					</a>
				</li>

			</ul>
		</div>

		<div class="hidden mt-8 sm:block">
			<div class="align-middle inline-block min-w-full border-b border-gray-200">
				<table class="min-w-full">
					<thead>
						<tr class="border-t border-gray-200">
							<th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
								<span class="lg:pl-2">Tours Request</span>
							</th>
							<th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Email</th>
							<th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Phone</th>
							<th class="hidden md:table-cell px-6 py-3 border-b border-gray-200 bg-gray-50 text-right text-xs font-medium text-gray-500 uppercase tracking-wider">Updated</th>
							<th class="pr-6 py-3 border-b border-gray-200 bg-gray-50 text-right text-xs font-medium text-gray-500 uppercase tracking-wider"></th>
						</tr>
					</thead>
					<tbody class="bg-white divide-y divide-gray-100">
						<tr>
							<td class="px-6 py-3 max-w-0 w-full whitespace-nowrap text-sm font-medium text-gray-900">
								<div class="flex items-center space-x-3 lg:pl-2">
									<p class="hover:text-gray-600 text-gray-900 font-bold">
										Anim aute id magna aliqua ad ad non deserunt sunt. Qui irure qui lorem cupidatat ...
									</p>
								</div>
							</td>
							<td class="px-6 py-3 text-sm text-gray-500 font-medium">
								<div class="flex items-center space-x-2">
									victor.moraton@gmail.com
								</div>
							</td>
							<td class="hidden md:table-cell px-6 py-3 whitespace-nowrap text-sm text-gray-500 text-right">+53 52719986</td>
							<td class="hidden md:table-cell px-6 py-3 whitespace-nowrap text-sm text-gray-500 text-right">March 17, 2020</td>
							<td class="px-6 py-3 whitespace-nowrap text-right text-sm font-medium">
								<a href="#" class="text-indigo-600 hover:text-indigo-900">View more</a>
							</td>
						</tr>
						<tr>
							<td class="px-6 py-3 max-w-0 w-full whitespace-nowrap text-sm font-medium text-gray-900">
								<div class="flex items-center space-x-3 lg:pl-2">
									<p class="hover:text-gray-600 text-gray-500 font-normal">
										Anim aute id magna aliqua ad ad non deserunt sunt. Qui irure qui lorem cupidatat ...
									</p>
								</div>
							</td>
							<td class="px-6 py-3 text-sm text-gray-500 font-medium">
								<div class="flex items-center space-x-2">
									victor.moraton@gmail.com
								</div>
							</td>
							<td class="hidden md:table-cell px-6 py-3 whitespace-nowrap text-sm text-gray-500 text-right">+53 52719986</td>
							<td class="hidden md:table-cell px-6 py-3 whitespace-nowrap text-sm text-gray-500 text-right">March 17, 2020</td>
							<td class="px-6 py-3 whitespace-nowrap text-right text-sm font-medium">
								<a href="#" class="text-indigo-600 hover:text-indigo-900">View nore</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</Fragment>
  );
}

export default TourRequest;
