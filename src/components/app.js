import { h } from 'preact';
import { Router } from 'preact-router';

import { useSelector, useDispatch } from 'react-redux'
import Header from './header';

// Code-splitting is automated for `routes` directory
import Home from '../routes/home';
import SignForm from '../routes/signForm';
import Detail from '../routes/detail';
import Contacts from '../routes/contacts';
import Tours from '../routes/tours';
import DashBoard from '../routes/dashboard';
import MyTours from '../routes/mytours';
import Profile from '../routes/profile';
import FormActivities from '../routes/formActivities';
import RedirectSigned from '../routes/redirect-signed';

import esTranslate from '../assets/locations/es.json';
import enTranslate from '../assets/locations/en.json';

import { TranslateProvider } from '@denysvuika/preact-translate';

const App = () => {
	const dispatch = useDispatch();
	const language = useSelector(state => state.language);

	const definition = language == "en" ? enTranslate : esTranslate;

	const translation = {
		en: enTranslate,
		es: esTranslate
	  };

	return (
		<TranslateProvider translations={translation}>
			<div id="app">
				<Router>
					<Home path="/" />
					<Home path="/home" />
					<Detail path="/detail/:_id" />
					<SignForm path="/sign" />
					<Tours path="/tours" />
					<MyTours path="/mytours" />
					<Contacts path="/contacts" />
					<Profile path="/profile" />
					<FormActivities path="/factivities/:tourCreatorId" />
					<DashBoard path="/dashboard" />
					<RedirectSigned path="/confirm" />
				</Router>
			</div>
		</TranslateProvider>
	);
}

export default App;
