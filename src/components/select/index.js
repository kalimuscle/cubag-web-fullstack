import { toNumber } from 'lodash';
import { h } from 'preact';
import {useEffect, useState} from "preact/hooks";
import style from './style.css';

const Select = ({items, value, onChange, label, required}) => {
	const [itemsShow, setItemsShow] = useState(false);
	//const [itemIndex, setItemIndex] = useState('first-floor');

	const requiredComponent = required ? <span class="text-red-500">*</span> : null

	const selectItem = (key) => {
	
		setItemsShow(false)
		onChange({value: key});
	}

	const listElements = items.map((item)=> (
		<li key={item["key"]} onClick={()=>selectItem(item["key"])} class="text-gray-900 cursor-default select-none relative py-2 pl-8 pr-4" id="listbox-option-0" role="option">				
			<span class="font-normal block truncate"> {item["name"]} </span>
		</li>
	))

	const list = itemsShow ? (
		<ul class="absolute z-10 mt-1 w-full bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm" tabindex="-1" role="listbox" aria-labelledby="listbox-label" aria-activedescendant="listbox-option-3">		
			{listElements}
		</ul>
	) : null

	return(
		<div>
			<label id="listbox-label" class="block text-sm font-medium text-gray-700"> {label} {requiredComponent}</label>
			<div class="mt-1 relative">
				<button onClick={()=>setItemsShow(!itemsShow)} type="button" class="relative w-full bg-white border border-gray-300 rounded-md shadow-sm pl-3 pr-10 py-2 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" aria-haspopup="listbox" aria-expanded="true" aria-labelledby="listbox-label">
					<span class="block truncate"> {items.find((item)=> item["key"] === value).name} </span>
					<span class="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
						<svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
							<path fill-rule="evenodd" d="M10 3a1 1 0 01.707.293l3 3a1 1 0 01-1.414 1.414L10 5.414 7.707 7.707a1 1 0 01-1.414-1.414l3-3A1 1 0 0110 3zm-3.707 9.293a1 1 0 011.414 0L10 14.586l2.293-2.293a1 1 0 011.414 1.414l-3 3a1 1 0 01-1.414 0l-3-3a1 1 0 010-1.414z" clip-rule="evenodd" />
						</svg>
					</span>
				</button>
				{list}
			</div>
		</div>
	);
};

export default Select;
