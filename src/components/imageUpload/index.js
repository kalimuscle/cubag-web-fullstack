import { toNumber } from 'lodash';
import { h } from 'preact';
import {useContext, useState} from "preact/hooks";
import ImageUploading from 'react-images-uploading';
import { TranslateContext } from '@denysvuika/preact-translate';

const ImageUploader = ({maxNumber,images, onChange}) => {
	const { setLang, t, lang } = useContext(TranslateContext);

	const onChangeImages = (imageList, addUpdateIndex) => {
		onChange({imageList, addUpdateIndex});
	};

	return(
		<ImageUploading
			acceptType={['jpg','jpeg','png']}
			multiple
			value={images}
			onChange={onChangeImages}
			maxNumber={maxNumber}
			dataURLKey="data_url"
		>
			{({
				imageList,
				onImageUpload,
				onImageRemoveAll,
				onImageUpdate,
				onImageRemove,
				isDragging,
				dragProps,
			}) => (
				// write your building UI
				<div className="upload__image-wrapper">
					<div class="text-left">
						<svg class="h-12 w-12 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
							<path vector-effect="non-scaling-stroke" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 13h6m-3-3v6m-9 1V7a2 2 0 012-2h6l2 2h6a2 2 0 012 2v8a2 2 0 01-2 2H5a2 2 0 01-2-2z" />
						</svg>
						<h3 class="mt-2 text-sm font-medium text-gray-900">
							{t('imageUploader.title')}
						</h3>
						
						<div class="mt-6">
							<button
								type="button"
								style={isDragging ? { color: 'red' } : undefined}
								onClick={onImageUpload}
								{...dragProps}
								class="inline-flex items-center px-4 py-2 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500">
								<svg class="-ml-1 mr-2 h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
									<path fill-rule="evenodd" d="M10 3a1 1 0 011 1v5h5a1 1 0 110 2h-5v5a1 1 0 11-2 0v-5H4a1 1 0 110-2h5V4a1 1 0 011-1z" clip-rule="evenodd" />
								</svg>
								{t('imageUploader.button_add')}
							</button>
							{
								imageList.length > 0 ? (
									<button
										type="button"
										onClick={onImageRemoveAll}
										{...dragProps}
										class="inline-flex items-center mx-4 px-4 py-2 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500">
										<svg class="-ml-1 mr-2 h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
											<path fill-rule="evenodd" d="M10 3a1 1 0 011 1v5h5a1 1 0 110 2h-5v5a1 1 0 11-2 0v-5H4a1 1 0 110-2h5V4a1 1 0 011-1z" clip-rule="evenodd" />
										</svg>
										{t('imageUploader.button_remove_all')}
									</button>
								): null
							}
							
						</div>
						
					</div>
					&nbsp;
					
					<ul role="list" class="grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
						{
							imageList.map((image, index) => (
								<li class="col-span-1 flex flex-col text-center bg-white rounded-lg shadow divide-y divide-gray-200">
									<div class="flex-1 flex flex-col p-8">
										<img class="w-32 h-32 flex-shrink-0 mx-auto" src={image['data_url']} alt="" width="100" />
									</div>
									<div>
										<div class="-mt-px flex divide-x divide-gray-200">
											<div class="w-0 flex-1 flex">
												<button type="button" class="m-2" onClick={() => onImageUpdate(index)}>
													{t('imageUploader.button_update')}
												</button>
											</div>
											<div class="-ml-px w-0 flex-1 flex">
												<button type="button" class="m-2" onClick={() => onImageRemove(index)}>
												{t('imageUploader.button_remove')}
												</button>
											</div>
										</div>
									</div>
								</li>
							))
						}
					</ul>
				</div>
				)}
		</ImageUploading>
	);
};

export default ImageUploader;
